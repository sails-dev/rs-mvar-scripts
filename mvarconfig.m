function s = mvarconfig(filename)

if nargin ~= 1
    error('filename is required');
end

% Read the file and scan line by line
f = fopen(filename);

line = fgetl(f);

s = {};

while ischar(line)
    % Strip anything from a comment character (#) onwards
    out = regexp(line, '([^#]*).*', 'tokens');

    if length(out) > 0
        out = out{1}{1};

        out = strtrim(out);

        if length(out) > 0
            dat = regexp(out, '^(.*)=(.*)$', 'tokens');

            if length(dat) ~= 1
                error(sprintf('Failure when reading path file in line %s', line));
            end

            varname = dat{1}{1};
            varvalue = dat{1}{2};

            if length(varname) < 1
                error(sprintf('Failure when reading var name in line %s', line));
            end

            if length(varvalue) < 1
                error(sprintf('Failure when reading var value in line %s', line));
            end

            s.(varname) = varvalue;
        end
    end

    line = fgetl(f);
end

% Fix up some fields to make them easier to use
fnames = fieldnames(s);

for i=1:numel(fnames)
    if startsWith(fnames{i}, 'DIR') | startsWith(fnames{i}, 'FILE')
        if ~strcmp(fnames{i}, 'DIR_BASE')
            newvalue = fullfile(s.DIR_BASE, s.(fnames{i}));
            s.(fnames{i}) = newvalue;
        end
    end
end

% Split participants and runs into MATLAB cell arrays
s.PARTICIPANTS = strsplit(replace(s.PARTICIPANTS, '"', ''));
s.RUNS = strsplit(replace(s.RUNS, '"', ''));

% Convert PROC_NUM into an int
s.PROC_NUM = str2num(s.PROC_NUM);

% Convert DEFAULT_MODEL_ORDER into an int
s.DEFAULT_MODEL_ORDER = str2num(s.DEFAULT_MODEL_ORDER);

% If FSL_LIBS is non-null and not in LD_LIBRARY_PATH, add it
% Horrible but necessary
if ~isempty(s.FSL_LIBS)
    if isempty(strfind(getenv('LD_LIBRARY_PATH'), s.FSL_LIBS))
        setenv('LD_LIBRARY_PATH', strcat([s.FSL_LIBS ':'], getenv('LD_LIBRARY_PATH')));
    end
end
