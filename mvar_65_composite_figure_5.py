#!/usr/bin/python3

from sys import argv, exit
from os.path import join

from PIL import Image, ImageDraw, ImageFont

from mvarconfig import (DIR_FIGURES, FONT, DEFAULT_MODEL_ORDER)

if len(argv) not in (1, 2):
    print("mvar_65_composite_figure_5.py [MODEL_ORDER]")
    exit(1)

if len(argv) == 2:
    model_order = int(argv[1])
else:
    # Default model order
    model_order = DEFAULT_MODEL_ORDER

print("I: Using model order {}".format(model_order))

fig_dir = join(DIR_FIGURES, str(model_order))

# Composite Figure 5
DPI = 600

LABEL_COLOUR = (85, 85, 85, 255)

fig_pixels = [int(7.5 * DPI), int(7.5 * DPI)]

# Create a blank figure
f = Image.new('RGBA', fig_pixels, 'white')

# Add the main plots
main = Image.open(join(fig_dir, '46_participant_examples.png'))
f.paste(main, (25, 0))

# Participants in order
p1 = Image.open(join(fig_dir, '47_917255_3_ms_psd_radial.png'))
f.paste(p1, (3375, 550))

p2 = Image.open(join(fig_dir, '47_406836_3_ms_psd_radial.png'))
f.paste(p2, (3375, 1525))

p3 = Image.open(join(fig_dir, '47_179245_3_ms_psd_radial.png'))
f.paste(p3, (3375, 2500))

p4 = Image.open(join(fig_dir, '47_100307_3_ms_psd_radial.png'))
f.paste(p4, (3375, 3475))

# Add the column labels

draw = ImageDraw.Draw(f)

font = ImageFont.truetype(FONT, 120)

draw.text((50, 50), "A", fill=LABEL_COLOUR, font=font)

draw.text((900, 50), "B", fill=LABEL_COLOUR, font=font)

draw.text((1800, 50), "C", fill=LABEL_COLOUR, font=font)

draw.text((2700, 50), "D", fill=LABEL_COLOUR, font=font)

draw.text((3350, 50), "E", fill=LABEL_COLOUR, font=font)

# Now create a non-alpha version to save
final = Image.new("RGB", f.size, (255, 255, 255))
final.paste(f, mask=f.split()[3])

final.save(join(fig_dir, '65_figure5.png'), dpi=(DPI, DPI))
