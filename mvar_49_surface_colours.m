% Matlab script to plot surfaces

% Start by reading our configuration to find paths etc
conf = mvarconfig('mvarconfig.ini')

% If you want to override the default model order, do it here
model_order = conf.DEFAULT_MODEL_ORDER;

% If set, figures will be closed after saving
closefigs = true;

% Now we need to add some paths for the modules we require
addpath(fullfile(conf.DIR_EXTERNAL, 'export_fig'))
addpath(fullfile(conf.DIR_EXTERNAL, 'osl', 'osl-core'))
osl_startup

addpath(fullfile(conf.DIR_EXTERNAL, 'mfiles'));

% This line may be necessary depending on your system On Linux boxes, MATLAB
% includes a screwed version of the Qt libraries which it then shoves into
% LD_LIBRARY_PATH whilst wb_command needs the system version it was built
% against.  We can work around that by forcing the normal library directory
% to the front of LD_LIBRARY_PATH, over-riding their override.
% We've guarded this behind being on Linux-x86_64 only, but you may
% need to comment it depending on your platform.
% I hate MATLAB and Mathworks and wish it would all go away (this comment
% endorsed by MH only).
if strcmp(computer('arch'), 'glnxa64')
    % Avoid pre-pending multiple times if we use the script more than once
    if ~startsWith(getenv('LD_LIBRARY_PATH'), '/usr/lib/x86_64-linux-gnu:')
        setenv('LD_LIBRARY_PATH', strcat('/usr/lib/x86_64-linux-gnu:', getenv('LD_LIBRARY_PATH')));
    end
end

% Load the parcellation

parc = parcellation(fullfile(conf.DIR_BASE, 'aal_cortical_merged_8mm_stacked.nii.gz'));
node_inds = load(fullfile(conf.DIR_BASE, 'aal_cortical_merged_sorted.inds'));

[~,I] = sort(node_inds);

% Load RGB Values etc
cgroups = readtable(fullfile(conf.DIR_BASE, 'circos_groups.csv'));
cgroups.RGB = str2double(strip(split(cgroups.RGBColour, ','))) / 255;

aal = readtable(fullfile(conf.DIR_BASE, 'aal_cortical_details.csv'));

base = fullfile(conf.DIR_FIGURES, num2str(model_order));


vals = aal.CircosGroupOrder;
cm = cgroups.RGB;

parc.plot_surf_montage(vals(I),...
        'montage', 'radial',...
        'interptype', 'enclosing',...
        'clims', [min(vals) max(vals)],...
        'cmap',cm,...
        'position', [100 100 1152 768],...
        'add_colorbar', false);

export_fig(fullfile(base, sprintf('49_surface_labels.png')), '-transparent');
if closefigs
    close('all');
end
