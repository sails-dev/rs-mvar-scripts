#!/usr/bin/python3

from sys import argv, exit
from os import makedirs
from os.path import join

import h5py

import numpy as np
import scipy.stats as ss

import matplotlib.pyplot as plt

import sails

from mvarconfig import (DIR_MODALSTATS, DIR_FIGURES,
                        FILE_CIRCOS_GROUPS, FILE_CIRCOS_REGIONS,
                        DEFAULT_MODEL_ORDER)

if len(argv) not in (1, 2):
    print("mvar_42_mode_dist.py [MODEL_ORDER]")
    exit(1)

if len(argv) == 2:
    model_order = int(argv[1])
else:
    # Default model order
    model_order = DEFAULT_MODEL_ORDER

print("I: Using model order {}".format(model_order))

in_dir = join(DIR_MODALSTATS, str(model_order))
out_dir = join(DIR_FIGURES, str(model_order))
makedirs(out_dir, exist_ok=True)

# Configuration

# Fourier bands to analyse and their names
bands = [(0, 3), (3, 7), (7, 13), (13, 30)]
raw_band_names = ['delta', 'theta', 'alpha', 'beta']
band_names = [r'$\{}$'.format(b) for b in raw_band_names]

num_bands = len(bands)

c = sails.CircosHandler.from_csv_files(FILE_CIRCOS_GROUPS, FILE_CIRCOS_REGIONS,
                                       analysis_column='AnalysisID_NewSort')

print("I: Loading fourier PSD data")
filename = join(in_dir, '32_fourier_summary.hdf5')
f = h5py.File(filename, 'r')

psd = f['psd'][...]

freq_vect = f['freq_vect'][...]

f.close()

# Calculate some stats
p = psd.mean(axis=3)
pd = np.einsum('iij->ij', p)

d = np.dot(pd.T, pd)
C = np.corrcoef(pd.T)

plt.rcParams.update({'font.size': 7})

# Create spectrum plot - color channels based on netmat colors
print("I: Plotting spectrum")

color_dict = c.colour_mapping(normalise=True)
chan_colors = {}

for group in c.groups:
    gcolor = c.groups[group].circoscolour
    for r in c.groups[group].regions.keys():
        chan_colors[r] = color_dict[gcolor]

chan_cols = []
for chan in c.data_regions:
    chan_cols.append(chan_colors[chan])

# Spectrum plot is on left of figure 4; needs to be 2.25 x 4
# We've reduced this as the larger version was not as readable
fig = plt.figure(figsize=(2.25, 4))
ax = fig.gca()

# We also need a version which is 1.75x1.75 for SI
fig2 = plt.figure(figsize=(1.75, 1.75))
ax_si = fig2.gca()

# We also need a version with only the alpha band highlighted
# for the summary figure and the poster
fig3 = plt.figure(figsize=(2.25, 4))
ax_sum = fig3.gca()

# Limit spectrum to 0-30Hz (we're not interested in gamma here)
for chanidx in range(pd.shape[0]):
    ax.plot(freq_vect, pd[chanidx, :]*1e7, linewidth=0.8, color=chan_cols[chanidx])
    ax_si.plot(freq_vect, pd[chanidx, :]*1e7, linewidth=0.6, color=chan_cols[chanidx])
    ax_sum.plot(freq_vect, pd[chanidx, :]*1e7, linewidth=0.6, color=chan_cols[chanidx])

plt.grid(False)

ax2 = ax.twiny()
ax2_si = ax_si.twiny()
ax2_sum = ax_sum.twiny()

# Add frequency band visualisation
for a, b in zip([ax, ax_sum], [ax2, ax2_sum]):
    b.set_xticks([])
    b.set_xticklabels([])
    b.set_xlim(0, 30)
    a.set_xlim(0, 30)
    b.xaxis.tick_top()
    a.xaxis.tick_bottom()

    a.set_xlabel('Frequency (Hz)')
    a.set_ylabel('Power (a.u.)')

# Handle SI slightly differently
ax2_si.set_xticks([])
ax2_si.set_xticklabels([])
ax2_si.set_xlim(0, 30)
ax_si.set_xlim(0, 30)
ax_si.set_ylim(0, 11)
for item in ax_si.get_xticklabels() + ax_si.get_yticklabels():
    item.set_fontsize(6)
ax2_si.xaxis.tick_top()
ax_si.xaxis.tick_bottom()

ax_si.set_xlabel('Frequency (Hz)', fontsize=6)
ax_si.set_ylabel('Power (a.u.)', fontsize=6)

for tag in ['right', 'top']:
    ax.spines[tag].set_visible(False)
    ax2.spines[tag].set_visible(False)

    ax_si.spines[tag].set_visible(False)
    ax2_si.spines[tag].set_visible(False)

    ax_sum.spines[tag].set_visible(False)
    ax2_sum.spines[tag].set_visible(False)

for a in [ax2, ax2_si, ax2_sum]:
    a.spines['left'].set_visible(False)
    a.spines['bottom'].set_visible(False)

ymn, ymx = ax.get_ylim()
ymn_si, ymx_si = ax_si.get_ylim()
ymn_sum, ymx_sum = ax_sum.get_ylim()

add_height = (ymx - ymn) * 0.03
add_height_si = (ymx_si - ymn_si) * 0.07
add_height_sum = (ymx_sum - ymn_sum) * 0.03

ax.set_ylim(ymn, ymx+add_height)
ax_sum.set_ylim(ymn_sum, ymx_sum+add_height_sum)
ax_si.set_ylim(ymn_si, ymx_si+add_height_si)

ax.spines['left'].set_bounds(ymn, ymx)
ax_si.spines['left'].set_bounds(ymn_si, ymx_si)
ax_sum.spines['left'].set_bounds(ymn_sum, ymx_sum)

# number of things
for freqnum, (b, g) in enumerate(zip(bands, band_names)):
    alpha = 0.15 + ((freqnum / num_bands) * 0.25)

    low = b[0] + .25
    high = b[1] - .25

    # Main figure
    xy = [(low, ymx), (low, ymn), (high, ymn), (high, ymx),
          (high - .5, ymx+add_height), (low + .5, ymx+add_height)]

    r = plt.Polygon(xy, closed=True, alpha=alpha)
    r.set_facecolor('grey')
    r.set_alpha(alpha)

    ax2.add_patch(r)

    ax.text(np.mean(b), ymx,
            g, horizontalalignment='center', fontsize=10)

    # SI figures
    xy = [(low, ymx_si), (low, ymn_si), (high, ymn_si), (high, ymx_si),
          (high - .5, ymx_si+add_height_si), (low + .5, ymx_si+add_height_si)]

    r2 = plt.Polygon(xy, closed=True, alpha=alpha)
    r2.set_facecolor('grey')
    r2.set_alpha(alpha)

    ax2_si.add_patch(r2)

    ax_si.text(np.mean(b), ymx_si,
               g, horizontalalignment='center', fontsize=6)

    # Summary figure - label alpha only
    if raw_band_names[freqnum] == 'alpha':
        xy = [(low, ymx_sum), (low, ymn_sum), (high, ymn_sum), (high, ymx_sum),
              (high - .5, ymx_sum+add_height_sum), (low + .5, ymx_sum+add_height_sum)]

        r = plt.Polygon(xy, closed=True, alpha=alpha)
        r.set_facecolor('grey')
        r.set_alpha(alpha)

        ax2_sum.add_patch(r)

        ax_sum.text(np.mean(b), ymx,
                    g, horizontalalignment='center', fontsize=10)


plt.figure(fig.number)
plt.tight_layout()
plt.savefig(join(out_dir, '40_spectrum.png'), dpi=600)

plt.figure(fig2.number)
plt.tight_layout()
ax_si.set_yticks([2, 4, 6, 8, 10], [2, 4, 6, 8, 10])
plt.title('Order {}'.format(model_order), fontsize=6, pad=3)
plt.savefig(join(out_dir, '40_spectrum_si.png'), dpi=600)

plt.figure(fig3.number)
plt.tight_layout()
plt.savefig(join(out_dir, '40_spectrum_summary.png'), dpi=600)

# Create per-band plot
for ii, band in enumerate(bands):
    band_name = raw_band_names[ii]

    # Netmat plots need to be narrow enough to fit in composite figure
    f = plt.figure(figsize=(1.5625, 2.0))

    # Find the relevant parts of the fourier spectrum
    idx = np.where((freq_vect >= band[0]) & (freq_vect <= band[1]))

    tmp = p[:, :, idx[0]].mean(axis=2)
    nareas = tmp.shape[0]

    # Netmat plot
    c.plot_netmat(tmp - np.diag(np.diag(tmp)), ax=plt.gca(),
                  labels=False, hregions=True,
                  showgrid=False, show_xticks=True,
                  show_yticks=True)

    plt.savefig(join(out_dir, '40_netmat_{}.png'.format(band_name)), dpi=600)

    # Circular plot - take threshold as 95th percentile
    indices = np.triu_indices(nareas, 1)
    thresh = ss.scoreatpercentile(np.abs(tmp[indices].flatten()), 95)

    # Take away diagonal and lower triangle indices
    tmp[np.tril_indices(nareas)] = np.nan

    # Get rid of beneath threshold connections
    tmp[np.abs(tmp) < thresh] = np.nan

    # Scale the remaining connections between 1-5
    mn = np.nanmin(np.abs(tmp))
    mx = np.nanmax(np.abs(tmp))

    # Now scale width to 4 wide then offset so that the minimum value
    # would be at 1; this leaves us at 1-5
    width = 4 / (mx - mn)
    offset = (1 - (mn * width))

    tmp = tmp * width
    tmp = tmp + (np.sign(tmp) * offset)

    # Circos plots need to be narrow enough to fit in composite figure
    c.gen_circos_plot(tmp, join(out_dir, '40_circos_{}'.format(band_name)),
                      radius=468, circle_prop=0.9,
                      ticks=False, tick_labels=False)
