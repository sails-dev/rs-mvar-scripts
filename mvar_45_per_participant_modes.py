#!/usr/bin/python3

from sys import argv, exit
from os.path import join

import h5py

import numpy as np

import matplotlib.pyplot as plt

from mvarconfig import (PARTICIPANTS, RUNS,
                        DIR_FIGURES, DIR_MODALSTATS,
                        DEFAULT_MODEL_ORDER)

if len(argv) not in (1, 2):
    print("mvar_42_mode_dist.py [MODEL_ORDER]")
    exit(1)

if len(argv) == 2:
    model_order = int(argv[1])
else:
    # Default model order
    model_order = DEFAULT_MODEL_ORDER

print("I: Using model order {}".format(model_order))

in_dir = join(DIR_MODALSTATS, str(model_order))
out_dir = join(DIR_FIGURES, str(model_order))

plt.style.use('default')

# Get our summary data
filename = join(in_dir, '31_surviving_modes.hdf5')
print("I: Loading {}".format(filename))

f = h5py.File(filename, 'r')

surviving_dts = f['s_dts'][...]
surviving_pfs = f['s_pfs'][...]
surviving_runs = f['s_runs'][...]

non_surviving_dts = f['n_dts'][...]
non_surviving_pfs = f['n_pfs'][...]
non_surviving_runs = f['n_runs'][...]

dt_thresholds = f['dt_thresholds'][...]
dts_per_run = f['dts_per_run'][...]

person_list = list(f['person_list'][...])
run_list = list(f['run_list'][...])

f.close()

print("I: Creating per-participant plot")

# Work out per-participant surviving peak frequency
num_parts = len(PARTICIPANTS)
part_array = np.array(person_list)
subj_freq = np.zeros((num_parts,))

# Calculate the average alpha frequency for participants
# in their surviving modes - we use this to order
# the plot below
cur = 0
for pidx, part in enumerate(PARTICIPANTS):
    runs_to_check = part_array == part

    if np.sum(runs_to_check) > 0:
        dts_to_add = np.sum(dts_per_run[runs_to_check])
        if dts_to_add > 0:
            pfs = surviving_pfs[cur:cur+dts_to_add]
            wts = surviving_dts[cur:cur+dts_to_add]
            alpha_pfs = (pfs >= 7) & (pfs <= 13)

            cur += dts_to_add

            if np.sum(alpha_pfs) > 0:
                pfs = pfs[alpha_pfs]
                wts = wts[alpha_pfs]

                subj_freq[pidx] = np.average(pfs, weights=wts)

# Group level DT maximum
vm = np.max(surviving_dts)

# Plot per-person surviving DTs
# 3 runs per person:
#  Run1: y=1.5, Run2: y=2.5, Run3: y=3.5
#  Run1: y=6.5; ...

vm = 25
run_gap = 2
part_gap = 10
run_size = part_gap + run_gap + run_gap


# Elsevier double column width is 190mm
# We want 1 x 1 ratio in inches, so 7.5inches x 7.5 inches
# We're only taking up half the column width and the full height
fig = plt.figure(figsize=(3.75, 7.5), tight_layout=True)

plt.rcParams.update({'font.size': 7})

axL = plt.subplot(1, 1, 1)
axL.xaxis.tick_bottom()
axR = axL.twinx()
axR.xaxis.tick_top()

for tag in ['right', 'top']:
    axL.spines[tag].set_visible(False)
    axR.spines[tag].set_visible(False)

ypos = (part_gap - (2 * run_gap)) / 2

bands = [(0, 3), (3, 7), (7, 13), (13, 30)]
greeks = [r'$\delta$', r'$\theta$', r'$\alpha$', r'$\beta$']

# We don't want to plot gamma here but want the colour calculations
# to stay consistent with other scripts
num_bands = len(bands) + 1

axL.set_xlim(0, 30)
axL.set_ylim(0, 1160)
axR.set_ylim(0, 1160)

axL.spines['left'].set_bounds(0, 1110)
axR.spines['left'].set_visible(False)

xl = axL.get_xlim()
yl = axL.get_ylim()

margin = 1

# number of things
for freqnum, (b, g) in enumerate(zip(bands, greeks)):
    alpha = 0.15 + ((freqnum / num_bands) * 0.25)

    low = b[0] + .25
    high = b[1] - .25

    xy = [(low, 1110), (low, 0), (high, 0), (high, 1110), (high - .5, 1135), (low + .5, 1135)]

    r = plt.Polygon(xy, closed=True, alpha=alpha)
    r.set_facecolor('gray')
    r.set_alpha(alpha)

    axL.add_patch(r)

    axL.text(np.mean(b), 1110,
             g, horizontalalignment='center', fontsize=11)

# Deals with initial increment to 0 below
ypos -= part_gap

for pidx in np.argsort(subj_freq):
    person = PARTICIPANTS[pidx]

    ypos += part_gap

    for runnum, run in enumerate(RUNS):
        if runnum > 0:
            ypos += run_gap

        # Look up the run index
        run_idx = np.where((np.array(person_list) == person) & (np.array(run_list) == str(run)))[0][0]

        # Find the non-surviving modes for this person/run
        n_idx = np.where(non_surviving_runs == run_idx)

        freq = non_surviving_pfs[n_idx]

        y = np.zeros_like(freq) + ypos

        axL.scatter(freq, y, c='k', alpha=0.05, marker='.', s=0.5)

        # Find and plot the surviving modes for this person/run
        s_idx = np.where(surviving_runs == run_idx)

        dt = surviving_dts[s_idx]
        freq = surviving_pfs[s_idx]

        min_dt = dt.min()
        max_dt = dt.max()

        y = np.zeros_like(freq) + ypos

        axL.scatter(freq, y, c=dt, cmap='viridis',
                    vmin=0, vmax=vm, s=10)

plt.xlim([0, 30])

gridline_ticks = np.arange(0, ypos + run_size, run_size)
label_ticks = np.arange(0.5 + run_gap + run_gap + (4 * run_size), ypos, run_size * 5)
people = np.arange(1, 80, 5)
label_ticks = [(run_size / 2) + ((x-1) * (run_size)) for x in people]

# axR.set_yticks(gridline_ticks, minor=False)
# axR.grid(True)

axL.set_yticks(label_ticks, minor=True)
axL.set_yticklabels(range(5, len(dts_per_run) // 3 + 1, 5), minor=True)
axL.set_yticklabels(np.r_[[''], range(5, len(dts_per_run) // 3 + 1, 5)], minor=True)
axL.set_yticklabels([])
axL.set_ylabel('Participant number')
axL.set_xlabel('Frequency (Hz)')
plt.setp(axL.get_yticklines(), visible=False)

# Participants whom we are going to plot on the left hand side of the main figure
# We highlight them here to make the link between the two sides of the figure

# TODO: Replace this if we want it on the final figure

# people = [1, 33, 78]
# right_pos = [(run_size / 2) + ((x-1) * (run_size)) for x in people]

people = []
right_pos = []

axR.set_yticks(right_pos)
axR.set_yticklabels(people)
axR.tick_params(axis='y', colors='red', labelsize='large')
axR.grid(False)

filename = join(out_dir, '45_perparticipant_modes.png')

plt.savefig(filename, dpi=600)

print("I: Saved {}".format(filename))
