#!/usr/bin/python3

import warnings

from sys import exit
from os import makedirs
from os.path import isfile, join
from random import sample

import h5py

import numpy as np
import scipy.stats as ss

import sails

from mvarconfig import (DIR_SIMULATION)


# Our version of scipy irritatingly has FutureWarning's triggered in it
# so hide them
warnings.simplefilter(action='ignore', category=FutureWarning)


def generate_simulation_data(nppts, f1, f2, sample_rate, num_samples):
    # Generate simulation data
    siggen = sails.simulate.SailsTutorialExample()

    # Start by generating some baseband signals which can later be used
    # as exemplar plots
    s1 = siggen.generate_basesignal(f1, .8+.05, sample_rate, num_samples)
    s2 = siggen.generate_basesignal(f2, .61, sample_rate, num_samples)

    s1 = (s1 - s1.mean(axis=0)) / s1.std(axis=0)
    s2 = (s2 - s2.mean(axis=0)) / s2.std(axis=0)

    f1_ppt = np.zeros((nppts))
    f2_ppt = np.zeros((nppts))

    data = []

    for ii in range(nppts):

        # Jitter f1 around its core point
        f1_ppt[ii] = 4 * np.random.random_sample() + (f1 - 2)

        # f2 is fixed (usually 1/f -> 0Hz)
        f2_ppt[ii] = f2

        X = siggen.generate_signal(f1_ppt[ii], f2_ppt[ii], sample_rate, num_samples)

        data.append(X)

    # data will be [participants, regions, timepoints, 0] so lose the pointless dimension
    data = np.array(data)[:, :, :, 0]

    w1, w2 = siggen.get_connection_weights()
    w1_mat, w2_mat = siggen.get_connection_weights(form='matrix')

    return data, s1, s2, f1_ppt, f2_ppt, w1, w2, w1_mat, w2_mat


def randomise_epoch_data(X_epoch):
    """
    Take a copy of the epoch data, randomise on channels and epochs
    and return the randomised copy
    """
    X_copy = np.zeros_like(X_epoch)
    num_chans = X_copy.shape[0]
    num_segments = X_copy.shape[2]
    num_to_permute = num_chans * num_segments

    samples = sample(list(np.arange(num_to_permute)), num_to_permute)
    sample_idx = 0

    sample_numbers = np.zeros((num_chans, num_segments, 2), np.int)

    for chan in range(X_epoch.shape[0]):
        for epoch in range(num_segments):
            orig_chan = int(samples[sample_idx] / num_segments)
            orig_idx = samples[sample_idx] % num_segments
            X_copy[chan, :, epoch] = X_epoch[orig_chan:orig_chan+1, :, orig_idx]
            sample_numbers[chan, epoch, :] = [orig_chan, orig_idx]
            sample_idx += 1

    return X_copy, sample_numbers


# General configuration for the generation and analysis
sample_rate = 128
num_seconds = 300
model_order = 5
nppts = 20
f1 = 10
f2 = 0
chunk_time = 15
num_perms = 100

# Check that our output file doesn't exist
makedirs(DIR_SIMULATION, exist_ok=True)

outfile = join(DIR_SIMULATION, 'simulation.hdf5')
if isfile(outfile):
    print("E: File {} already exists: aborting".format(outfile))
    exit(1)

with h5py.File(outfile, 'w') as outf:
    num_samples = int(sample_rate * num_seconds)
    delay_vect = np.arange(model_order + 1)
    freq_vect = np.linspace(0, sample_rate/2, 512)

    # Generate simulation data
    # data is [participants, regions, timepoints]
    # f1_ppt and f2_ppt contain the frequencies for the two simulated connectivity
    # patterns
    print("I: Generating simulation data for {} participants".format(nppts))
    data, s1, s2, f1_ppt, f2_ppt, w1, w2, w1_mat, w2_mat = generate_simulation_data(nppts, f1, f2,
                                                                                    sample_rate, num_samples)

    # Save data
    outf.create_dataset('data', data=data)
    outf.create_dataset('f1_ppt', data=f1_ppt)
    outf.create_dataset('f2_ppt', data=f2_ppt)
    outf.create_dataset('w1', data=w1)
    outf.create_dataset('w2', data=w2)
    outf.create_dataset('w1_mat', data=w1_mat)
    outf.create_dataset('w2_mat', data=w2_mat)
    outf.create_dataset('sample_rate', data=sample_rate)
    outf.create_dataset('f1', data=f1)
    outf.create_dataset('f2', data=f2)
    outf.create_dataset('s1', data=s1)
    outf.create_dataset('s2', data=s2)

    # Create somewhere to store models / permutation info
    mgroup = outf.create_group('models')

    # Fit an MVAR model per participant and extract Fourier and Modal metrics

    # Data is participant, channels, time
    segment_len = sample_rate * chunk_time
    num_segments = data.shape[2] // segment_len

    print("I: Segment count for permutations: {}".format(num_segments))

    for ppt in range(nppts):
        print("I: Participant: {}".format(ppt))

        # Create somewhere to store our data
        part_group = mgroup.create_group(str(ppt))

        X_epoch = data[ppt, :, 0:num_segments * segment_len]
        X_epoch = X_epoch.reshape(X_epoch.shape[0], num_segments, segment_len)

        # Data needs to be (channels, time, epochs)
        X_epoch = np.ascontiguousarray(X_epoch.swapaxes(1, 2))

        part_sample_nums = []
        part_dts = []
        part_pfs = []

        # Fit a set of models
        print("I: Perm complete: ", end='')
        for modelnum in range(num_perms):
            # Print some progress information
            if (modelnum + 1) % 10 == 0:
                count = int((modelnum + 1) / num_perms * 100)
                end = '' if (modelnum + 1) < num_perms else '\n'
                print("{}% ".format(count), end=end, flush=True)

            if modelnum == 0:
                # Our first model is our observed statistic so data needs to be
                # in correct order
                X_copy = X_epoch.copy()
                sample_numbers = np.zeros((X_epoch.shape[0], X_epoch.shape[2], 2), np.int)
                sample_numbers[:, :, 0] = np.arange(X_epoch.shape[0])[:, None]
                sample_numbers[:, :, 1] = np.arange(X_epoch.shape[2])[None, :]
            else:
                # Permute the data
                X_copy, sample_numbers = randomise_epoch_data(X_epoch)

            m = sails.VieiraMorfLinearModel.fit_model(X_copy, delay_vect)

            modes = sails.MvarModalDecomposition.initialise(m, sample_rate)

            # If it's the unpermuted model, we need to save some details out
            if modelnum == 0:
                F = sails.FourierMvarMetrics.initialise(m, sample_rate, freq_vect)
                M = sails.ModalMvarMetrics.initialise(m, sample_rate,
                                                      freq_vect, sum_modes=False)

                # Save to the HDF5 file
                F.to_hdf5(part_group.create_group('F'))
                M.to_hdf5(part_group.create_group('M'))
                m.to_hdf5(part_group.create_group('model'))

            # Store sample nums / dts / pfs for all permutations
            part_sample_nums.append(sample_numbers[:, :, :, None])
            part_dts.append(modes.dampening_time)
            part_pfs.append(modes.peak_frequency)

        part_sample_nums = np.concatenate(part_sample_nums, axis=-1)
        part_dts = np.concatenate(part_dts, axis=-1)
        part_pfs = np.concatenate(part_pfs, axis=-1)

        # Now calculate the 99% max dist threshold for the participant and
        # print out the good/bad mode indices
        max_dist_dts = [np.max(part_dts[:, run]) for run in range(part_dts.shape[1])]
        part_thresh = ss.scoreatpercentile(max_dist_dts, 99)

        print("I: Participant {} : 99% threshold {:.3f}".format(ppt, part_thresh))

        part_group.create_dataset('sample_nums', data=part_sample_nums)
        part_group.create_dataset('dts', data=part_dts)
        part_group.create_dataset('pfs', data=part_pfs)
        part_group.create_dataset('thresh', data=part_thresh)

        thresh_modes = [x for x in M.modes.mode_indices if M.modes.dampening_time[x[0]] > part_thresh]

        # Check that we have one real and one complex mode
        # This is a sanity check because, although this simulation is quite robust,
        # it involves random data generation and permutation testing so we would
        # expect very rare false negatives; better to catch them now than later
        good = False
        high_mode = None
        low_mode = None

        if len(thresh_modes) == 2:
            # Look for real mode
            if np.sum([len(x) == 1 for x in thresh_modes]) == 1:
                # Look for complex mode
                if np.sum([len(x) == 2 for x in thresh_modes]) == 1:
                    # We're good!
                    good = True

                    # Store the indices to stop the plotting script having to
                    # recalculate them
                    if len(thresh_modes[0]) == 1:
                        low_mode, high_mode = thresh_modes
                    else:
                        high_mode, low_mode = thresh_modes

                else:
                    print("E: No significant complex mode for participant {}".format(ppt))
            else:
                print("E: No significant real mode for participant {}".format(ppt))

        if not good:
            exit(1)

        # Store the mode indices of the good modes
        part_group.create_dataset('low_mode', data=low_mode)
        part_group.create_dataset('high_mode', data=high_mode)
