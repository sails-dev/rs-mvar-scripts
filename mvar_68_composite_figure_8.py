#!/usr/bin/python3

from sys import argv, exit
from os.path import join

from PIL import Image, ImageDraw, ImageFont

from mvarconfig import (DIR_FIGURES, FONT,
                        DEFAULT_MODEL_ORDER)

if len(argv) not in (1, 2):
    print("mvar_68_composite_figure_8.py [MODEL_ORDER]")
    exit(1)

if len(argv) == 2:
    model_order = int(argv[1])
else:
    # Default model order
    model_order = DEFAULT_MODEL_ORDER

print("I: Using model order {}".format(model_order))

fig_dir = join(DIR_FIGURES, str(model_order))

# Composite Figure 7
DPI = 600

LABEL_COLOUR = (85, 85, 85, 255)

# Figure 7
fig_pixels = [int(7.5 * DPI), int(8.5 * DPI)]

PC_X_OFFSET = [0, fig_pixels[0] // 2]

# SURFACE_X_OFFSET is calculated below as we need the image size
SURFACE_Y_OFFSET = 150

ABS_HIST_X_OFFSET = 0
ABS_HIST_Y_OFFSET = int(1.7 * DPI)

REL_HIST_X_OFFSET = int(1.875 * DPI)
REL_HIST_Y_OFFSET = int(1.7 * DPI)

ABS_SCATTER_X_OFFSET = 0
ABS_SCATTER_Y_OFFSET = ABS_HIST_Y_OFFSET + int(1.875 * DPI)

REL_SCATTER_X_OFFSET = int(1.875 * DPI)
REL_SCATTER_Y_OFFSET = REL_HIST_Y_OFFSET + int(1.875 * DPI)

# Although we only need the alpha one for the main paper (that
# band was our interest), we produce beta and theta ones for
# supplementary material so that people know we aren't hiding
# anything
for band in ['alpha', 'beta', 'theta']:
    # Create a blank figure
    f = Image.new('RGBA', fig_pixels, 'white')

    # Start with the surface plots
    pc1_surf = Image.open(join(fig_dir, '50_hcp_modalpca_{}_component_radial_1.png'.format(band)))
    pc2_surf = Image.open(join(fig_dir, '50_hcp_modalpca_{}_component_radial_2.png'.format(band)))

    # Needs to be calculated here as we need the image size
    SURFACE_X_OFFSET = (PC_X_OFFSET[1] - pc1_surf.size[0]) // 2

    f.paste(pc2_surf, (PC_X_OFFSET[1] + SURFACE_X_OFFSET, SURFACE_Y_OFFSET))
    f.paste(pc1_surf, (PC_X_OFFSET[0] + SURFACE_X_OFFSET, SURFACE_Y_OFFSET))

    # Note that the ordering here is careful to avoid overlapping incorrectly
    # Don't change without checking that it still works

    # Add the relative histograms
    pc1_rel_hist = Image.open(join(fig_dir, '51_pca_part_{}_rel_hist_1.png'.format(band)))
    pc2_rel_hist = Image.open(join(fig_dir, '51_pca_part_{}_rel_hist_2.png'.format(band)))

    f.paste(pc2_rel_hist, (PC_X_OFFSET[1] + REL_HIST_X_OFFSET, REL_HIST_Y_OFFSET))
    f.paste(pc1_rel_hist, (PC_X_OFFSET[0] + REL_HIST_X_OFFSET, REL_HIST_Y_OFFSET))

    # Add the absolute histograms
    pc1_abs_hist = Image.open(join(fig_dir, '51_pca_part_{}_abs_hist_1.png'.format(band)))
    pc2_abs_hist = Image.open(join(fig_dir, '51_pca_part_{}_abs_hist_2.png'.format(band)))

    f.paste(pc2_abs_hist, (PC_X_OFFSET[1] + ABS_HIST_X_OFFSET, ABS_HIST_Y_OFFSET))
    f.paste(pc1_abs_hist, (PC_X_OFFSET[0] + ABS_HIST_X_OFFSET, ABS_HIST_Y_OFFSET))

    # Add the relative scatter plots
    pc1_rel_scatter = Image.open(join(fig_dir, '51_pca_part_{}_rel_scatter_1.png'.format(band)))
    pc2_rel_scatter = Image.open(join(fig_dir, '51_pca_part_{}_rel_scatter_2.png'.format(band)))

    f.paste(pc1_rel_scatter, (PC_X_OFFSET[0] + REL_SCATTER_X_OFFSET, REL_SCATTER_Y_OFFSET))
    f.paste(pc2_rel_scatter, (PC_X_OFFSET[1] + REL_SCATTER_X_OFFSET, REL_SCATTER_Y_OFFSET))

    # Add the absolute scatter plots
    pc1_abs_scatter = Image.open(join(fig_dir, '51_pca_part_{}_abs_scatter_1.png'.format(band)))
    pc2_abs_scatter = Image.open(join(fig_dir, '51_pca_part_{}_abs_scatter_2.png'.format(band)))

    f.paste(pc1_abs_scatter, (PC_X_OFFSET[0] + ABS_SCATTER_X_OFFSET, ABS_SCATTER_Y_OFFSET))
    f.paste(pc2_abs_scatter, (PC_X_OFFSET[1] + ABS_SCATTER_X_OFFSET, ABS_SCATTER_Y_OFFSET))

    # Add some text
    font = ImageFont.truetype(FONT, 180)

    draw = ImageDraw.Draw(f)

    textsize = draw.textsize("PC1", font=font)

    draw.text((PC_X_OFFSET[1] // 2 - (textsize[0] // 2), 50),
              'PC1', font=font, fill=LABEL_COLOUR)

    draw.text((PC_X_OFFSET[1] + (PC_X_OFFSET[1] // 2) - (textsize[0] // 2), 50),
              'PC2', font=font, fill=LABEL_COLOUR)

    font = ImageFont.truetype(FONT, 70)

    textsize = draw.textsize("Frequency", font=font)
    draw.text((int((1.05) * DPI) - (textsize[0] // 2), REL_HIST_Y_OFFSET + 20),
              'Frequency',
              font=font, fill=LABEL_COLOUR)

    draw.text((PC_X_OFFSET[1] + int((1.05) * DPI) - (textsize[0] // 2), REL_HIST_Y_OFFSET + 20),
              'Frequency',
              font=font, fill=LABEL_COLOUR)

    textsize = draw.textsize("Relative Frequency", font=font)

    draw.text((int((1.875 + 1.1) * DPI) - (textsize[0] // 2), REL_HIST_Y_OFFSET + 20),
              'Relative Frequency',
              font=font, fill=LABEL_COLOUR)

    draw.text((PC_X_OFFSET[1] + int((1.875 + 1.1) * DPI) - (textsize[0] // 2), REL_HIST_Y_OFFSET + 20),
              'Relative Frequency',
              font=font, fill=LABEL_COLOUR)

    LINE_X_OFFSET = PC_X_OFFSET[1] + 80
    draw.line((LINE_X_OFFSET, 150,
               LINE_X_OFFSET, fig_pixels[1] - 150),
              fill=LABEL_COLOUR, width=5)

    # Add some labels
    font = ImageFont.truetype(FONT, 120)

    draw.text((PC_X_OFFSET[0] + 50, 50), 'A', font=font, fill=LABEL_COLOUR)
    draw.text((PC_X_OFFSET[0] + 50, 1000), 'B', font=font, fill=LABEL_COLOUR)
    draw.text((PC_X_OFFSET[0] + 50, 2200), 'C', font=font, fill=LABEL_COLOUR)

    draw.text((PC_X_OFFSET[1] + 125, 50), 'A', font=font, fill=LABEL_COLOUR)
    draw.text((PC_X_OFFSET[1] + 125, 1000), 'B', font=font, fill=LABEL_COLOUR)
    draw.text((PC_X_OFFSET[1] + 125, 2200), 'C', font=font, fill=LABEL_COLOUR)

    # Now create a non-alpha version to save
    final = Image.new("RGB", f.size, (255, 255, 255))
    final.paste(f, mask=f.split()[3])

    if band == 'alpha':
        filename = '68_figure8.png'
    else:
        filename = '68_supp_{}.png'.format(band)

    final.save(join(fig_dir, filename), dpi=(DPI, DPI))


# One big supplementary plot for surfaces
fig_pixels = [int(7.5 * DPI), int(3.8 * DPI)]

labels = {'alpha': "α", 'beta': "β", 'theta': "θ"}

f = Image.new('RGBA', fig_pixels, 'white')

font = ImageFont.truetype(FONT, 120)

draw = ImageDraw.Draw(f)

for idx, band in enumerate(['theta', 'alpha', 'beta']):
    # Start with the surface plots
    pc1_surf = Image.open(join(fig_dir, '50_hcp_modalpca_{}_component_radial_1.png'.format(band)))
    pc2_surf = Image.open(join(fig_dir, '50_hcp_modalpca_{}_component_radial_2.png'.format(band)))
    pc3_surf = Image.open(join(fig_dir, '50_hcp_modalpca_{}_component_radial_3.png'.format(band)))
    pc4_surf = Image.open(join(fig_dir, '50_hcp_modalpca_{}_component_radial_4.png'.format(band)))

    # Scale them down to 75% of their original size
    new_size = (int(0.75 * pc1_surf.size[0]), int(0.75 * pc1_surf.size[1]))
    pc1_surf = pc1_surf.resize(new_size)
    pc2_surf = pc2_surf.resize(new_size)
    pc3_surf = pc3_surf.resize(new_size)
    pc4_surf = pc4_surf.resize(new_size)

    x_start = 50
    y_start = 100

    x_offset = new_size[0] + 85
    y_offset = new_size[1] + 200

    # Needs to be calculated here as we need the image size
    f.paste(pc4_surf, (x_start + x_offset*3, y_start + y_offset*idx))
    f.paste(pc3_surf, (x_start + x_offset*2, y_start + y_offset*idx))
    f.paste(pc2_surf, (x_start + x_offset*1, y_start + y_offset*idx))
    f.paste(pc1_surf, (x_start + x_offset*0, y_start + y_offset*idx))

    draw.text((x_start + x_offset*0, int(y_start / 4.5) + y_offset*idx),
              labels[band], font=font, fill=LABEL_COLOUR)

    if idx == 0:
        textsize = draw.textsize("PC1", font=font)

        for k in range(4):
            draw.text((x_start + int(x_offset * (k + 0.465)) - textsize[0] // 2,
                       y_start - textsize[1] // 2),
                      'PC{}'.format(k+1),
                      font=font, fill=LABEL_COLOUR)

    if idx < 2:
        ypos = y_start + int(y_offset*(idx + 0.88))
        draw.line((x_start, ypos, fig_pixels[0] - x_start, ypos),
                  fill=LABEL_COLOUR, width=5)

# Now create a non-alpha version to save
final = Image.new("RGB", f.size, (255, 255, 255))
final.paste(f, mask=f.split()[3])

filename = '68_supp_theta_alpha_beta.png'
final.save(join(fig_dir, filename), dpi=(DPI, DPI))
