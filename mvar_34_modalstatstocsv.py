#!/usr/bin/python3

from sys import argv, exit
from os.path import join

import h5py

from anamnesis import obj_from_hdf5group
from sails import PCA  # noqa

from mvarconfig import (DIR_MODALSTATS, DEFAULT_MODEL_ORDER)

if len(argv) not in (1, 2):
    print("mvar_34_modalstatstocsv.py [MODEL_ORDER]")
    exit(1)

if len(argv) == 2:
    model_order = int(argv[1])
else:
    # Default model order
    model_order = DEFAULT_MODEL_ORDER

print("I: Using model order {}".format(model_order))

base_dir = join(DIR_MODALSTATS, str(model_order))

# We will extract the data for each of these frequency bands
FREQ_BANDS = ['alpha', 'theta', 'beta', 'low', 'high']

for bandname in FREQ_BANDS:
    print("I: Loading data for {}".format(bandname))

    # Load our summary data
    inpath = join(base_dir, '33_hcp_pca_{}_params.hdf5'.format(bandname))

    # Prepare our output file
    outpath = join(base_dir, '34_hcp_pca_{}_freq_data.csv'.format(bandname))

    f = h5py.File(inpath, 'r')

    pca = obj_from_hdf5group(f['pca'])
    part_nums = f['part_nums'][...]
    run_nums = f['run_nums'][...]
    freqs = f['freqs'][...]

    f.close()

    print("I: Saving CSV for {}".format(bandname))

    with open(outpath, 'w') as outf:
        outf.write('PC,Participant,Run,Score,Frequency\n')
        for pc in range(pca.npcs):
            for idx, freq in enumerate(freqs):
                dat = []
                dat.append(pc + 1)
                dat.append(int(part_nums[idx]))
                dat.append(int(run_nums[idx]))
                dat.append(pca.scores[idx, pc])
                dat.append(freq)

                outf.write('{:d},{:d},{:d},{:.6e},{:.6f}\n'.format(*dat))
