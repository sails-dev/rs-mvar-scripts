#!/usr/bin/python3

import itertools

from os import makedirs
from os.path import join

import h5py

import numpy as np
import scipy.signal as signal
import matplotlib.pyplot as plt

from matplotlib import gridspec

from anamnesis import obj_from_hdf5group
import sails

import warnings

from mvarconfig import (DIR_SIMULATION, DIR_FIGURES)

# Our version of scipy irritatingly has FutureWarning's triggered in it
# so hide them
warnings.simplefilter(action='ignore', category=FutureWarning)

plt.style.use('ggplot')
plt.rcParams.update({'font.size': 7})


def remaining_modes(M, used_modes):
    """
    Return mode indicies which are not in the used_modes list.

    Warning: this does not retain the order of the modes in relation
    to the pole list
    """

    km = set(M.modes.mode_indices)
    um = set(used_modes)

    if km.intersection(um) != um:
        raise Exception("All known modes not in available modes")

    return list(km - um)


def decorate(ax, xlab=True):
    """
    Decorate an axes with appropriate settings
    """
    if xlab:
        ax.set_xlabel('Frequency (Hz)')

    ax.set_ylabel('PSD')

    ax.grid(True)

    ax.set_xlim([0, 25])
    ax.set_ylim(0, 21)

    for tag in ['right', 'top']:
        ax.spines[tag].set_visible(False)


def tidy_up_axes(fig, plt_height, plt_across, nareas):
    plt.figure(fig.number)

    ppt = 0
    # Sort out xticks etc
    for i in range(plt_height):
        for j in range(plt_across):
            plt.subplot(plt_height, plt_across, ppt+1)
            if j == 0:
                plt.yticks(range(nareas), range(1, nareas+1))
            else:
                plt.yticks([], [])

            if i == plt_height - 1:
                # Bottom row
                plt.xticks(range(nareas), range(1, nareas+1))
            else:
                plt.xticks([], [])

            ppt += 1


# Configuration

# Note to plot for spectrum plot
node_to_plot = 6

# Number of plots on a given row
plt_across = 5

# Number of seconds to plot on timeseries plot
timeseries_seconds = 5

# Check that we have an output directory
makedirs(DIR_FIGURES, exist_ok=True)

# Find and load the simulation data
f = h5py.File(join(DIR_SIMULATION, 'simulation.hdf5'), 'r')

sample_rate = float(f['sample_rate'][...])

# data is the overall dataset: (ppt, node, timepoints)
data = f['data'][...]
nppts = data.shape[0]
nareas = data.shape[1]

# s1 and s2 are the base signals used in simulation
s1 = f['s1'][...]
s2 = f['s2'][...]

# w1 and w2 are the connection weights between nodes in the network
w1 = f['w1'][...]
w2 = f['w2'][...]

# w1 and w2 are the matrix forms of the weights
w1_mat = f['w1_mat'][...]
w2_mat = f['w2_mat'][...]

# f1 and f2 are the centre frequencies
f1 = float(f['f1'][...])
f2 = float(f['f2'][...])

# f1_ppt and f2_ppt contain the randomly chosen frequencies for the signals for
# each participant
f1_ppt = f['f1_ppt'][...]
f2_ppt = f['f2_ppt'][...]

# Need to extract delay vector; use 0th model to do it
delay_vect = f['models']['0']['model']['delay_vect'][...]

freq_vect = np.linspace(0, sample_rate / 2, 512)

# Set up indices in the frequency vector around our regions of interest
f1_inds = np.where((freq_vect > f1-4) & (freq_vect < f1+4))[0]
f2_inds = np.where((freq_vect > f2-4) & (freq_vect < f2+4))[0]

print("I: Part 1 - loading and preparing data")

plt_height = int(np.ceil(nppts / plt_across))

# Set up our plots
figsize = (12, 12)

plot_timeseries = plt.figure(figsize=(7.5, 2.5))
plot_modes = plt.figure(figsize=figsize)
plot_modal_low = plt.figure(figsize=figsize)
plot_modal_high = plt.figure(figsize=figsize)
plot_modal_noise = plt.figure(figsize=figsize)
plot_fourier_low = plt.figure(figsize=figsize)
plot_fourier_high = plt.figure(figsize=figsize)

modal_low_netmats = []
modal_high_netmats = []
modal_noise_netmats = []

modal_low_freqs = []
modal_high_freqs = []

modal_low_pos = []
modal_high_pos = []

fourier_low_netmats = []
fourier_high_netmats = []

modal_low_spectra = np.zeros((nppts, len(freq_vect)), np.complex)
modal_high_spectra = np.zeros((nppts, len(freq_vect)), np.complex)

model_data = []

fourier_spectra = np.zeros((nppts, len(freq_vect)))

# Contains correlations between:
#  0: (fourier_low, low weights)
#  1: (fourier_high, high weights)
#  2: (modal_low, low weights)
#  3: (modal_high, high weights)
model_correlations = np.zeros((nppts, 4))

# Contains correlations between modal noise modes and
#  0: (fourier low)
#  1: (fourier high)
#  2: (modal low)
#  3: (modal high)
noise_correlations = np.zeros((nppts, 4))

all_dt_thresh = []

for ppt in range(nppts):
    print("I: Processing participant: {}".format(ppt))

    m = obj_from_hdf5group(f['models'][str(ppt)]['model'])
    M = obj_from_hdf5group(f['models'][str(ppt)]['M'])
    F = obj_from_hdf5group(f['models'][str(ppt)]['F'])

    model_data.append((m, F, M))

    # Thresholds and specific modes
    this_thresh = float(f['models'][str(ppt)]['thresh'][...])

    low_mode = tuple(f['models'][str(ppt)]['low_mode'][...].tolist())
    high_mode = tuple(f['models'][str(ppt)]['high_mode'][...].tolist())
    good_modes = [low_mode, high_mode]
    all_dt_thresh.append(this_thresh)

    print("I: Participant {}: Threshold: {:.3f}".format(ppt, this_thresh))

    # Check whether we have one 0ish mode and one non-zeroish mode
    low_freq = M.modes.peak_frequency[low_mode[0]][0]
    modal_low_pos.append(low_mode)
    modal_low_freqs.append(low_freq)

    high_freq = M.modes.peak_frequency[high_mode[0]][0]
    modal_high_pos.append(high_mode)
    modal_high_freqs.append(high_freq)

    pole_indices = list(itertools.chain(*good_modes))

    # Plot the modes which we extract for each participant
    # including the f1 frequency
    plt.figure(plot_modes.number)
    plt.subplot(plt_height, plt_across, ppt+1)
    plt.scatter(np.squeeze(M.modes.peak_frequency),
                np.squeeze(M.modes.dampening_time), color='b')
    plt.scatter(np.squeeze(M.modes.peak_frequency)[pole_indices],
                np.squeeze(M.modes.dampening_time)[pole_indices], color='r')
    yl = plt.ylim()
    plt.vlines(f1_ppt[ppt], yl[0], yl[1])

    # Plot the low mode structure
    m_low = sails.ModalMvarMetrics.initialise_from_modes(m, M.modes, sample_rate,
                                                         np.array([low_freq]),
                                                         [low_mode[0]],
                                                         sum_modes=True)

    mln = m_low.PSD[:, :, 0, 0]
    plt.figure(plot_modal_low.number)
    plt.subplot(plt_height, plt_across, ppt+1)
    plt.imshow(mln, interpolation='nearest', cmap='inferno')
    plt.grid(False)
    modal_low_netmats.append(mln)
    model_correlations[ppt, 2] = np.corrcoef(mln.reshape(-1), np.reshape(w2_mat, -1))[0, 1]

    # Plot the high mode structure
    m_high = sails.ModalMvarMetrics.initialise_from_modes(m, M.modes, sample_rate,
                                                          np.array([high_freq]),
                                                          high_mode,
                                                          sum_modes=True)

    mhn = m_high.PSD[:, :, 0, 0]
    plt.figure(plot_modal_high.number)
    plt.subplot(plt_height, plt_across, ppt+1)
    plt.imshow(mhn, interpolation='nearest', cmap='inferno')
    plt.grid(False)
    modal_high_netmats.append(mhn)
    model_correlations[ppt, 3] = np.corrcoef(mhn.reshape(-1), np.reshape(w1_mat, -1))[0, 1]

    for idx in high_mode:
        modal_high_spectra[ppt, :] += M.S[node_to_plot, node_to_plot, :, idx]
    modal_high_spectra[ppt, :] /= len(high_mode)

    # Plot the structure for the "noise" modes
    used_modes = [high_mode] + [low_mode]
    rem_modes = remaining_modes(M, used_modes)
    rem_poles = list(itertools.chain(*rem_modes))

    m_noise = sails.ModalMvarMetrics.initialise_from_modes(m, M.modes, sample_rate,
                                                           freq_vect,
                                                           rem_poles,
                                                           sum_modes=True)

    plt.figure(plot_modal_noise.number)
    plt.subplot(plt_height, plt_across, ppt+1)
    noise_psd = m_noise.PSD[:, :, :, 0].sum(axis=2)
    plt.imshow(noise_psd, interpolation='nearest', cmap='inferno')
    plt.grid(False)
    modal_noise_netmats.append(noise_psd)

    for idx in low_mode:
        modal_low_spectra[ppt, :] += M.S[node_to_plot, node_to_plot, :, idx]
    modal_low_spectra[ppt, :] /= len(low_mode)

    # Plot the fourier matrices
    fln = F.PSD[:, :, f2_inds, 0].mean(axis=-1).squeeze()
    fourier_low_netmats.append(fln)
    plt.figure(plot_fourier_low.number)
    plt.subplot(plt_height, plt_across, ppt+1)
    plt.imshow(fln, interpolation='nearest', cmap='inferno')
    plt.grid(False)
    model_correlations[ppt, 0] = np.corrcoef(fln.reshape(-1), np.reshape(w2_mat, -1))[0, 1]

    fhn = F.PSD[:, :, f1_inds, 0].mean(axis=-1).squeeze()
    plt.figure(plot_fourier_high.number)
    plt.subplot(plt_height, plt_across, ppt+1)
    plt.imshow(fhn, interpolation='nearest', cmap='inferno')
    plt.grid(False)
    fourier_high_netmats.append(fhn)
    model_correlations[ppt, 1] = np.corrcoef(fhn.reshape(-1), np.reshape(w1_mat, -1))[0, 1]

    fourier_spectra[ppt, :] = np.abs(F.S[node_to_plot, node_to_plot, :, 0])

    noise_correlations[ppt, 0] = np.corrcoef(fln.reshape(-1), noise_psd.reshape(-1))[0, 1]
    noise_correlations[ppt, 1] = np.corrcoef(fhn.reshape(-1), noise_psd.reshape(-1))[0, 1]
    noise_correlations[ppt, 2] = np.corrcoef(mln.reshape(-1), noise_psd.reshape(-1))[0, 1]
    noise_correlations[ppt, 3] = np.corrcoef(mhn.reshape(-1), noise_psd.reshape(-1))[0, 1]


print("I: Part 2 - saving summary figures")
plt.figure(plot_modes.number)
plt.savefig(join(DIR_FIGURES, '91_sim_modes_poles.png'), dpi=600)

plt.figure(plot_modal_low.number)
tidy_up_axes(plot_modal_low, plt_height, plt_across, nareas)
plt.savefig(join(DIR_FIGURES, '91_sim_modes_low.png'), dpi=600)

plt.figure(plot_modal_high.number)
tidy_up_axes(plot_modal_high, plt_height, plt_across, nareas)
plt.savefig(join(DIR_FIGURES, '91_sim_modes_high.png'), dpi=600)

plt.figure(plot_modal_noise.number)
tidy_up_axes(plot_modal_noise, plt_height, plt_across, nareas)
plt.savefig(join(DIR_FIGURES, '91_sim_modes_noise.png'), dpi=600)

plt.figure(plot_fourier_low.number)
tidy_up_axes(plot_fourier_low, plt_height, plt_across, nareas)
plt.savefig(join(DIR_FIGURES, '91_sim_fourier_low.png'), dpi=600)

plt.figure(plot_fourier_high.number)
tidy_up_axes(plot_fourier_high, plt_height, plt_across, nareas)
plt.savefig(join(DIR_FIGURES, '91_sim_fourier_high.png'), dpi=600)

modal_low_netmats = np.array(modal_low_netmats)
modal_high_netmats = np.array(modal_high_netmats)
modal_noise_netmats = np.array(modal_noise_netmats)

fourier_low_netmats = np.array(fourier_low_netmats)
fourier_high_netmats = np.array(fourier_high_netmats)

plt.figure()
plt.imshow(modal_low_netmats.mean(axis=0), interpolation='nearest', cmap='inferno')
plt.savefig(join(DIR_FIGURES, '91_sim_modes_low_avg.png'), dpi=600)

plt.figure()
plt.imshow(modal_high_netmats.mean(axis=0), interpolation='nearest', cmap='inferno')
plt.savefig(join(DIR_FIGURES, '91_sim_modes_high_avg.png'), dpi=600)

plt.figure()
plt.imshow(modal_noise_netmats.mean(axis=0), interpolation='nearest', cmap='inferno')
plt.savefig(join(DIR_FIGURES, '91_sim_modes_noise_avg.png'), dpi=600)

plt.figure()
plt.imshow(fourier_low_netmats.mean(axis=0), interpolation='nearest', cmap='inferno')
plt.savefig(join(DIR_FIGURES, '91_sim_fourier_low_avg.png'), dpi=600)

plt.figure()
plt.imshow(fourier_high_netmats.mean(axis=0), interpolation='nearest', cmap='inferno')
plt.savefig(join(DIR_FIGURES, '91_sim_fourier_high_avg.png'), dpi=600)


#####################################################################################################
print("I: Part 3 - timeseries plot")

sf, pxx_s1 = signal.welch(s1.T, fs=sample_rate, nperseg=128, nfft=512)
sf, pxx_s2 = signal.welch(s2.T, fs=sample_rate, nperseg=128, nfft=512)

timeseries_samples = int(sample_rate * timeseries_seconds)

plt.figure(plot_timeseries.number)
plt.axes([.15, .8, .65, .15], frameon=False)
plt.plot(s1[:timeseries_samples], 'r')
plt.plot(s2[:timeseries_samples]+7.5, 'b')
plt.xlim(-10, timeseries_samples)
yl = plt.ylim()
plt.xticks([])
plt.yticks([])

x_label_pos = -2.25
x_min = -1.75

plt.axes([.05, .8, .1, .15], frameon=False)
plt.text(x_label_pos, 7.5, 'Mode 1', verticalalignment='center', color='b')
plt.text(x_label_pos, 0,   'Mode 2', verticalalignment='center', color='r')
plt.xlim(x_label_pos, 1)
plt.ylim(*yl)
plt.xticks([])
plt.yticks([])
plt.fill_between(np.linspace(0, 2, 257), np.zeros((257,)), 20*pxx_s1[0, :], color='r')
plt.fill_between(np.linspace(0, 2, 257), np.zeros((257,))+7.5, 20*pxx_s2[0, :]+7.5, color='b')
plt.xlim(x_min, 1)
plt.text(-3.0, 8.0, 'A', fontsize=14)
plt.text(19.0, 8.0, 'B', fontsize=14)

# Use the 0th participants data as an exemplar
plt.axes([.15, .1, .65, .7], frameon=False)
plt.plot(data[0, :, :timeseries_samples].T + np.arange(nareas) * 7.5, 'k', linewidth=.5)
plt.hlines(np.arange(nareas)*7.5, -10, timeseries_samples, linewidth=.2)
plt.xlim(-10, timeseries_samples)
plt.xticks([])
plt.yticks([])
yl = plt.ylim()

plt.axes([.05, .1, .1, .7], frameon=False)
plt.barh(np.arange(nareas)*7.5+1, w1, color='r', height=2)
plt.barh(np.arange(nareas)*7.5-1, w2, color='b', height=2)
plt.xlim(x_min, 1)
plt.ylim(*yl)

for ii in range(nareas):
    plt.text(x_label_pos, 7.5*ii, 'Node {0}'.format(ii+1), verticalalignment='center')

plt.xticks([])
plt.yticks([])

plt.axes([.85, .6, .13, .34])
plt.pcolormesh(w2_mat, cmap='Blues')
plt.xticks(np.arange(nareas, 0, -1)-.5, [''] * nareas, fontsize=6)
plt.yticks(np.arange(nareas, 0, -1)-.5, np.arange(nareas, 0, -1), fontsize=6)
plt.axis('scaled')

plt.axes([.85, .2, .13, .34])
plt.pcolormesh(w1_mat, cmap='Reds')
plt.xticks(np.arange(nareas, 0, -1)-.5, np.arange(nareas, 0, -1), fontsize=6)
plt.yticks(np.arange(nareas, 0, -1)-.5, np.arange(nareas, 0, -1), fontsize=6)
plt.axis('scaled')

plt.savefig(join(DIR_FIGURES, '91_timeseries.png'), dpi=600)

#####################################################################################################
# Make some combined figures for the paper

# Elsevier double column width is 190mm
# We want 1 x (3/4) ratio in inches, so 7.5inches x 5.625 inches

print("I: Part 4 - combined plots")
fig = plt.figure(figsize=(7.5, 6.2))

plt.subplot(3, 4, 1)
plt.plot(freq_vect, np.abs(fourier_spectra.T), 'k', alpha=0.2)
plt.plot(freq_vect, np.abs(fourier_spectra.T).mean(axis=1), 'k', linewidth=2)
plt.title('Full Spectrum')
decorate(plt.gca(), xlab=False)
plt.text(-5.0, plt.ylim()[1] + 1.5, 'A', fontsize=14)

plt.subplot(3, 4, 5)
plt.plot(freq_vect, np.abs(fourier_spectra.T), 'k', linewidth=0.2)
plt.plot(freq_vect[f1_inds], np.abs(fourier_spectra.T)[f1_inds, :], 'r', linewidth=0.4)
plt.plot(freq_vect[f2_inds], np.abs(fourier_spectra.T)[f2_inds, :], 'b', linewidth=0.4)
decorate(plt.gca(), xlab=False)
plt.title('Fourier Spectrum')
plt.text(-5.0, plt.ylim()[1] + 1.5, 'B', fontsize=14)

plt.subplot(3, 4, 9)
plt.grid(False)
plt.plot(freq_vect, np.abs(modal_low_spectra.T), 'b', linewidth=0.4)
plt.plot(freq_vect, np.abs(modal_high_spectra.T), 'r', linewidth=0.4)
plt.title('Modal Spectrum')
decorate(plt.gca(), xlab=True)
plt.text(-5.0, plt.ylim()[1] + 1.5, 'C', fontsize=14)

plt.subplot(3, 4, 2)
plt.pcolormesh(w2_mat, cmap='Blues')
plt.xticks(np.arange(nareas, 0, -1)-.5, np.arange(nareas, 0, -1))
plt.yticks(np.arange(nareas, 0, -1)-.5, np.arange(nareas, 0, -1))
plt.title('Original F1')
plt.axis('scaled')
plt.text(-2.0, plt.ylim()[1] + 1.2, 'D', fontsize=14)

plt.subplot(3, 4, 3)
plt.pcolormesh(w1_mat, cmap='Reds')
plt.xticks(np.arange(nareas, 0, -1)-.5, np.arange(nareas, 0, -1))
plt.yticks(np.arange(nareas, 0, -1)-.5, np.arange(nareas, 0, -1))
plt.title('Original F2')
plt.axis('scaled')

plt.subplot(3, 4, 6)
plt.pcolormesh(fourier_low_netmats.mean(axis=0), cmap='Blues')
plt.xticks(np.arange(nareas, 0, -1)-.5, np.arange(nareas, 0, -1))
plt.yticks(np.arange(nareas, 0, -1)-.5, np.arange(nareas, 0, -1))
plt.title('Fourier F1')
plt.axis('scaled')
plt.text(-2.0, plt.ylim()[1] + 1.2, 'E', fontsize=14)

plt.subplot(3, 4, 7)
plt.pcolormesh(fourier_high_netmats.mean(axis=0), cmap='Reds')
plt.xticks(np.arange(nareas, 0, -1)-.5, np.arange(nareas, 0, -1))
plt.yticks(np.arange(nareas, 0, -1)-.5, np.arange(nareas, 0, -1))
plt.title('Fourier F2')
plt.axis('scaled')

plt.subplot(3, 4, 10)
plt.pcolormesh(modal_low_netmats.mean(axis=0), cmap='Blues')
plt.xticks(np.arange(nareas, 0, -1)-.5, np.arange(nareas, 0, -1))
plt.yticks(np.arange(nareas, 0, -1)-.5, np.arange(nareas, 0, -1))
plt.title('Modal M1')
plt.axis('scaled')
plt.text(-2.0, plt.ylim()[1] + 1.1, 'F', fontsize=14)

plt.subplot(3, 4, 11)
plt.pcolormesh(modal_high_netmats.mean(axis=0), cmap='Reds')
plt.xticks(np.arange(nareas, 0, -1)-.5, np.arange(nareas, 0, -1))
plt.yticks(np.arange(nareas, 0, -1)-.5, np.arange(nareas, 0, -1))
plt.title('Modal M2')
plt.axis('scaled')

plt.subplot(3, 4, 12)
plt.pcolormesh(modal_noise_netmats.mean(axis=0), cmap='bone_r')
plt.xticks(np.arange(nareas, 0, -1)-.5, np.arange(nareas, 0, -1))
plt.yticks(np.arange(nareas, 0, -1)-.5, np.arange(nareas, 0, -1))
plt.title('Noise modes')
plt.axis('scaled')

# Add in the boxplot of correlations
plt.subplot(3, 4, 4)
bplt = plt.boxplot(model_correlations, patch_artist=True)
plt.xticks(np.arange(1, 5), ['F1', 'F2', 'M1', 'M2'])
plt.ylabel('Correlation Coefficient')
plt.ylim([0.5, 1.05])
plt.grid(True)

bplt['boxes'][0].set_facecolor('b')
bplt['boxes'][1].set_facecolor('r')
bplt['boxes'][2].set_facecolor('b')
bplt['boxes'][3].set_facecolor('r')
plt.title('Structure Correlations')
plt.text(-0.6, 1.09, 'G', fontsize=14)

# Add in the boxplot of noise correlations
plt.subplot(3, 4, 8)
bplt = plt.boxplot(noise_correlations, patch_artist=True)
plt.xticks(np.arange(1, 5), ['F1', 'F2', 'M1', 'M2'])
plt.ylabel('Correlation Coefficient')
plt.xlabel('Method')
plt.ylim([0, 1])
plt.grid(True)

bplt['boxes'][0].set_facecolor('b')
bplt['boxes'][1].set_facecolor('r')
bplt['boxes'][2].set_facecolor('b')
bplt['boxes'][3].set_facecolor('r')
plt.title('Noise Correlations')
plt.text(-0.6, 1.09, 'H', fontsize=14)

plt.subplots_adjust(left=0.06, right=0.98, top=0.95, bottom=0.07, hspace=0.48, wspace=.45)
plt.savefig(join(DIR_FIGURES, '91_sim_netmats.png'), dpi=600, bbox_inches='tight')


#####################################################################################################
# And the spectral figure

print("I: Part 5 - spectral figures")

fig = plt.figure(figsize=(16, 12))

gs = gridspec.GridSpec(3, 2, width_ratios=[3, 1])

# Original spectra spectrum
plt.subplot(gs[0])

plt.plot(freq_vect, np.abs(fourier_spectra.T), 'k', alpha=0.1)
plt.plot(freq_vect, np.abs(fourier_spectra.T).mean(axis=1), 'k')
plt.grid(True)
plt.xlim([-2, 42])
plt.title('Original Spectrum')
plt.xticks([], [])
plt.ylabel('Power (a.u.)')


# Fourier spectra
plt.subplot(gs[2])

plt.plot(freq_vect, np.abs(fourier_spectra.T), 'k', linewidth=0.2)
plt.plot(freq_vect[f1_inds], np.abs(fourier_spectra.T)[f1_inds, :], 'r', linewidth=0.4)
plt.plot(freq_vect[f2_inds], np.abs(fourier_spectra.T)[f2_inds, :], 'b', linewidth=0.4)
plt.grid(True)
plt.xlim([-2, 42])
plt.title('Fourier Filtered Spectra')
plt.xticks([], [])
plt.ylabel('Power (a.u.)')

# Modal spectra
plt.subplot(gs[4])
plt.grid(False)
ax1 = plt.gca()
ax1.grid(False)
ax2 = ax1.twinx()
ax2.grid(True)
ax1.plot(freq_vect, np.abs(modal_low_spectra.T), 'b', linewidth=0.4)
ax2.plot(freq_vect, np.abs(modal_high_spectra.T), 'r', linewidth=0.4)
plt.xlim([-2, 42])
plt.title('Modal Spectra')
ax1.set_ylabel('Power (a.u.): Low')
ax2.set_ylabel('Power (a.u.): High')
ax1.set_xlabel('Frequency (Hz)')

# Peak frequency estimates
plt.subplot(gs[1])
plt.plot(f1_ppt, modal_high_freqs, '*r')
plt.plot(np.arange(24), np.arange(24), 'k--')
plt.grid(True)
plt.xlabel('Simulated Frequency')
plt.ylabel('Modal Estimated Frequency')
plt.ylim(-4, 24)
plt.xlim(-4, 24)
plt.title('Peak Frequency Estimates')

# Peak frequency estimates
# Create a pole plot for one participant (the last one we calculated)
# in which we highlight the indices of the poles in the appropriate colours

# low_mode and high_mode will still be set to the appropriate modes
plt.subplot(gs[5])
cx = np.cos(np.linspace(0, 2*np.pi, 128))
cy = np.sin(np.linspace(0, 2*np.pi, 128))
plt.grid(True)
plt.plot(cx, cy, 'k')
plt.plot(0.75*cx, 0.75*cy, 'k--', linewidth=.2)
plt.plot(0.5*cx, 0.5*cy, 'k--', linewidth=.2)
plt.plot(0.25*cx, 0.25*cy, 'k--', linewidth=.2)

plt.plot(1.15*cx[8:25], 1.15*cy[8:25], 'k')
plt.arrow(1.15*cx[23], 1.15*cy[23], cx[24]-cx[23], cy[24]-cy[23],
          head_width=.05, color='k')

re = M.modes.evals.real[:, 0]
im = M.modes.evals.imag[:, 0]

# Plot all modes (can't do multiple marker sizes at once)
for idx in range(len(M.modes.evals)):
    if idx in low_mode:
        color = 'b'
    elif idx in high_mode:
        color = 'r'
    else:
        color = 'g'

    plt.plot(re[idx], im[idx], '+',
             ms=M.modes.dampening_time[idx, 0],
             markeredgewidth=2, color=color)

plt.xlabel('Real')
plt.ylabel('Imaginary')
ax = plt.gca()
plt.ylim(-1.1, 1.1)
plt.xlim(-1.1, 1.1)
plt.gca().set_aspect('equal', 'datalim')
plt.annotate('Frequency', xy=(.56, 1.02))
plt.grid(True)
plt.title('Pole plot for example simulation')

plt.subplots_adjust(left=0.075, right=0.95, top=0.95, bottom=0.05, hspace=0.6)

plt.savefig(join(DIR_FIGURES, '91_sim_spectral.png'), dpi=600, bbox_inches='tight')

# Original spectra spectrum
fig = plt.figure(figsize=(3, 9))
plt.subplot(311)
plt.plot(freq_vect, np.abs(fourier_spectra.T), 'k', alpha=0.2)
plt.plot(freq_vect, np.abs(fourier_spectra.T).mean(axis=1), 'k', linewidth=2)
plt.title('Full Spectrum')
decorate(plt.gca())

plt.subplot(312)
plt.plot(freq_vect, np.abs(fourier_spectra.T), 'k', linewidth=0.2)
plt.plot(freq_vect[f1_inds], np.abs(fourier_spectra.T)[f1_inds, :], 'r', linewidth=0.4)
plt.plot(freq_vect[f2_inds], np.abs(fourier_spectra.T)[f2_inds, :], 'b', linewidth=0.4)
decorate(plt.gca())
plt.title('Fourier Filtered Spectrum')

# Modal spectra
plt.subplot(313)
plt.grid(False)
plt.plot(freq_vect, np.abs(modal_low_spectra.T), 'b', linewidth=0.4)
plt.plot(freq_vect, np.abs(modal_high_spectra.T), 'r', linewidth=0.4)
plt.title('Modal Spectra')
decorate(plt.gca())
plt.subplots_adjust(left=.2, right=.95, top=.95, hspace=.4, wspace=.3)
plt.savefig(join(DIR_FIGURES, '91_sim_group_spectra.png'), dpi=600, transparent=True)

# Figure 1B
plt.figure(figsize=(7.5, 3.75))
plt.subplot(2, 3, 1)
plt.plot(freq_vect, np.einsum('iik->ki...', F.PSD[:, :, :, 0]), 'k', linewidth=1, alpha=.5)
plt.plot(freq_vect, F.PSD[6, 6, :, 0], 'k', linewidth=2)
plt.ylabel('PSD')
decorate(plt.gca(), xlab=False)
plt.text(-7.25, 0.13, 'C', fontsize=14)
plt.ylim(0, .14)

plt.subplot(2, 3, 4)
plt.plot(freq_vect, M.PSD[6, 6, :, :], linewidth=2)
plt.plot(freq_vect, M.PSD[6, 6, :, high_mode].sum(axis=0), linewidth=2, color='r')
plt.plot(freq_vect, M.PSD[6, 6, :, low_mode].T, linewidth=2, color='b')
plt.xlabel('Frequency (Hz')
plt.ylabel('PSD')
decorate(plt.gca())
plt.text(-7.25, 0.14, 'D', fontsize=14)
plt.ylim(0, .14)

ax = plt.subplot(2, 3, 2)
sails.plotting.root_plot(M.modes.evals,
                         ax=ax,
                         plotargs={'markersize': 6})

sails.plotting.root_plot(M.modes.evals[high_mode, 0],
                         ax=ax,
                         plotargs={'color': 'r', 'markersize': 13})

sails.plotting.root_plot(M.modes.evals[low_mode, 0],
                         ax=ax,
                         plotargs={'color': 'b', 'markersize': 13})

plt.xticks([-1, 0, 1], [-1, 0, 1])
plt.yticks([-1, 0, 1], [-1, 0, 1])
plt.axis('scaled')
plt.text(-1.9, 1.0, 'E', fontsize=14)

plt.subplot(2, 3, 5)

plt.plot(M.modes.peak_frequency, M.modes.dampening_time, 'k+')

plt.plot(M.modes.peak_frequency[high_mode, 0],
         M.modes.dampening_time[high_mode, 0],
         'r+', markersize=15)

plt.plot(M.modes.peak_frequency[low_mode, 0],
         M.modes.dampening_time[low_mode, 0],
         'b+', markersize=15)

# Add the thresholding line
plt.hlines(all_dt_thresh[-1], plt.xlim()[0], plt.xlim()[1], linestyle='--')

plt.grid(True)
for tag in ['right', 'top']:
    plt.gca().spines[tag].set_visible(False)
plt.xlabel('Frequency (Hz)')
plt.ylabel('Damping Time')
plt.text(-10, 8.0, 'F', fontsize=14)

plt.subplot(2, 3, 3)
plt.pcolormesh(m_low.PSD[:, :, 0, 0], cmap='Blues')
plt.xticks(np.arange(nareas, 0, -1)-.5, ['']*nareas)
plt.yticks(np.arange(nareas, 0, -1)-.5, np.arange(nareas, 0, -1))
plt.axis('scaled')
plt.text(-3.5, 9.25, 'G', fontsize=14)


plt.subplot(2, 3, 6)
plt.pcolormesh(m_high.PSD[:, :, 0, 0], cmap='Reds')
plt.xticks(np.arange(nareas, 0, -1)-.5, np.arange(nareas, 0, -1))
plt.yticks(np.arange(nareas, 0, -1)-.5, np.arange(nareas, 0, -1))
plt.axis('scaled')
plt.text(-3.5, 10.0, 'H', fontsize=14)

plt.subplots_adjust(left=.075, right=.98, top=.95, hspace=.4, wspace=.3)
plt.savefig(join(DIR_FIGURES, '91_single_session.png'), dpi=600, transparent=False)

# Create a supplementary plot with all participants poles in
fig = plt.figure(figsize=(7.5, 7.5))
plt.subplot(5, 4, 1)

for k in range(nppts):
    plt.subplot(5, 4, k+1)
    M = model_data[k][2]

    plt.plot(M.modes.peak_frequency, M.modes.dampening_time, 'k+')

    plt.plot(M.modes.peak_frequency[modal_high_pos[k], 0],
             M.modes.dampening_time[modal_high_pos[k], 0],
             'r+', markersize=15)

    plt.plot(M.modes.peak_frequency[modal_low_pos[k], 0],
             M.modes.dampening_time[modal_low_pos[k], 0],
             'b+', markersize=15)

    # Add the thresholding line
    plt.hlines(all_dt_thresh[k], plt.xlim()[0], plt.xlim()[1], linestyle='--')

    # Make our x and y limits consistent
    plt.xlim(-5, 45)
    plt.ylim(0, 10)

    # If we're not at the bottom, don't show the x axis tick labels
    if k < 16:
        plt.gca().set_xticklabels([])

    # If we're not at the left, don't show the y axis ticks
    if k % 4 != 0:
        plt.gca().set_yticklabels([])

# Can't figure out how to get matplotlib to put a shared x/y label on subplots
fig.text(0.5, 0.04, 'Frequency (Hz)', ha='center', fontsize=12)
fig.text(0.04, 0.5, 'Damping Time', va='center', rotation='vertical', fontsize=12)

plt.savefig(join(DIR_FIGURES, '91_supp_IndividualSimPoles.png'), dpi=600, transparent=False)
