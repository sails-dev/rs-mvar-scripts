#!/usr/bin/python3 -u

from sys import argv, exit

from os import makedirs
from os.path import join, isfile
from random import sample
from time import time

import h5py
import numpy as np

import sails

from mvarconfig import (DIR_PREPROCVEDATA, DIR_NULLDIST,
                        DEFAULT_MODEL_ORDER)

# Number of seconds in each randomisation chunk
chunk_seconds = 45

# Number of permutations
num_permutations = 1000


def randomise_epoch_data(X_epoch):
    """
    Take a copy of the epoch data, randomise on channels and epochs
    and return the randomised copy
    """
    X_copy = np.zeros_like(X_epoch)
    num_chans = X_copy.shape[0]
    num_segments = X_copy.shape[2]
    num_to_permute = num_chans * num_segments

    samples = sample(list(np.arange(num_to_permute)), num_to_permute)
    sample_idx = 0

    sample_numbers = np.zeros((num_chans, num_segments, 2), np.int)

    for chan in range(X_epoch.shape[0]):
        for epoch in range(num_segments):
            orig_chan = int(samples[sample_idx] / num_segments)
            orig_idx = samples[sample_idx] % num_segments
            X_copy[chan, :, epoch] = X_epoch[orig_chan:orig_chan+1, :, orig_idx]
            sample_numbers[chan, epoch, :] = [orig_chan, orig_idx]
            sample_idx += 1

    return X_copy, sample_numbers


def analyse_run(subj, run, model_order):
    # Work out which files we are working on
    name = '{}_{}.hdf5'.format(subj, run)

    in_path = join(DIR_PREPROCVEDATA, name)
    out_dir = join(DIR_NULLDIST, str(model_order))
    out_path = join(out_dir, name)

    makedirs(out_dir, exist_ok=True)

    if not isfile(in_path):
        print("E: {} does not exist: cannot process".format(in_path))
        exit(1)

    if isfile(out_path):
        print("W: {} already exists: skipping and not processing".format(out_path))
        exit(0)

    print("I: Loading {0}".format(in_path))

    dF = h5py.File(in_path, 'r')

    X = dF['data'][...]
    sample_rate = dF['sample_rate'][...]

    # Add a fake epoch dimension
    X = X[:, :, None]

    # Divide model up into chunk_seconds second chunks
    segment_len = int(sample_rate * chunk_seconds)
    num_segments = int(X.shape[1] // segment_len)

    X = X[:, 0:segment_len*num_segments, :]

    X_epoch = X.reshape(X.shape[0], num_segments, segment_len)

    # Data needs to be (channels, time, epochs)
    X_epoch = np.ascontiguousarray(X_epoch.swapaxes(1, 2))

    f = h5py.File(out_path, 'w')

    permgroup = f.create_group('perms')

    # Store max DT distribution for later use
    max_dts = []

    for modelnum in range(num_permutations):
        print("I: Permutation {} : {}".format(modelnum, time()))
        if modelnum == 0:
            # Our first model is our observed statistic
            X_copy = X_epoch.copy()
            sample_numbers = np.zeros((X_epoch.shape[0], X_epoch.shape[2], 2), np.int)
            sample_numbers[:, :, 0] = np.arange(X_epoch.shape[0])[:, None]
            sample_numbers[:, :, 1] = np.arange(X_epoch.shape[2])[None, :]
        else:
            # Permute the data
            X_copy, sample_numbers = randomise_epoch_data(X_epoch)

        # Set up our frequency vector
        freq_vect = np.linspace(0, sample_rate/2, 100)

        # Model fit
        print("I: Fitting model : {}".format(time()))
        model = sails.VieiraMorfLinearModel.fit_model(X_copy, np.arange(model_order))

        print("I: Extracting modes : {}".format(time()))
        modes = sails.MvarModalDecomposition.initialise(model, sample_rate)

        if modelnum == 0:
            # Metrics
            print("I: Analysing fourier metrics : {}".format(time()))
            F = sails.FourierMvarMetrics.initialise(model, sample_rate, freq_vect)

            print("I: Analysing modal metrics : {}".format(time()))
            M = sails.ModalMvarMetrics.initialise_from_modes(model, modes, sample_rate, freq_vect)

            # Save out our fourier and modal metrics as well as our model
            F.to_hdf5(f.create_group('F'))
            M.to_hdf5(f.create_group('M'))
            model.to_hdf5(f.create_group('model'))

            del F, M

        max_dts.append(np.max(modes.dampening_time))

        # Save our permutation information
        g = permgroup.create_group(str(modelnum))

        g.create_dataset('permutation', data=sample_numbers)
        g.create_dataset('DTs', data=modes.dampening_time)
        g.create_dataset('PFs', data=modes.peak_frequency)

        del modes, model

    # Save the max distribution of dampening times for later use
    f.create_dataset('max_perm_dts', data=np.array(max_dts))

    f.close()


if __name__ == '__main__':
    if len(argv) < 3:
        print("mvar_21_permute_modes.py PARTICIPANT RUN [MODEL_ORDER]")
        exit(1)

    subj = argv[1]
    run = argv[2]
    if len(argv) >= 4:
        model_order = int(argv[3])
    else:
        # Default model order
        model_order = DEFAULT_MODEL_ORDER

    print("I: Fitting models at order {}".format(model_order))

    analyse_run(subj, run, model_order)
