% Matlab script to plot surfaces

% Start by reading our configuration to find paths etc
conf = mvarconfig('mvarconfig.ini')

% Change the model order here if you don't want to use the default
model_order = conf.DEFAULT_MODEL_ORDER;

% If set, figures will be closed after saving
closefigs = true;

% Now we need to add some paths for the modules we require
addpath(fullfile(conf.DIR_EXTERNAL, 'export_fig'))
addpath(fullfile(conf.DIR_EXTERNAL, 'osl', 'osl-core'))
osl_startup

addpath(fullfile(conf.DIR_EXTERNAL, 'mfiles'));

% This line may be necessary depending on your system On Linux boxes, MATLAB
% includes a screwed version of the Qt libraries which it then shoves into
% LD_LIBRARY_PATH whilst wb_command needs the system version it was built
% against.  We can work around that by forcing the normal library directory
% to the front of LD_LIBRARY_PATH, over-riding their override.
% We've guarded this behind being on Linux-x86_64 only, but you may
% need to comment it depending on your platform.
% I hate MATLAB and Mathworks and wish it would all go away (this comment
% endorsed by MH only).
if strcmp(computer('arch'), 'glnxa64')
    % Avoid pre-pending multiple times if we use the script more than once
    if ~startsWith(getenv('LD_LIBRARY_PATH'), '/usr/lib/x86_64-linux-gnu:')
        setenv('LD_LIBRARY_PATH', strcat('/usr/lib/x86_64-linux-gnu:', getenv('LD_LIBRARY_PATH')));
    end
end

% Load the parcellation

parc = parcellation(fullfile(conf.DIR_BASE, 'aal_cortical_merged_8mm_stacked.nii.gz'));
node_inds = load(fullfile(conf.DIR_BASE, 'aal_cortical_merged_sorted.inds'));

[~,I] = sort(node_inds);

modalbase = fullfile(conf.DIR_MODALSTATS, num2str(model_order));
base = fullfile(conf.DIR_FIGURES, num2str(model_order));

bands = {'alpha', 'beta', 'theta', 'low', 'high'};

for bnum = 1:numel(bands)
    band = bands{bnum};

    % Load data from HDF5 file
    hdfname = fullfile(modalbase, sprintf('33_hcp_pca_%s_params.hdf5', band));
    projections = h5read(hdfname, '/projections');
    components = h5read(hdfname, '/pca/components');
    data_mean = h5read(hdfname, '/pca/data_mean');
    pcs = h5read(hdfname, '/pcs');

    % Do the MATLAB is backwards (in more ways than one) dance
    projections = permute(projections, [3, 2, 1]);
    components = permute(components, [2, 1]);
    data_mean = permute(data_mean, [2, 1]);
    pcs = permute(pcs, [3, 2, 1]);

    num_pcs = size(projections, 1);
    num_rois = size(pcs, 1);

    % data mean
    x = diag(reshape(data_mean, num_rois, num_rois));
    cl = 1e-6; % max(x);

    parc.plot_surf_montage(x(I),'montage','radial',...
            'interptype','enclosing',...
            'clims',[0 cl],'cmap','reds');

    export_fig(fullfile(base, sprintf('50_hcp_modalpca_%s_mean_radial.png', band)), '-transparent');
    if closefigs
        close('all');
    end

    % Components
    components = reshape(components, num_pcs, num_rois, num_rois);
    for ii = 1:num_pcs
        p = diag(squeeze(components(ii,:,:)));

        % As the sign is arbitrary, if the maximum is < 0, flip the sign so that it's positive
        % for visualisation purposes - this matches the scatter plots etc in the Python code
        if max(p) < 0
            p = p * -1;
        end

        cl = max([max(p) abs(min(p))]);
        % We need to use the _r map here to match the python ones with green as negative
        cm = get_redblue_cmap(-cl, cl, 128, [], 'GreenFuchsia_r');

        % Plot in tall form
        parc.plot_surf_montage(p(I), 'montage', 'tall', ...
                               'interptype','enclosing', ...
                               'clims', [-cl cl], 'cmap', cm, ...
                               'position', [100 100 512*2 768*2]);

        figpath = fullfile(base, sprintf('50_hcp_modalpca_%s_component_tall_%d.png', band, ii));
        export_fig(figpath, '-transparent');

        if closefigs
            close('all')
        end

        % Plot in radial form
        parc.plot_surf_montage(p(I), 'montage', 'radial', ...
                               'interptype', 'enclosing', ...
                               'clims', [-cl cl], 'cmap', cm, ...
                               'position', [100 100 768*2 512*2]);

        figpath = fullfile(base, sprintf('50_hcp_modalpca_%s_component_radial_%d.png', band, ii));
        export_fig(figpath, '-transparent');

        if closefigs
            close('all')
        end
    end

    % Data projections
    proj = reshape(projections, num_pcs*2, num_rois, num_rois);
    for ii = 1:(num_pcs*2)
        p = diag(squeeze(proj(ii,:,:)));
        cl = 1e-6;

        % Plot in radial mode
        parc.plot_surf_montage(p(I), 'montage', 'radial',...
                               'interptype','enclosing',...
                               'clims',[0 cl],'cmap','reds');

        figname = fullfile(base, sprintf('50_hcp_modalpca_%s_scoreproj_radial_%d.png', band, ii));
        export_fig(figname, '-transparent');

        if closefigs
            close('all')
        end
    end
end
