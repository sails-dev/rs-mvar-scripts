% Matlab script to plot surfaces

% Start by reading our configuration to find paths etc
conf = mvarconfig('mvarconfig.ini')

% If you want to override the default model order, do it here
model_order = conf.DEFAULT_MODEL_ORDER;

% If set, figures will be closed after saving
closefigs = true;

% Now we need to add some paths for the modules we require
addpath(fullfile(conf.DIR_EXTERNAL, 'export_fig'))
addpath(fullfile(conf.DIR_EXTERNAL, 'osl', 'osl-core'))
osl_startup

addpath(fullfile(conf.DIR_EXTERNAL, 'mfiles'));

% This line may be necessary depending on your system On Linux boxes, MATLAB
% includes a screwed version of the Qt libraries which it then shoves into
% LD_LIBRARY_PATH whilst wb_command needs the system version it was built
% against.  We can work around that by forcing the normal library directory
% to the front of LD_LIBRARY_PATH, over-riding their override.
% We've guarded this behind being on Linux-x86_64 only, but you may
% need to comment it depending on your platform.
% I hate MATLAB and Mathworks and wish it would all go away (this comment
% endorsed by MH only).
if strcmp(computer('arch'), 'glnxa64')
    % Avoid pre-pending multiple times if we use the script more than once
    if ~startsWith(getenv('LD_LIBRARY_PATH'), '/usr/lib/x86_64-linux-gnu:')
        setenv('LD_LIBRARY_PATH', strcat('/usr/lib/x86_64-linux-gnu:', getenv('LD_LIBRARY_PATH')));
    end
end

% Load the parcellation

parc = parcellation(fullfile(conf.DIR_BASE, 'aal_cortical_merged_8mm_stacked.nii.gz'));
node_inds = load(fullfile(conf.DIR_BASE, 'aal_cortical_merged_sorted.inds'));

[~,I] = sort(node_inds);

base = fullfile(conf.DIR_FIGURES, num2str(model_order));

% Load data from HDF5 file
hdfname = fullfile(conf.DIR_MODALSTATS, num2str(model_order), '32_fourier_summary.hdf5');

f_psd = h5read(hdfname, '/psd');

% Do the "MATLAB is backwards" dance
f_psd = permute(f_psd, [4, 3, 2, 1]);

freq_vect = h5read(hdfname, '/freq_vect');

% Surface plot for each of five bands
bands = [[0, 3]; [3, 7]; [7, 13]; [13, 30]; [30, 60]];
band_names = {'delta', 'theta', 'alpha', 'beta', 'gamma'};

for bnum = 1:numel(band_names)
    band_name = band_names{bnum};
    low_freq = bands(bnum, 1);
    high_freq = bands(bnum, 2);

    f_idx = (freq_vect >= low_freq) & (freq_vect < high_freq);

    p = f_psd(:, :, f_idx, :);

    % Average across runs and then frequencies
    p = mean(p, 4);
    p = mean(p, 3);

    % Take just the diagonal for this (off-diags are in circos plots)
    x = diag(p);

    parc.plot_surf_montage(x(I),...
            'montage','radial',...
            'interptype', 'enclosing',...
            'clims', [min(x) max(x)],...
            'cmap','parula',...
            'position', [100 100 1400 933]);

    export_fig(fullfile(base, sprintf('41_fourier_%s.png', band_name)), '-transparent');
    if closefigs
        close('all');
    end
end
