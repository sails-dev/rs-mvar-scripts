#!/usr/bin/python3

# Resample all VEs down to an Fs of 120Hz, run through orthogonalisation and
# sort out the order of the parcels

from sys import exit
from os import makedirs
from os.path import isdir, isfile, join

import h5py

import numpy as np

from sails import fast_resample, symmetric_orthonormal

from mvarconfig import (DIR_RAWVEDATA, DIR_PREPROCVEDATA,
                        FILE_AALINDS, PARTICIPANTS, RUNS)

# Downsample from 240Hz Fs to 120Hz
downsample_factor = 2.0

# Find the AAL indices to order the VEs
inds = np.loadtxt(FILE_AALINDS).astype(int)-1

if not isdir(DIR_RAWVEDATA):
    print("Error: {} does not exist".format(DIR_RAWVEDATA))
    exit(1)

makedirs(DIR_PREPROCVEDATA, exist_ok=True)

error = False

# Pre-process each participant and each run in turn
for participant in PARTICIPANTS:
    for run in RUNS:
        inname = 'roinet{}_MEG_{}-Restin_rmegpreproc.mathdf5'.format(participant, run)
        inpath = join(DIR_RAWVEDATA, inname)

        outname = '{}_{}.hdf5'.format(participant, run)
        outpath = join(DIR_PREPROCVEDATA, outname)

        if isfile(outpath):
            print("W: {} exists: skipping".format(outpath))
            continue

        if not isfile(inpath):
            print("E: {} does not exist: cannot downsample".format(inpath))
            error = True
            continue

        print("I: Re-sampling {}/{}".format(participant, run))

        # Find the original data
        in_f = h5py.File(inpath, 'r')
        out_f = h5py.File(outpath, 'w')

        X = in_f['data'][...]

        # Re-calculate the sample rate
        sample_rate = in_f['sample_rate'][0, 0] / downsample_factor

        in_f.close()

        # Re-sample and orthogonalise
        X = fast_resample(X, downsample_factor)
        X, _ = symmetric_orthonormal(X)

        # Re-order into the right order for AAL
        X = X[inds, :]

        # Save out to the new file
        out_f.create_dataset('data', data=X)
        out_f.create_dataset('sample_rate', data=np.array(sample_rate))

        out_f.close()

if error:
    print("E: At least one fatal error occured: check logs")
    exit(1)

exit(0)
