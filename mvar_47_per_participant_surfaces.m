% Matlab script to plot surfaces

% Start by reading our configuration to find paths etc
conf = mvarconfig('mvarconfig.ini')

% Change the model order here if you don't want to use the default
model_order = conf.DEFAULT_MODEL_ORDER;

% If set, figures will be closed after saving
closefigs = true;

% Now we need to add some paths for the modules we require
addpath(fullfile(conf.DIR_EXTERNAL, 'export_fig'))
addpath(fullfile(conf.DIR_EXTERNAL, 'osl', 'osl-core'))
osl_startup

addpath(fullfile(conf.DIR_EXTERNAL, 'mfiles'));

% This line may be necessary depending on your system On Linux boxes, MATLAB
% includes a screwed version of the Qt libraries which it then shoves into
% LD_LIBRARY_PATH whilst wb_command needs the system version it was built
% against.  We can work around that by forcing the normal library directory
% to the front of LD_LIBRARY_PATH, over-riding their override.
% We've guarded this behind being on Linux-x86_64 only, but you may
% need to comment it depending on your platform.
% I hate MATLAB and Mathworks and wish it would all go away (this comment
% endorsed by MH only).
if strcmp(computer('arch'), 'glnxa64')
    % Avoid pre-pending multiple times if we use the script more than once
    if ~startsWith(getenv('LD_LIBRARY_PATH'), '/usr/lib/x86_64-linux-gnu:')
        setenv('LD_LIBRARY_PATH', strcat('/usr/lib/x86_64-linux-gnu:', getenv('LD_LIBRARY_PATH')));
    end
end

% Load the parcellation

parc = parcellation(fullfile(conf.DIR_BASE, 'aal_cortical_merged_8mm_stacked.nii.gz'));
node_inds = load(fullfile(conf.DIR_BASE, 'aal_cortical_merged_sorted.inds'));

[~,I] = sort(node_inds);

modalbase = fullfile(conf.DIR_MODALSTATS, num2str(model_order));
base = fullfile(conf.DIR_FIGURES, num2str(model_order));

participants = {'100307', '179245', '406836', '917255'};
runs = {'3', '3', '3', '3'};

for pnum = 1:numel(participants)
    part = participants{pnum};
    run = runs{pnum};

    % Load data from HDF5 file
    hdfname = fullfile(modalbase, sprintf('46_part_%s_%s.hdf5', part, run));

    f_psd = h5read(hdfname, '/F_PSD');
    ms_psd = h5read(hdfname, '/Ms_PSD');

    ms_S = h5read(hdfname, '/Ms_S');

    f_M = h5read(hdfname, '/F_M');
    S_M = h5read(hdfname, '/S_M');
    N_M = h5read(hdfname, '/N_M');

    x = diag(squeeze(ms_psd));

    parc.plot_surf_montage(x(I),'montage','radial',...
            'interptype','enclosing',...
            'clims',[0 max(x)],'cmap','reds', ...
            'position', [100 100 1200 800]);

    export_fig(fullfile(base, sprintf('47_%s_%s_ms_psd_radial.png', part, run)), '-transparent');
    if closefigs
        close('all');
    end

    %    % Plot in radial form
    %    parc.plot_surf_montage(p(I), 'montage', 'radial', ...
    %                           'interptype', 'enclosing', ...
    %                           'clims', [-cl cl], 'cmap', cm, ...
    %                           'position', [100 100 768*2 512*2]);

end
