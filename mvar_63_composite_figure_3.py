#!/usr/bin/python3

from os.path import join

import shutil

from mvarconfig import (DIR_FIGURES)

# This one is easy
shutil.copyfile(join(DIR_FIGURES, '91_sim_netmats.png'),
                join(DIR_FIGURES, '63_figure3.png'))
