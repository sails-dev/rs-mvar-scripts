#!/usr/bin/python3

from sys import argv, exit
from os import makedirs
from os.path import join, isfile

import h5py

import numpy as np

from scipy import stats

import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap, rgb2hex

from anamnesis import obj_from_hdf5group

from sails import PCA, CircosHandler  # noqa

from mvarconfig import (DIR_MODALSTATS, DIR_FIGURES,
                        FILE_CIRCOS_GROUPS, FILE_CIRCOS_REGIONS,
                        DEFAULT_MODEL_ORDER)

# We will extract the data for each of these frequency bands
FREQ_BANDS = {'theta': (1, 7),
              'alpha': (7, 13),
              'beta': (13, 30),
              'low': (0, 5),
              'high': (15, 100)}
FREQ_BANDS = {'alpha': (7,13)}

if len(argv) not in (1, 2):
    print("mvar_51_modelpca_plot.py [MODEL_ORDER]")
    exit(1)

if len(argv) == 2:
    model_order = int(argv[1])
else:
    # Default model order
    model_order = DEFAULT_MODEL_ORDER

in_dir = join(DIR_MODALSTATS, str(model_order))
out_dir = join(DIR_FIGURES, str(model_order))

makedirs(out_dir, exist_ok=True)

# Set up matplotlib
plt.style.use('ggplot')
plt.rcParams.update({'font.size': 7})

# Set up the CircosHandler
c = CircosHandler.from_csv_files(FILE_CIRCOS_GROUPS, FILE_CIRCOS_REGIONS,
                                 analysis_column='AnalysisID_NewSort')

# Generate come colourscales
col2 = [0.085, 0.6, 0.201]  # Green
col1 = [.6, .1, .6]  # Fuchsia

col_centre = [0.865, 0.865, 0.865]  # Grey

R = np.interp(np.linspace(-1, 1), [-1, 0, 1], [col1[0], col_centre[0], col2[0]])
G = np.interp(np.linspace(-1, 1), [-1, 0, 1], [col1[1], col_centre[1], col2[1]])
B = np.interp(np.linspace(-1, 1), [-1, 0, 1], [col1[2], col_centre[2], col2[2]])
A = np.ones_like(R)

GrFu = ListedColormap(np.c_[R, G, B, A], name='GrFu')


for bandname in sorted(FREQ_BANDS.keys()):
    filename = join(in_dir, '33_hcp_pca_{}_params.hdf5'.format(bandname))

    f = h5py.File(filename, 'r')

    pca = obj_from_hdf5group(f['pca'])
    pcs = f['pcs'][...]
    part_nums = f['part_nums'][...]
    run_nums = f['run_nums'][...]
    dts = f['dts'][...]
    freqs = f['freqs'][...]

    split_C = f['split_C'][...]
    split_C_original = f['split_C_original'][...]

    num_rois = pcs.shape[0]
    num_pcs = pcs.shape[2]
    num_parts = int(part_nums.max()) + 1

    f.close()

    # Plotting

    # Split plots
    outname = join(out_dir, '51_pca_{}_split.png'.format(bandname))

    # We plot the abs of the correlations as the sign of the component is
    # ill-determined - we're only interested in the overall structure
    f1 = plt.figure(figsize=(7.5, 3.5))

    plt.violinplot(np.abs(split_C), widths=0.75, showmedians=True)
    plt.xlabel('Component')
    plt.ylabel('Split-half absolute correlation')

    f1.savefig(outname, dpi=600)

    plt.close(f1)

    # Generate some metrics
    # Find the weighted average frequency for each participant
    subj_freq = np.zeros((num_parts,))

    for ii in range(num_parts):
        inds = part_nums == ii

        if inds.sum() > 0:
            subj_freq[ii] = np.average(freqs[inds], weights=dts[inds])

    I = np.argsort(subj_freq)

    # Get average freqs per subj
    subjs = np.zeros((num_parts, num_pcs, num_pcs)) * np.nan

    for pc in range(num_pcs):
        for ii in range(num_parts):
            inds = part_nums == ii

            f = freqs[inds]
            s = pca.scores[inds, pc]
            d = dts[inds]

            if np.sum(s > 0) > 0:
                subjs[ii, 0, pc] = np.average(f[s > 0], weights=d[s > 0])
                subjs[ii, 2, pc] = np.average(s[s > 0], weights=d[s > 0])

            if np.sum(s < 0) > 0:
                subjs[ii, 1, pc] = np.average(f[s < 0], weights=d[s < 0])
                subjs[ii, 3, pc] = np.average(s[s < 0], weights=d[s < 0])

    pc_max = stats.scoreatpercentile(np.abs(pcs.flatten()), 99.9)

    freqrange = FREQ_BANDS[bandname]

    # Do netmat, circos, scatter and histogram plots
    # TODO: Rename k to pc to make it clearer
    for pc in range(num_pcs):
        # Load the bayesian regression predictions
        bayesname = join(in_dir, '42_pca_bayes_stats_{}_pc{}.csv'.format(bandname, (pc+1)))

        bayeslin = None

        # Only if we have the predictions:
        if isfile(bayesname):
            bayeslin = np.loadtxt(bayesname, skiprows=1, usecols=(0, 9, 10, 11), delimiter=",")

        outname = join(out_dir, '51_pca_{}_netmat_{}.png'.format(bandname, (pc + 1)))

        f2 = plt.figure(figsize=(1.875, 1.875))

        tmp = pcs[:, :, pc].copy()

        scores = pca.scores.copy()

        # If the whole component is negative, flip the sign so that it's positive
        # The sign is arbitrary
        if tmp.max() < 0:
            tmp = tmp * -1
            scores = scores * -1

        c.plot_netmat(tmp - np.diag(np.diag(tmp)), ax=plt.gca(),
                      labels=False, hregions=True,
                      showgrid=False, show_xticks=True,
                      show_yticks=True, cmap=GrFu,
                      vmin=-pc_max, vmax=pc_max)

        f2.savefig(outname, dpi=600)

        plt.close(f2)

        # Circular plot - take threshold as 95th percentile
        indices = np.triu_indices(num_rois, 1)
        thresh = stats.scoreatpercentile(np.abs(tmp[indices].flatten()), 95)

        # Take away diagonal and lower triangle indices
        tmp[np.tril_indices(num_rois)] = np.nan

        # Get rid of beneath threshold connections
        tmp[np.abs(tmp) < thresh] = np.nan

        # Scale the remaining connections to between 1-5 (line width)
        mn = np.nanmin(np.abs(tmp))
        mx = np.nanmax(np.abs(tmp))

        # Now scale width to 4 wide then offset so that the minimum value
        # would be at 1; this leaves us at 1-5
        width = 4 / (mx - mn)
        offset = (1 - (mn * width))

        tmp = tmp * width

        tmp = tmp + (np.sign(tmp) * offset)

        outname = join(out_dir, '51_pca_{}_circos_{}'.format(bandname, (pc + 1)))

        try:
            c.gen_circos_plot(tmp, outname,
                              radius=562, circle_prop=0.9,
                              ticks=False, tick_labels=False,
                              linkcolors=('black', rgb2hex(col1), rgb2hex(col2)))
        except Exception as e:
            print("E: Circos generation failed ", e)
            #exit(1)

        # Scatter plots
        f3 = plt.figure(figsize=(2, 3))

        x = freqs
        y = scores[:, pc]

        # Calculate the point density
        xy = np.vstack([x, y])
        z = stats.gaussian_kde(xy)(xy)

        # KDE colouring
        plt.scatter(x, y*1e6, c=z, s=5, edgecolor='')

        # Sort the points by density, so that the densest points are plotted last
        idx = z.argsort()
        x, y, z = x[idx], y[idx], z[idx]

        # Do one plot with and without tendency
        plt.xlim(freqrange[0] - 1, freqrange[1] + 1)

        mx = np.abs(scores * 1e6).max()

        plt.ylim(-mx, mx)

        plt.xlabel('Frequency (Hz)')
        plt.ylabel('PC Score', labelpad=-5)

        # Now draw on some arrows
        plt.arrow(freqrange[1] + 0.5, -0.2, 0, -10, color=col1, width=0.3)
        plt.arrow(freqrange[1] + 0.5, 0.2, 0, 10, color=col2, width=0.3)

        outname = join(out_dir, '51_pca_{}_scatter_{}'.format(bandname, (pc + 1)))

        plt.subplots_adjust(bottom=0.15, left=0.25)

        f3.savefig(outname, dpi=600)

        # Plot bayes estimation - central tendency and 95% PI
        if bayeslin is not None:
            plt.plot(bayeslin[:, 0], bayeslin[:, 1]*1e6, 'k', linewidth=1.5)

            plt.fill_between(bayeslin[:, 0], bayeslin[:, 2]*1e6, bayeslin[:, 3]*1e6,
                             color='k', alpha=0.2)

            outname = join(out_dir, '51_pca_{}_scatter_{}_bayes'.format(bandname, (pc + 1)))

            f3.savefig(outname, dpi=600)

        plt.close(f3)

        # And the histogram plot
        f4 = plt.figure(figsize=(2, 1.55))

        xl = 30

        y = scores[:, pc]

        plt.hist(freqs[y > 0],
                 np.linspace(0, xl, 64*2),
                 histtype='stepfilled',
                 color=col2,
                 linewidth=2,
                 weights=dts[y > 0],
                 alpha=.5)

        plt.hist(freqs[y < 0],
                 np.linspace(0, xl, 64*2),
                 histtype='stepfilled',
                 color=col1,
                 linewidth=2,
                 weights=dts[y < 0],
                 alpha=.5)

        plt.xlim(freqrange[0] - 1, freqrange[1] + 1)

        # Don't show xtick labels here as when we composite they'll be duplicated
        xt, xtp = plt.xticks()
        plt.xticks(xt, [''] * len(xt))
        plt.subplots_adjust(left=0.25)

        outname = join(out_dir, '51_pca_{}_hist_{}'.format(bandname, (pc + 1)))

        f4.savefig(outname, dpi=600)
        plt.close(f4)

        # Plots which will be part of figure 8

        # Absolute frequency histograms
        f5 = plt.figure(figsize=(1.875, 1.875))

        s = scores[:, pc]

        nbins = 16

        plt.hist(subjs[:, 0, pc],
                 np.linspace(freqrange[0], freqrange[1], nbins),
                 histtype='stepfilled',
                 color=col2,
                 linewidth=2,
                 fc=(*col1, 0.5))

        plt.hist(subjs[:, 1, pc],
                 np.linspace(freqrange[0], freqrange[1], nbins),
                 histtype='stepfilled',
                 color=col2,
                 linewidth=2,
                 fc=(*col2, 0.5))

        plt.xlim(freqrange[0], freqrange[1])
        plt.ylim(0, 20)

        if pc == 0:
            plt.ylabel('Num ppts')
        else:
            # Hide tick labels without getting rid of them to keep spacing equal
            for i in plt.gca().get_yticklabels():
                i.set_color("white")

        plt.subplots_adjust(bottom=0.10, left=0.25)

        # TODO: Handle xticks if needed
        outname = join(out_dir, '51_pca_part_{}_abs_hist_{}'.format(bandname, (pc + 1)))

        f5.savefig(outname, dpi=600)

        plt.close(f5)

        # Absolute scatter plot
        f6 = plt.figure(figsize=(1.875, 5))

        plt.scatter(subjs[I, 0, pc],
                    np.arange(subjs.shape[0]),
                    s=np.abs(subjs[I, 2, pc])*5e6,
                    c=np.array(col1)[None, :],
                    label='+ve load')

        plt.scatter(subjs[I, 1, pc],
                    np.arange(subjs.shape[0]),
                    s=np.abs(subjs[I, 3, pc])*5e6,
                    c=np.array(col2)[None, :],
                    label='-ve load')

        plt.xlim(freqrange[0], freqrange[1])

        # TODO: Handle xticks if needed
        # plt.xticks([8,10,12])

        plt.xlabel('Frequency (Hz)')

        if pc == 0:
            plt.ylabel('Participant (sorted by AF)')
        else:
            # Hide tick labels without getting rid of them to keep spacing equal
            for i in plt.gca().get_yticklabels():
                i.set_color("white")

        plt.subplots_adjust(top=0.95, bottom=0.10, left=0.25)

        outname = join(out_dir, '51_pca_part_{}_abs_scatter_{}'.format(bandname, (pc + 1)))

        f6.savefig(outname, dpi=600)

        plt.close(f6)

        # And the relative frequency histograms

        # Only plot subjects who have both positive and negative means
        # TODO: Should we do the same above?
        to_plot = np.isnan(subjs[:, :2, pc]).sum(axis=1) == 0
        subjs2 = subjs.copy()
        subjs2[to_plot == False, :, :] = np.nan  # noqa: E712 : is False doesn't work with numpy
        ref = np.nanmean(subjs2[:, :2, pc], axis=1)

        f7 = plt.figure(figsize=(1.875, 1.875))

        nbins = 24

        plt.hist(subjs2[:, 0, pc] - ref,
                 np.linspace(-3, 3, nbins),
                 histtype='stepfilled',
                 color=col2,
                 linewidth=2,
                 fc=(*col1, 0.5))

        plt.hist(subjs2[:, 1, pc] - ref,
                 np.linspace(-3, 3, nbins),
                 histtype='stepfilled',
                 color=col2,
                 linewidth=2,
                 fc=(*col2, 0.5))

        plt.xlim(-3, 3)
        plt.ylim(0, 20)

        # Hide tick labels without getting rid of them to keep spacing equal
        for i in plt.gca().get_yticklabels():
            i.set_color("white")

        plt.subplots_adjust(bottom=0.10, left=0.25)

        # TODO: Handle xticks if needed
        outname = join(out_dir, '51_pca_part_{}_rel_hist_{}'.format(bandname, (pc + 1)))

        f7.savefig(outname, dpi=600)

        # Relative scatter plot
        f8 = plt.figure(figsize=(1.875, 5))

        plt.scatter(subjs2[I, 0, pc] - ref[I],
                    np.arange(subjs.shape[0]),
                    s=np.abs(subjs[I, 2, pc])*5e6,
                    c=np.array(col1)[None, :],
                    label='+ve load')

        plt.scatter(subjs2[I, 1, pc] - ref[I],
                    np.arange(subjs.shape[0]),
                    s=np.abs(subjs[I, 3, pc])*5e6,
                    c=np.array(col2)[None, :],
                    label='-ve load')

        plt.xlim(-3, 3)

        # TODO: Handle xticks if needed
        # plt.xticks([8,10,12])

        plt.xlabel('Frequency (Hz)')

        # Hide tick labels without getting rid of them to keep spacing equal
        for i in plt.gca().get_yticklabels():
            i.set_color("white")

        plt.subplots_adjust(top=0.95, bottom=0.10, left=0.25)

        outname = join(out_dir, '51_pca_part_{}_rel_scatter_{}'.format(bandname, (pc + 1)))

        f8.savefig(outname, dpi=600)

        plt.close(f8)


plt.figure(figsize=(8,8))
plt.axes([.2,.2,.6,.6])
plt.scatter(x*-1e6, y*1e6, c=freqs, s=10, edgecolor='',cmap='coolwarm')
plt.xlabel('PC1 Score',fontsize=14)
plt.ylabel('PC2 Score',fontsize=14)
cb = plt.colorbar()
cb.set_label('Frequency (Hz)',fontsize=14)
outname = join(out_dir, 'SI_pcascore.png')
plt.savefig(outname,dpi=300)
