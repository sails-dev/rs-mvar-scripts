% Run all participants serially

% Start by reading our configuration to find paths etc
conf = mvarconfig('mvarconfig.ini')

% Iterate over all participants and call the beamforming script
for i = 1:numel(conf.PARTICIPANTS)
    mvar_01_beamform_participant(conf.PARTICIPANTS{i});
end
