% Matlab script to beamform one participant

function beamform_participant(subID)

% Start by reading our configuration to find paths etc
conf = mvarconfig('mvarconfig.ini')

% Now we need to add some paths for the modules we require
addpath(fullfile(conf.DIR_EXTERNAL, 'export_fig'))
addpath(fullfile(conf.DIR_EXTERNAL, 'osl', 'osl-core'))
osl_startup

addpath(fullfile(conf.DIR_EXTERNAL, 'mfiles'));

% Initialise spm and fieldtrip
%ynicInit spm12
%addpath('/groups/Projects/P1108/andrew/oscillatory_modes/osl-bf/toolboxes/spm12/external/fieldtrip');
%ft_defaults;

% Add FSL scripts
% Horrendous
%setenv('FSLDIR', '/usr/share/fsl-5.0');
%setenv('FSLOUTPUTTYPE', 'NIFTI_GZ');
%setenv('FSLMULTIFILEQUIT', 'TRUE');
%setenv('LD_LIBRARY_PATH', strcat(getenv('LD_LIBRARY_PATH'), ':/usr/lib/fsl-5.0'));
%addpath(genpath('/groups/Projects/P1108/andrew/oscillatory_modes/osl-bf/toolboxes/fsl'));

% Add OSL toolboxes
%addpath(genpath('/groups/Projects/P1108/andrew/oscillatory_modes/osl-bf/toolboxes/osl'));

% Run beamforming
%cd /groups/Projects/P1108/andrew/oscillatory_modes/osl-bf/

% Call the main beamforming script
mvar_02_hcp_aal_beamform(subID);
