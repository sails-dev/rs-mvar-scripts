#!/usr/bin/python3

from sys import argv, exit
from os.path import join

from PIL import Image, ImageFont, ImageDraw

from mvarconfig import (DIR_FIGURES, FONT,
                        DEFAULT_MODEL_ORDER)

if len(argv) not in (1, 2):
    print("mvar_66_composite_figure_6.py [MODEL_ORDER]")
    exit(1)

if len(argv) == 2:
    model_order = int(argv[1])
else:
    # Default model order
    model_order = DEFAULT_MODEL_ORDER

print("I: Using model order {}".format(model_order))

fig_dir = join(DIR_FIGURES, str(model_order))

# Composite Figure 6
DPI = 600

# Figure 7
fig_pixels = [int(7.5 * DPI), int(7.5 * DPI)]

INDIVID_OFFSET = [fig_pixels[0] // 2, 0]

SCATTER_OFFSET = [50, 0]

MODE_OFFSET = [50, (fig_pixels[0] // 15) * 9]

# Create a blank figure
f = Image.new('RGBA', fig_pixels, 'white')

# Start with the individual plot
indiv = Image.open(join(fig_dir, '45_perparticipant_modes.png'))
f.paste(indiv, INDIVID_OFFSET)

# Add the scatter plot
scatter = Image.open(join(fig_dir, '42_mode_dist.png'))
f.paste(scatter, SCATTER_OFFSET)

# Add the all/surviving modes PSD plot
modes = Image.open(join(fig_dir, '42_all_surviving_modes_detail.png'))
f.paste(modes, MODE_OFFSET)

# Add some text
font = ImageFont.truetype(FONT, 120)

draw = ImageDraw.Draw(f)

draw.text((100, 100),
          'A', font=font, fill=(85, 85, 85, 255))

draw.text((100, MODE_OFFSET[1] + 50),
          'B', font=font, fill=(85, 85, 85, 255))

draw.text((INDIVID_OFFSET[0] + 50, 100),
          'C', font=font, fill=(85, 85, 85, 255))

# Now create a non-alpha version to save
final = Image.new("RGB", f.size, (255, 255, 255))
final.paste(f, mask=f.split()[3])

final.save(join(fig_dir, '66_figure6.png'), dpi=(DPI, DPI))
