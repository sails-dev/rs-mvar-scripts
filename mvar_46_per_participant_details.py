#!/usr/bin/python3

from sys import argv, exit
from os import makedirs
from os.path import join

import h5py

import numpy as np
import scipy.stats as ss

import matplotlib.pyplot as plt
# import matplotlib.image as mpimg

import sails
from anamnesis import obj_from_hdf5group

from mvarconfig import (DIR_NULLDIST, DIR_MODALSTATS, DIR_FIGURES,
                        FILE_CIRCOS_GROUPS, FILE_CIRCOS_REGIONS,
                        DEFAULT_MODEL_ORDER)


def add_freq_tabs(ax, ymn, ymx, add_height, txt_height, bands, band_names):
    # Hard-coded to deal with lack of gamma in this case but we want the
    # alphas to match plots with gamma in
    num_bands = 5

    for freqnum, (b, g) in enumerate(zip(bands, band_names)):
        alpha = 0.15 + ((freqnum / num_bands) * 0.25)

        low = b[0] + .25
        high = b[1] - .25

        xy = [(low, ymx), (low, ymn), (high, ymn), (high, ymx),
              (high - .5, ymx+add_height), (low + .5, ymx+add_height)]

        r = plt.Polygon(xy, closed=True, alpha=alpha)
        r.set_facecolor('gray')

        ax.add_patch(r)

        ax.text(np.mean(b), txt_height,
                g, horizontalalignment='center', fontsize=7)


if len(argv) not in (1, 2):
    print("mvar_46_per_participant_details.py [MODEL_ORDER]")
    exit(1)

if len(argv) == 2:
    model_order = int(argv[1])
else:
    # Default model order
    model_order = DEFAULT_MODEL_ORDER

print("I: Using model order {}".format(model_order))

modal_out_dir = join(DIR_MODALSTATS, str(model_order))
fig_out_dir = join(DIR_FIGURES, str(model_order))

makedirs(modal_out_dir, exist_ok=True)
makedirs(fig_out_dir, exist_ok=True)


# Configuration
bands = [(0, 3), (3, 7), (7, 13), (13, 30)]
band_names = [r'$\delta$', r'$\theta$', r'$\alpha$', r'$\beta$']
to_plot = [('100307', '3'), ('179245', '3'), ('406836', '3'), ('917255', '3')]

plt.rcParams.update({'font.size': 7})

models = []
Mobjs = []
Fobjs = []
freq_vect = None
Fs = []
dt_thresholds = []

# Modes surviving and non-surviving
Ms = []
Mn = []

Fspsd = []
Mspsd = []

for ii, (person, run) in enumerate(to_plot):
    name = '{}_{}.hdf5'.format(person, run)
    print("I: Loading {}".format(name))

    filename = join(DIR_NULLDIST, str(model_order), name)

    f = h5py.File(filename, 'r')

    model = obj_from_hdf5group(f['model'])
    M = obj_from_hdf5group(f['M'])
    F = obj_from_hdf5group(f['F'])
    max_perm_dts = f['max_perm_dts'][...]

    dt_thresh = ss.scoreatpercentile(max_perm_dts, 99)

    # Extract surviving / non-surviving modes
    s_mode_inds = np.where(M.modes.dampening_time > dt_thresh)[0]
    n_mode_inds = np.where(M.modes.dampening_time <= dt_thresh)[0]

    if freq_vect is None:
        freq_vect = M.freq_vect

    ms = sails.ModalMvarMetrics.initialise_from_modes(model, M.modes,
                                                      M.sample_rate,
                                                      M.freq_vect,
                                                      mode_inds=s_mode_inds)

    mn = sails.ModalMvarMetrics.initialise_from_modes(model, M.modes,
                                                      M.sample_rate,
                                                      M.freq_vect,
                                                      mode_inds=n_mode_inds)

    models.append(model)
    Mobjs.append(M)
    Fobjs.append(F)

    f_m = np.einsum('iij->ij', F.PSD[:, :, :, 0]).mean(axis=0)
    s_m = np.einsum('iij->ij', ms.PSD[:, :, :, 0]).mean(axis=0)
    n_m = np.einsum('iij->ij', mn.PSD[:, :, :, 0]).mean(axis=0)

    Fs.append(f_m[..., None])
    Ms.append(s_m[..., None])
    Mn.append(n_m[..., None])

    mspsd = ms.PSD[:, :, np.argmax(s_m), 0][..., None]
    Mspsd.append(mspsd)

    fspsd = F.PSD[:, :, np.argmax(n_m), 0][..., None]
    Fspsd.append(fspsd)

    ms_S = ms.S[:, :, np.argmax(s_m), 0][..., None]

    dt_thresholds.append(dt_thresh)

    # Save out these arrays for use in MATLAB
    outname = '46_part_{}_{}.hdf5'.format(person, run)

    outfilename = join(modal_out_dir, outname)

    print("I: Saving {}".format(outfilename))

    outf = h5py.File(outfilename, 'w')

    outf.create_dataset('F_M', data=f_m)
    outf.create_dataset('S_M', data=s_m)
    outf.create_dataset('N_M', data=n_m)
    outf.create_dataset('Ms_PSD', data=mspsd)
    outf.create_dataset('F_PSD', data=fspsd)
    outf.create_dataset('Ms_S', data=ms_S)

    outf.close()

# Combine into arrays
Fs = np.concatenate(Fs, axis=-1)
Ms = np.concatenate(Ms, axis=-1)
Mn = np.concatenate(Mn, axis=-1)
Mspsd = np.concatenate(Mspsd, axis=-1)
Fspsd = np.concatenate(Fspsd, axis=-1)


# Summary plot across all participants
fig = plt.figure(figsize=(4, 5))

ax = fig.gca()

plt.plot(np.abs(Fs).mean(axis=1))
plt.plot(np.abs(Fs).mean(axis=1) - np.abs(Ms).mean(axis=1))
plt.plot(np.abs(Ms).mean(axis=1))
plt.grid(False)

ax2 = ax.twiny()

ax2.set_xticks([])
ax2.set_xticklabels([])
ax2.set_xlim(0, 30)
ax.set_xlim(0, 30)
ax2.xaxis.tick_top()
ax.xaxis.tick_bottom()

for tag in ['right', 'top', 'left', 'bottom']:
    ax.spines[tag].set_visible(False)
    ax2.spines[tag].set_visible(False)

ax.set_xlabel('Frequency (Hz)')

ymn, ymx = ax.get_ylim()

add_height = (ymx - ymn) * 0.1
txt_height = ymx + (add_height * 0.5)

ax.set_ylim(ymn, ymx+add_height)

add_freq_tabs(ax2, ymn, ymx, add_height, txt_height, bands, band_names)

leg = ax.legend(['All', 'Non-Surviving', 'Surviving'], facecolor='white')

# Shove the legend down a bit
bb = leg.get_bbox_to_anchor().inverse_transformed(ax.transAxes)
yOffset = 0.4
bb.y0 -= yOffset
bb.y1 -= yOffset
leg.set_bbox_to_anchor(bb, transform=ax.transAxes)

filename = join(fig_out_dir, '46_allpart_summary.png'.format(model_order))
print("I: Saving {}".format(filename))
plt.savefig(filename, dpi=600, transparent=True)

# Big per-participant plot

# Load Circos and channel colouring data
c = sails.CircosHandler.from_csv_files(FILE_CIRCOS_GROUPS, FILE_CIRCOS_REGIONS,
                                       analysis_column='AnalysisID_NewSort')

color_dict = c.colour_mapping(normalise=True)
chan_colors = {}

for group in c.groups:
    gcolor = c.groups[group].circoscolour
    for r in c.groups[group].regions.keys():
        chan_colors[r] = color_dict[gcolor]

chan_cols = []
for chan in c.data_regions:
    chan_cols.append(chan_colors[chan])

vm = 25
run_gap = 2
part_gap = 10
run_size = part_gap + run_gap + run_gap

# Elsevier double column width is 190mm
# We want 1 x 1 ratio in inches, so 7.5inches x 7.5 inches
# Need to leave space on the right for the overlays
fig = plt.figure(figsize=(5.5, 7.5))
scale = .9

# Start with the power/frequency mode plots for Fourier analysis
ax_pos = [(.06, .08+jj*.24*scale, .20*scale, .18*scale) for jj in range(len(to_plot))]
s_max = 4
y_max = 4.8
for ii in range(len(to_plot)):
    model = models[ii]
    F = Fobjs[ii]
    # dt_thresh = dt_thresholds[ii]

    # s_mode_inds = np.where(M.modes.dampening_time > dt_thresh)[0]
    # n_mode_inds = np.where(M.modes.dampening_time <= dt_thresh)[0]

    ax = plt.axes(ax_pos[ii])

    for tag in ['top', 'right']:
        ax.spines[tag].set_visible(False)

    add_freq_tabs(ax, 0, s_max, s_max / 10, s_max, bands, band_names)

    plt.grid(False)

    for k in range(model.nsignals):
        plt.plot(freq_vect, np.abs(F.S[k, k, :, 0])*1e4,
                 linewidth=0.5, color=chan_cols[k])

    plt.xlim(0, 30)
    plt.ylim(0, y_max)
    ax.spines['left'].set_bounds(0, s_max)
    ax.set_yticks(range(5))
    plt.ylabel('Power (a.u)')
    if ii == 0:
        plt.xlabel('Frequency (Hz)')

# Add a master legend to the top
ax = plt.axes([.06, .9, .16, .02], visible=True)
for tag in ['top', 'right', 'bottom', 'left']:
    ax.spines[tag].set_visible(False)
ax.set_xticks([])
ax.set_yticks([])
ax.text(0.5, 0.5, 'Fourier PSD', fontsize=7,
        horizontalalignment='center', verticalalignment='center')

# Now the Damping time/frequency
ax_pos = [(.33, .08+jj*.24*scale, .20*scale, .18*scale) for jj in range(len(to_plot))]
for ii in range(len(to_plot)):
    model = models[ii]
    M = Mobjs[ii]
    dt_thresh = dt_thresholds[ii]

    s_mode_inds = np.where(M.modes.dampening_time > dt_thresh)[0]
    n_mode_inds = np.where(M.modes.dampening_time <= dt_thresh)[0]

    ax = plt.axes(ax_pos[ii])

    for tag in ['top', 'right']:
        ax.spines[tag].set_visible(False)

    add_freq_tabs(ax, 0, 40, 4, 40, bands, band_names)

    plt.grid(False)

    plt.scatter(M.modes.peak_frequency[n_mode_inds],
                np.abs(M.modes.dampening_time[n_mode_inds]),
                marker='.', color='k', alpha=0.5,
                label="All Modes")

    plt.scatter(M.modes.peak_frequency[s_mode_inds],
                np.abs(M.modes.dampening_time[s_mode_inds]),
                marker='.', color='r',
                label="Modes with significant\ndamping times")

    plt.xlim(0, 30)
    plt.ylim(0, 48)
    ax.spines['left'].set_bounds(0, 40)
    ax.set_yticks([0, 10, 20, 30, 40])
    plt.ylabel('Damping Time (s)')
    if ii == 0:
        plt.xlabel('Frequency (Hz)')

# Add a master legend to the top
handles, labels = plt.gca().get_legend_handles_labels()
ax = plt.axes([.3, .9, .2, .05], visible=True)
for tag in ['top', 'right', 'bottom', 'left']:
    ax.spines[tag].set_visible(False)
ax.set_xticks([])
ax.set_yticks([])
ax.legend(handles, labels, loc='center', fontsize=7, frameon=False)

# Now the Damping time information
ax_pos = [(.6, .08+jj*.24*scale, .20*scale, .18*scale) for jj in range(len(to_plot))]

for ii in range(len(to_plot)):
    ax = plt.axes(ax_pos[ii])

    for tag in ['top', 'right']:
        ax.spines[tag].set_visible(False)

    add_freq_tabs(ax, 0, 10, 1, 10, bands, band_names)

    plt.grid(False)
    plt.plot(freq_vect, Fs[:, ii]*1e7, 'k', label='Full Spectrum', linewidth=2)
    plt.plot(freq_vect, Ms[:, ii]*1e7, 'r', label='Oscillations with \nLong Damping Time', linewidth=2)

    plt.xlim(0, 30)
    # plt.ylim(0, Fs[ii].max()*1.1+Fs[ii].max()*.1)
    plt.ylim(0, 12)
    ax.spines['left'].set_bounds(0, 10)
    ax.set_yticks([0, 2, 4, 6, 8, 10])
    plt.ylabel('Power (a.u)')
    if ii == 0:
        plt.xlabel('Frequency (Hz)')

# Add a master legend to the top
handles, labels = plt.gca().get_legend_handles_labels()
ax = plt.axes([.58, .9, .2, .05], visible=True)
for tag in ['top', 'right', 'bottom', 'left']:
    ax.spines[tag].set_visible(False)
ax.set_xticks([])
ax.set_yticks([])
ax.legend(handles, labels, loc='center', fontsize=7, frameon=False)

# Add the netmats
ax_pos = [(.82, .062+jj*.24*scale, .18*scale, .18*scale) for jj in range(len(to_plot))]
for ii in range(4):
    ax = plt.axes(ax_pos[ii])
    cscale = 1e6
    ax = c.plot_netmat(Mspsd[:, :, ii] * cscale,
                       labels=False,
                       ax=ax,
                       cmap='hot_r',
                       vmin=0.0)

    plt.colorbar(ax.collections[0], orientation='horizontal')

ax = plt.axes([.82, .90, .16, .02], visible=True)
for tag in ['top', 'right', 'bottom', 'left']:
    ax.spines[tag].set_visible(False)
ax.set_xticks([])
ax.set_yticks([])
ax.text(0.5, 0.5, 'SSE CSD Matrix', fontsize=7,
        horizontalalignment='center', verticalalignment='center')

filename = join(fig_out_dir, '46_participant_examples.png'.format(model_order))
print("I: Saving {}".format(filename))
plt.savefig(filename, dpi=600)
