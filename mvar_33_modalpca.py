#!/usr/bin/python3

from sys import argv, exit
from os.path import join
import multiprocessing

import h5py

import numpy as np

from scipy import stats

from sails import PCA

from mvarconfig import (DIR_MODALSTATS, PROC_NUM,
                        DEFAULT_MODEL_ORDER)

# Select data and preproc options
PREPROC = None

# Number of PCs for main analysis
NPCS = 5

# Whether to use 'full' or 'triu' (upper trianglar) for PCA
MATRIX = 'full'

# Netmat threshold in number of SDs
NETMAT_THRESH = 4

# Split-half analysis options
SPLIT_HALVES_NUMBER = 500
SPLIT_HALVES_NPCS = 50

# We will extract the data for each of these frequency bands
FREQ_BANDS = {'theta': (1, 7),
              'alpha': (7, 13),
              'beta': (13, 30),
              'low': (0, 5),
              'high': (15, 100)}


def split_half_pca(num, dat, half, sp, sh_npcs):
    """
    Take the data, original PCA object and run the split-half analysis on it.

    In a function so that we can use multiprocessing
    """

    # Print an update every 50 permutations
    if num % 50 == 0:
        print("I: Split-half run {}".format(num))

    perm = np.random.permutation(dat.shape[1])

    pca1 = PCA(dat[:, perm[:half]].T, sh_npcs)
    pca2 = PCA(dat[:, perm[half:]].T, sh_npcs)

    split_C = np.corrcoef(pca1.components, pca2.components)[sh_npcs:, 0]

    # Check the correlations with the original PCs
    split_C_orig_p1 = np.corrcoef(pca1.components, sp.components)[sh_npcs:, 0:sh_npcs].T
    split_C_orig_p2 = np.corrcoef(pca2.components, sp.components)[sh_npcs:, 0:sh_npcs].T

    return split_C, split_C_orig_p1, split_C_orig_p2


#####################################################################
# Main script
#####################################################################

if len(argv) not in (1, 2):
    print("mvar_33_modalpca.py [MODEL_ORDER]")
    exit(1)

if len(argv) == 2:
    model_order = int(argv[1])
else:
    # Default model order
    model_order = DEFAULT_MODEL_ORDER

print("I: Using model order {}".format(model_order))

inout_dir = join(DIR_MODALSTATS, str(model_order))

inf = h5py.File(join(inout_dir, '30_modal_summary.hdf5'), 'r')

psd = inf['PSD'][...]
freqs = inf['PF'][...]
dts = inf['DT'][...]
dt_thresholds = inf['dt_thresholds'][...]
part_nums = inf['part_idx'][...]
run_nums = inf['run_nums'][...]

num_rois = psd.shape[0]

# Process the data for each frequency band
# TODO: DOCUMENT THIS BIT
thresh = np.median(psd.std(axis=(0, 1))) * NETMAT_THRESH
goods = psd.std(axis=(0, 1)) < thresh

for bandname in sorted(FREQ_BANDS):
    lowf, highf = FREQ_BANDS[bandname]

    # Select the relevant modes
    print("I: Processing {}: {}Hz-{}Hz".format(bandname, lowf, highf))

    freqsel = (freqs > lowf) & (freqs < highf)
    inds = goods * freqsel

    print('I: {0}/{1} modes selected'.format(inds.sum(), len(inds)))

    # Either use the full PSD matrix or the triangular upper
    if MATRIX == 'full':
        dat = psd[:, :, inds].reshape(num_rois*num_rois, -1)
    elif MATRIX == 'triu':
        triu_inds = np.triu_indices(num_rois, 1)

        dat = np.array([psd[triu_inds[0], triu_inds[1], ii] for ii in range(psd.shape[2])]).T
        dat = dat[:, inds]
    else:
        raise ValueError("Bad MATRIX option {}".format(MATRIX))

    # Pre-processing
    if PREPROC == 'demean':
        dat = dat - dat.mean(axis=1)[:, None]
    elif PREPROC == 'zscore':
        dat = stats.zscore(dat, axis=1)
    elif PREPROC == 'ppz':
        # Per-participant normalisation
        for part in set([int(x) for x in part_nums]):
            jj = (part_nums[inds] == part)
            dat[:, jj] = (dat[:, jj] - dat[:, jj].mean()) / dat[:, jj].std()
    elif PREPROC is None:
        pass
    else:
        raise ValueError("Bad PREPROC option {}".format(PREPROC))

    part_nums_filt = part_nums[inds]
    run_nums_filt = run_nums[inds]
    dts_filt = dts[inds]
    freqs_filt = freqs[inds]

    aweights = dts[inds]
    aweights = aweights / aweights.max()

    # Perform PCA and component projection
    sp = PCA(dat.T, NPCS)

    # Store the max/min projection values
    projections = np.zeros((NPCS, 2, num_rois*num_rois))

    # Pull out projections based on the max/min value
    for ii in range(NPCS):
        sc = np.zeros((NPCS, ))
        sc[ii] = sp.scores[:, ii].max()
        projections[ii, 0, :] = sp.project_score(sc)

        sc = np.zeros((NPCS, ))
        sc[ii] = sp.scores[:, ii].min()
        projections[ii, 1, :] = sp.project_score(sc)

    # Unfold and prepare data for saving
    if MATRIX == 'full':
        pcs = sp.components.T.reshape(num_rois, num_rois, NPCS)
        pcs_load = sp.loadings.T.reshape(num_rois, num_rois, NPCS)
    elif MATRIX == 'triu':
        pcs = np.zeros((num_rois, num_rois, NPCS))
        for ii in range(NPCS):
            pcs[triu_inds[0], triu_inds[1], ii] = sp.components[ii, :]
    else:
        raise ValueError("Bad MATRIX option {}".format(MATRIX))

    # Run a split half analysis to examine component reproducibility
    half = np.floor(dat.shape[1] / 2).astype(int)

    # Run the split-half analysis using multiprocessing
    mp = multiprocessing.Pool(processes=PROC_NUM)

    args = [(x, dat, half, sp, SPLIT_HALVES_NPCS) for x in range(SPLIT_HALVES_NUMBER)]

    res = mp.starmap(split_half_pca, args)

    # Store the split half correlations
    split_C = np.array([x[0] for x in res])

    # And the correlations of each half with the full PCs
    split_C_original = np.array([np.dstack([x[1], x[2]]) for x in res])

    # Save out our data
    outpath = join(inout_dir, '33_hcp_pca_{}_params.hdf5'.format(bandname))

    print("I: Saving data to {}".format(outpath))

    outf = h5py.File(outpath, 'w')

    # Save the raw PCA data
    sp.to_hdf5(outf.create_group('pca'))
    outf.create_dataset('dat', data=dat.T)
    outf.create_dataset('pcs', data=pcs)
    outf.create_dataset('projections', data=projections)
    outf.create_dataset('part_nums', data=part_nums_filt)
    outf.create_dataset('run_nums', data=run_nums_filt)
    outf.create_dataset('dts', data=dts_filt)
    outf.create_dataset('freqs', data=freqs_filt)

    # Save the split-half data
    outf.create_dataset('split_C', data=split_C)
    outf.create_dataset('split_C_original', data=split_C_original)

    outf.close()
