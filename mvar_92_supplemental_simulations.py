#!/usr/bin/python3

import warnings

from os.path import join

import numpy as np
import scipy.stats as ss
from scipy import signal
import matplotlib.pyplot as plt
plt.style.use('ggplot')
plt.rcParams.update({'font.size': 7})

import sails

from mvarconfig import (DIR_SIMULATION)


# Our version of scipy irritatingly has FutureWarning's triggered in it
# so hide them
warnings.simplefilter(action='ignore', category=FutureWarning)

# ---------------------------------------------------------------------
# 3-Mode simulation

def generate_basesignal(f, r, sample_rate, num_samples):
    if f > 0:
        wr = (2 * np.pi * f) / sample_rate
        a1 = np.array([1, -2*r*np.cos(wr), (r**2)])
    else:
        a1 = np.poly([r])

    return signal.filtfilt(1, a1, np.random.randn(1, num_samples)).T


def get_connection_weights(form='vector'):
    weight1 = np.array([1, 1, 1, 0, 0, 0, 1, 1, 1, 0])
    weight2 = np.array([0, 0, 0, .5, .7, .9, 1, .9, .7, .5])
    weight3 = np.array([1, 0, 1, 0, 1, 0, 1, 0, 1, 0])

    if form == 'vector':
        return weight1, weight2, weight3
    elif form == 'matrix':
        return weight1[:, None].dot(weight1[None, :]), \
               weight2[:, None].dot(weight2[None, :]), \
               weight3[:, None].dot(weight3[None, :])


sample_rate = 128
num_seconds = 300
model_order = 7

num_samples = int(sample_rate * num_seconds)
delay_vect = np.arange(model_order + 1)
freq_vect = np.linspace(0, sample_rate/2, 512)

weights = get_connection_weights()

x1 = ss.zscore(generate_basesignal(0, 0.71, sample_rate, num_samples)) * weights[0][None, :]
x2 = ss.zscore(generate_basesignal(9, 0.85, sample_rate, num_samples)) * weights[1][None, :]
x3 = ss.zscore(generate_basesignal(42, 0.75, sample_rate, num_samples)) * weights[2][None, :]

X = (x1 + x2 + x3).T
X = X[:, :, None]
X = X + np.random.randn(*X.shape)

model = sails.modelfit.VieiraMorfLinearModel.fit_model(X, delay_vect)
modes = sails.MvarModalDecomposition.initialise(model, sample_rate)

F = sails.FourierMvarMetrics.initialise(model, sample_rate, freq_vect)
M = sails.ModalMvarMetrics.initialise_from_modes(model, modes, sample_rate, freq_vect)

m1 = [2]
m2 = [0, 1]
m3 = [3, 4]

M1 = sails.ModalMvarMetrics.initialise_from_modes(model, modes, sample_rate, freq_vect, mode_inds=m1)
M2 = sails.ModalMvarMetrics.initialise_from_modes(model, modes, sample_rate, freq_vect, mode_inds=m2)
M3 = sails.ModalMvarMetrics.initialise_from_modes(model, modes, sample_rate, freq_vect, mode_inds=m3)

plt.style.use('ggplot')

plt.figure(figsize=(12, 4))
plt.subplot(121)
y = np.einsum('iijk->ij', np.abs(F.S))
plt.plot(freq_vect, y.T, 'k')
plt.xlabel('Frequency (Hz)')
plt.ylabel('Power')
plt.title('Fourier Spectrum')
plt.subplot(122)
y = np.einsum('iijk->ij', np.abs(M1.S))
plt.plot(freq_vect, y.T, 'b')
y = np.einsum('iijk->ij', np.abs(M2.S))
plt.plot(freq_vect, y.T, 'r')
y = np.einsum('iijk->ij', np.abs(M3.S))
plt.plot(freq_vect, y.T, 'g')
plt.title('Modal Spectrum')
plt.xlabel('Frequency (Hz)')

plt.savefig(join(DIR_SIMULATION, '92_sim_threenode_spectra.png'), dpi=300)


plt.figure(figsize=(12, 4))
ax = plt.subplot(121)
modes.pole_plot(ax=ax)
ax = plt.subplot(122)
plt.plot(modes.peak_frequency, modes.dampening_time, '+')
plt.xlabel('Frequency (Hz)')
plt.ylabel('Damping Time')

plt.savefig(join(DIR_SIMULATION, '92_sim_threenode_poles.png'), dpi=300)

plt.figure(figsize=(12, 4))
plt.subplot(131)
inds = freq_vect < 1
y = np.abs(M1.S[:, :, inds, 0].mean(axis=2))
plt.pcolormesh(y, cmap='Blues')
plt.title('Mode 1 Network')
plt.xticks(np.arange(10)+0.5, np.arange(10)+1)
plt.yticks(np.arange(10)+0.5, np.arange(10)+1)

plt.subplot(132)
inds = np.logical_and(freq_vect > 8, freq_vect < 10)
y = np.abs(M2.S[:, :, inds, 0].mean(axis=2))
plt.pcolormesh(y, cmap='Reds')
plt.title('Mode 2 Network')
plt.xticks(np.arange(10)+0.5, np.arange(10)+1)
plt.yticks(np.arange(10)+0.5, np.arange(10)+1)

plt.subplot(133)
inds = np.logical_and(freq_vect > 40, freq_vect < 44)
y = np.abs(M3.S[:, :, inds, 0].mean(axis=2))
plt.pcolormesh(y, cmap='Greens')
plt.title('Mode 3 Network')
plt.xticks(np.arange(10)+0.5, np.arange(10)+1)
plt.yticks(np.arange(10)+0.5, np.arange(10)+1)

plt.savefig(join(DIR_SIMULATION, '92_sim_threenode_netmats.png'), dpi=300)

# ---------------------------------------------------------------------
# Noise varying simulation

X = (x1 + x2 + x3).T
X = X[:, :, None]

spec = np.zeros((10, 10, 512, 12))
dat = np.zeros((10, 1000, 12))
noises = np.linspace(0.2, 5, 12)
mm = []
for ii in range(12):
    Y = X + np.random.randn(*X.shape)*noises[ii]
    Y = ss.zscore(Y, axis=1)
    dat[:, :, ii] = Y[:, 1000:2000, 0]

    model = sails.modelfit.VieiraMorfLinearModel.fit_model(Y, delay_vect)
    mm.append(sails.MvarModalDecomposition.initialise(model, sample_rate))
    F = sails.FourierMvarMetrics.initialise(model, sample_rate, freq_vect)
    spec[:, :, :, ii] = np.abs(F.S[..., 0])

ind = 6
plt.figure(figsize=(12, 6))
plt.subplots_adjust(left=0.075, right=0.98, hspace=0.35)
ax1 = plt.subplot(132)
ax2 = plt.subplot(131)
ax3 = plt.subplot(133)
for ii in range(12):
    ax1.plot(freq_vect, spec[ind, ind, :, 11-ii], color=np.array([0.05, 0, 0])*(ii+1)*1.5)
    ax2.plot(dat[ind, :250, 11-ii] + 6*ii, color=np.array([0.05, 0, 0])*(ii+1)*1.5)
    ax3.plot(mm[11-ii].peak_frequency, mm[11-ii].dampening_time, '.', color=np.array([0.05, 0, 0])*(ii+1)*1.5)
ax2.set_title('Time-Series')
ax2.set_xlabel('Time (samples)')
ax2.set_ylabel('Noise StDev')
ax2.set_yticks(np.arange(12)*6)
ax2.set_yticklabels(np.round(noises[::-1], 1))
ax1.set_title('Power Spectra')
ax1.set_xlabel('Frequency (Hz)')
ax1.set_ylabel('Power')
ax3.set_title('Damping Times')
ax3.set_xlabel('Frequency (Hz)')
ax3.set_ylabel('Damping Time')

plt.savefig(join(DIR_SIMULATION, '92_sim_noiselevel.png'), dpi=300)


#%% -----------------------------------------------------


peak_freq = 12
sample_rate = 96
seconds = 600
noise_std = 0.02
x = sails.simulate.single_pole_oscillator(peak_freq, sample_rate, seconds, noise_std=noise_std, random_seed=42, r=.99)[0,:]
x = stats.zscore(x)

y = x.T + 0.25*x.T**2
y = y - signal.savgol_filter(y, 511, 1)

X = x[None,:,None]
Y = y[None,:,None]

delay_vect = np.arange(15)

m1 = sails.modelfit.VieiraMorfLinearModel.fit_model(X, delay_vect)
m2 = sails.modelfit.VieiraMorfLinearModel.fit_model(Y, delay_vect)

mo1 = sails.MvarModalDecomposition.initialise(m1, sample_rate)
mo2 = sails.MvarModalDecomposition.initialise(m2, sample_rate)

freq_vect = np.linspace(0, sample_rate/2)
F1 = sails.FourierMvarMetrics.initialise(m1, sample_rate, freq_vect)
F2 = sails.FourierMvarMetrics.initialise(m2, sample_rate, freq_vect)


plt.figure(figsize=(10, 10))
plt.subplot(3,2,1)
plt.plot(X[0,100:200,:], 'b')
plt.title('Sinusoidal Signal')
plt.xlabel('Time (samples)')
plt.subplot(3,2,2)
plt.plot(Y[0,100:200,:], 'r')
plt.title('Non-sinusoidal Signal')
plt.xlabel('Time (samples)')

plt.subplot(3,2,3)
plt.semilogy(freq_vect, F1.S[0,0,:,0], 'b')
plt.ylabel('Power (a.u.)')
plt.subplot(3,2,4)
plt.semilogy(freq_vect, F2.S[0,0,:,0], 'r')

plt.subplot(3,2,5)
plt.plot(mo1.peak_frequency, np.abs(mo1.evals), 'b.')
plt.xlim(freq_vect[0], freq_vect[-1])
plt.xlabel('Frequency(Hz)')
plt.ylabel('Eigenvalue Magnitude')
plt.subplot(3,2,6)
plt.plot(mo2.peak_frequency, np.abs(mo2.evals), 'r.')
plt.xlim(freq_vect[0], freq_vect[-1])
plt.xlabel('Frequency(Hz)')
plt.subplots_adjust(hspace=0.3, top=0.95)

plt.savefig(join(DIR_SIMULATION, '92_sim_nonsinusoidal.png'), dpi=300)

