function cm = get_redblue_cmap(clmin,clmax,N,centre_val,map)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if nargin<5 || isempty(map)
    map = 'BlueRed_r';
end

flip = false;
if strcmp(map(end-1:end),'_r')
    flip = true;
    map = map(1:end-2);
end

if nargin<4 || isempty(centre_val)
    centre_val = [0.865, 0.865, 0.865];
end

% scale to sensible order of magnitude
min_scale = floor(log10(abs(clmin)));
clmin = clmin*10.^-min_scale;

max_scale = floor(log10(abs(clmax)));
clmax = clmax*10.^-max_scale;

% Round up to nearest 1
clmin = -round(abs(clmin),1);
clmax = round(abs(clmax),1);

% make largest order of magnitude the ones unit
if abs(max_scale) < abs(min_scale)
    clmin = clmin ./ 10*(max_scale-min_scale);
elseif abs(max_scale) > abs(min_scale)
    clmax = clmax ./ 10*(min_scale-max_scale);
end

if strcmp('BlueRed',map)
    col1 = [0.230, 0.299, 0.754];
    col2 = [0.706, 0.016, 0.150];
elseif strcmp( 'PurpleOrange',map)
    col1 = [0.436, 0.308, 0.631];
    col2 = [0.759, 0.334, 0.046];
elseif strcmp( 'GreenPurple', map)
    col1 = [0.085, 0.532, 0.201];
    col2 = [0.436, 0.308, 0.631];
elseif strcmp('BlueTan',map)
    col1 = [0.217, 0.525, 0.910];
    col2 = [0.677, 0.492, 0.093];
elseif strcmp('GreenRed',map)
    col1 = [0.085, 0.532, 0.201];
    col2 = [0.758, 0.214, 0.233];
elseif strcmp('GreenFuchsia',map)
    col1 = [0.085, 0.6, 0.201];
    col2 = [.6,.1,.6];
else
    warning('Colourmap not recognised, please use one of {BlueRed,PurpleOrange,GreenPurple,BlueTan,GreenRed}');
end

if flip == true
    tmp = col2;
    col2 = col1;
    col1 = tmp;
end

% get positive values
max_ratio = abs(clmax)/ ( abs(clmin) + abs(clmax) );
max_lims = round(N*max_ratio);
c1 = cat(1,linspace(col1(1),centre_val(1),max_lims),...
           linspace(col1(2),centre_val(2),max_lims),...
           linspace(col1(3),centre_val(3),max_lims));

% get negative values
min_ratio = abs(clmin)/( abs(clmax)+abs(clmin) );
min_lims = round(N*min_ratio);
c2 = cat(1,linspace(centre_val(1),col2(1),min_lims),...
           linspace(centre_val(2),col2(2),min_lims),...
           linspace(centre_val(3),col2(3),min_lims));

% set map
cm = flipud(cat(2,c1,[centre_val;centre_val;centre_val],c2)');

end

