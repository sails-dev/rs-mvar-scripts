#!/bin/bash

set -e
set -u

function checkout_and_reset {
    repourl="$1"
    reponame="$2"

    if [ ! -d ${reponame} ]; then
        echo "I: Checking out ${repourl} into ${reponame}"
        git clone "${repourl}" "${reponame}"
    fi

    if [ ! -d ${reponame} ]; then
        echo "E: Failed to checkout ${repourl} to ${reponame}"
        exit 1
    fi

    if [ $# -gt 2 ]; then
        repohash="$3"

        pushd $(realpath ${reponame}) >/dev/null 2>&1

        curhead="$(git rev-parse HEAD)"

        if [ "${curhead}" != "${repohash}" ]; then
            echo "I: Resetting ${reponame} to ${repohash}"
            git reset --hard ${repohash}
        fi

        popd >/dev/null 2>&1

    else
        echo "I: No hash specified - leaving as-is"
    fi
}

# Check that we're in the right place
if [ ! -f checkout_external.sh ]; then
    echo "E: This script must be run from the external directory"
fi

# Script to checkout external repositories and set them at revisions used

# Main SAILS toolbox
checkout_and_reset https://vcs.ynic.york.ac.uk/analysis/sails sails 4f1204972f712c2925c10cf7b07ce2f49a7b265c

# Anamnesis
checkout_and_reset https://vcs.ynic.york.ac.uk/naf/anamnesis anamnesis 390a968de06346fb0a3dcb71badbf5d52c7b0ca5

# export_fig MATLAB toolbox
checkout_and_reset https://github.com/altmany/export_fig export_fig 99747242898c63deaa2d17cb6ec2658f7c1e2f75

# OSL needs a directory structure and a copy of spm12
mkdir -p osl
checkout_and_reset https://github.com/OHBA-analysis/osl-core osl/osl-core 4a33b4c049c443e001aad30342cc47eedae2a78c
checkout_and_reset https://github.com/OHBA-analysis/ohba-external osl/ohba-external 2e04d9f79f5d32b8a908eddc87a4a274d41fb70c
checkout_and_reset https://github.com/OHBA-analysis/MEG-ROI-nets osl/MEG-ROI-nets 3dbd03da2604bd39d9a3dc728ecc96acee452750
checkout_and_reset https://github.com/OHBA-analysis/parcellations osl/parcellations 5090bf62b311d7acea8c79c17a405d3f19228377
checkout_and_reset https://github.com/spm/spm12 osl/spm12 3085dac00ac804adb190a7e82c6ef11866c8af02

if [ ! -d osl/std_masks ]; then
    # We can't get this from a git repo. Instead, we pull one of AQs paper archives
    # and extract just the std_masks folder
    if [ ! -f osl/HMM_Task_Download.zip ]; then
        wget -O osl/HMM_Task_Download.zip https://osf.io/6kwdb/download
    fi

    # Unpack into a temporary directory then move
    pushd $(realpath osl) >/dev/null 2>&1

    mkdir -p tmp
    unzip HMM_Task_Download.zip 'HMM_Task_Download/toolboxes/osl/std_masks/*' -d tmp
    mv tmp/HMM_Task_Download/toolboxes/osl/std_masks .
    rm -fr tmp

    popd >/dev/null 2>&1
fi
