#!/usr/bin/python3

from sys import argv, exit
from os import makedirs
from os.path import join

import h5py

import numpy as np

import matplotlib.pyplot as plt

import sails

from mvarconfig import (DIR_MODALSTATS, DIR_FIGURES,
                        FILE_CIRCOS_GROUPS, FILE_CIRCOS_REGIONS,
                        DEFAULT_MODEL_ORDER)

if len(argv) not in (1, 2):
    print("mvar_42_mode_dist.py [MODEL_ORDER]")
    exit(1)

if len(argv) == 2:
    model_order = int(argv[1])
else:
    # Default model order
    model_order = DEFAULT_MODEL_ORDER

print("I: Using model order {}".format(model_order))

in_dir = join(DIR_MODALSTATS, str(model_order))
out_dir = join(DIR_FIGURES, str(model_order))

makedirs(out_dir, exist_ok=True)

# Get our summary data
filename = join(in_dir, '31_surviving_modes.hdf5')
print("I: Loading {}".format(filename))

f = h5py.File(filename, 'r')

all_psd = f['a_psd'][...]

surviving_dts = f['s_dts'][...]
surviving_pfs = f['s_pfs'][...]
surviving_runs = f['s_runs'][...]
surviving_psd = f['s_psd'][...]

non_surviving_dts = f['n_dts'][...]
non_surviving_pfs = f['n_pfs'][...]
non_surviving_runs = f['n_runs'][...]

dt_thresholds = f['dt_thresholds'][...]
dts_per_run = f['dts_per_run'][...]

person_list = list(f['person_list'][...])
run_list = list(f['run_list'][...])

freq_vect = f['freq_vect'][...]

f.close()

# Plot all modes on two graphs to demonstrate seperation

black_colour = (0.3, 0.3, 0.3)
red_colour = (0.9, 0.1, 0.1)

# Bands to show and their names
bands = [(0, 3), (3, 7), (7, 13), (13, 30), (30, 60)]
band_names = [r'$\delta$', r'$\theta$', r'$\alpha$', r'$\beta$', r'$\gamma$']
num_bands = len(band_names)

# Elsevier double column width is 190mm
plt.rcParams.update({'font.size': 7})

# New two versions of this plot.  Bit of a hack.
for version in ['42_mode_dist.png', '42_mode_dist_si.png']:
    is_si = version == '42_mode_dist_si.png'

    if is_si:
        w, h = 1.75, 3.5
        fontsize = 5
        tl = True
    else:
        w, h = 3.75, 4.5
        fontsize = 7
        tl = False

    fig = plt.subplots(nrows=1, ncols=5, figsize=(w, h), tight_layout=tl)

    ax1 = plt.subplot(1, 5, (2, 5))
    ax1R = ax1.twiny()
    ax1R.patch.set_alpha(0)

    ns_pfs = []
    ns_dts = []
    s_pfs = []
    s_dts = []

    ax1.scatter(non_surviving_pfs, non_surviving_dts, marker='.', alpha=0.1, color=black_colour)
    ax1.scatter(surviving_pfs, surviving_dts, marker='.', alpha=0.1, color=red_colour)

    ax2 = plt.subplot(1, 5, (1))

    bins = np.linspace(0, 50, 100)

    ns_count, _ = np.histogram(non_surviving_dts, bins)
    ns_count = ns_count / ns_count.sum()

    s_count, _ = np.histogram(surviving_dts, bins)
    s_count = s_count / s_count.sum()

    s_plt = plt.plot(s_count, bins[0:99], color=red_colour)
    ns_plt = plt.plot(ns_count, bins[0:99], color=black_colour)

    if is_si:
        ax2.set_ylabel('Damping Time (s)', fontsize=fontsize, labelpad=-1.5)
    else:
        ax2.set_ylabel('Damping Time (s)', fontsize=fontsize)
        ax2.get_yaxis().set_label_coords(-0.5, 0.5)

    if not is_si:
        ax1.set_xticklabels(['']*len(ax1.get_xticklabels()))

    ax2.set_xlabel('Prop', fontsize=fontsize)

    ax1R.set_xticks([])
    ax1R.set_xticklabels([])

    ax1.set_xlim(0, 60)
    ax1R.set_xlim(0, 60)

    if is_si:
        ax1.set_xticks([20, 40, 60])
        ax1.set_xticklabels([20, 40, 60])

        for item in ax1.get_xticklabels() + ax2.get_xticklabels() + ax2.get_yticklabels():
            item.set_fontsize(fontsize)

        ax1.set_xlabel('Frequency (Hz)', fontsize=fontsize)

    yt = ax2.get_yticks()
    ax1.set_yticks(yt)
    ax1.set_yticklabels([''] * len(yt))

    _, ymx = ax2.get_ylim()
    ymn = 0
    add_height = (ymx - ymn) * 0.03

    for freqnum, (b, g) in enumerate(zip(bands, band_names)):
        alpha = 0.15 + ((freqnum / num_bands) * 0.25)

        low = b[0] + .25
        high = b[1] - .25

        xy = [(low, ymx), (low, ymn), (high, ymn), (high, ymx),
              (high - .5, ymx+add_height), (low + .5, ymx+add_height)]

        r = plt.Polygon(xy, closed=True, alpha=alpha)
        r.set_facecolor('gray')

        ax1R.add_patch(r)

        ax1R.text(np.mean(b), ymx,
                  g, horizontalalignment='center', fontsize=fontsize)

    ax1.set_ylim(ymn, ymx+add_height)
    ax1R.set_ylim(ymn, ymx+add_height)
    ax2.set_ylim(ymn, ymx+add_height)

    for tag in ['top', 'right']:
        ax1.spines[tag].set_visible(False)
        ax1R.spines[tag].set_visible(False)
        ax2.spines[tag].set_visible(False)

    ax1.spines['left'].set_bounds(0, ymx)
    ax2.spines['left'].set_bounds(0, ymx)
    ax1R.spines['left'].set_visible(False)

    if is_si:
        ax1.set_title('Order {}'.format(model_order), fontsize=fontsize+2)

    plt.savefig(join(out_dir, version), dpi=600)

# Surviving mode plots
c = sails.CircosHandler.from_csv_files(FILE_CIRCOS_GROUPS, FILE_CIRCOS_REGIONS,
                                       analysis_column='AnalysisID_NewSort')

color_dict = c.colour_mapping(normalise=True)
chan_colors = {}

for group in c.groups:
    gcolor = c.groups[group].circoscolour
    for r in c.groups[group].regions.keys():
        chan_colors[r] = color_dict[gcolor]

chan_cols = []
for chan in c.data_regions:
    chan_cols.append(chan_colors[chan])


# All mode plot
fig = plt.subplots(nrows=1, ncols=5, figsize=(3.75, 3.75/2), tight_layout=False)

ax1 = plt.subplot(1, 5, (2, 5))
ax1R = ax1.twiny()

for k in range(all_psd.shape[0]):
    plt.plot(freq_vect, all_psd[k, :, :].mean(axis=0), color=chan_cols[k], alpha=0.3)

# Only put labels on top plot
_, ymx = ax1.get_ylim()
ymn = 0
add_height = (ymx - ymn) * 0.07

for freqnum, (b, g) in enumerate(zip(bands, band_names)):
    alpha = 0.15 + ((freqnum / num_bands) * 0.25)

    low = b[0] + .25
    high = b[1] - .25

    xy = [(low, ymx), (low, ymn), (high, ymn), (high, ymx),
          (high - .5, ymx+add_height), (low + .5, ymx+add_height)]

    r = plt.Polygon(xy, closed=True, alpha=alpha)
    r.set_facecolor('gray')

    ax1R.add_patch(r)

    # No text on this one

ax1.set_ylim(ymn, ymx+add_height)

ax2 = plt.subplot(1, 5, (1))
ax2.remove()

ax1.set_xticklabels([''] * len(ax1.get_xticks()))
ax1R.set_xticks([])
ax1R.set_xticklabels([])

ax1.set_xlim(0, 60)
ax1R.set_xlim(0, 60)

for tag in ['top', 'right']:
    ax1.spines[tag].set_visible(False)
    ax1R.spines[tag].set_visible(False)
    ax2.spines[tag].set_visible(False)

plt.subplots_adjust(bottom=0.20, top=0.95)

plt.savefig(join(out_dir, '42_all_modes.png'), dpi=600)

# Surviving mode plot
fig = plt.subplots(nrows=1, ncols=5, figsize=(3.75, 3.75/2), tight_layout=False)

ax1 = plt.subplot(1, 5, (2, 5))
ax1R = ax1.twiny()
ax1R.patch.set_alpha(0)

for k in range(surviving_psd.shape[0]):
    plt.plot(freq_vect, surviving_psd[k, :, :].mean(axis=0), color=chan_cols[k], alpha=0.3)

# Only put labels on top plot
_, ymx = ax1.get_ylim()
ymn = 0
add_height = (ymx - ymn) * 0.07

for freqnum, (b, g) in enumerate(zip(bands, band_names)):
    alpha = 0.15 + ((freqnum / num_bands) * 0.25)

    low = b[0] + .25
    high = b[1] - .25

    xy = [(low, ymx), (low, ymn), (high, ymn), (high, ymx),
          (high - .5, ymx+add_height), (low + .5, ymx+add_height)]

    r = plt.Polygon(xy, closed=True, alpha=alpha)
    r.set_facecolor('gray')

    ax1R.add_patch(r)

    # No text on this one

ax1.set_ylim(ymn, ymx+add_height)

ax2 = plt.subplot(1, 5, (1))
ax2.remove()

ax1.set_xlabel('Frequency (Hz)')

ax1R.set_xticks([])
ax1R.set_xticklabels([])

ax1.set_xlim(0, 60)
ax1R.set_xlim(0, 60)

for tag in ['top', 'right']:
    ax1.spines[tag].set_visible(False)
    ax1R.spines[tag].set_visible(False)
    ax2.spines[tag].set_visible(False)

plt.subplots_adjust(bottom=0.20, top=0.95)

plt.savefig(join(out_dir, '42_surviving_modes.png'), dpi=600)

# Alternate summary plot
fig = plt.subplots(nrows=1, ncols=5, figsize=(3.75, 3.75), tight_layout=False)

ax1 = plt.subplot(1, 5, (2, 5))
ax1R = ax1.twiny()
ax1R.patch.set_alpha(0)

plt.plot(freq_vect, all_psd.mean(axis=0).mean(axis=0), color=red_colour, alpha=0.8)
plt.plot(freq_vect, surviving_psd.mean(axis=0).mean(axis=0), color=black_colour, alpha=0.8)

# Only put labels on top plot
_, ymx = ax1.get_ylim()
ymn = 0
add_height = (ymx - ymn) * 0.07

for freqnum, (b, g) in enumerate(zip(bands, band_names)):
    alpha = 0.15 + ((freqnum / num_bands) * 0.25)

    low = b[0] + .25
    high = b[1] - .25

    xy = [(low, ymx), (low, ymn), (high, ymn), (high, ymx),
          (high - .5, ymx+add_height), (low + .5, ymx+add_height)]

    r = plt.Polygon(xy, closed=True, alpha=alpha)
    r.set_facecolor('gray')

    ax1R.add_patch(r)

    # No text on this one

ax1.set_ylim(ymn, ymx+add_height)

ax2 = plt.subplot(1, 5, (1))
ax2.remove()

ax1.set_xlabel('Frequency (Hz)')
ax1.set_ylabel('Power Spectral Density')

ax1R.set_xticks([])
ax1R.set_xticklabels([])

ax1.set_xlim(0, 60)
ax1R.set_xlim(0, 60)

for tag in ['top', 'right']:
    ax1.spines[tag].set_visible(False)
    ax1R.spines[tag].set_visible(False)
    ax2.spines[tag].set_visible(False)

plt.subplots_adjust(bottom=0.20, top=0.95)

plt.savefig(join(out_dir, '42_all_surviving_modes.png'), dpi=600)

# Alternate alternate summary plot
fig = plt.subplots(nrows=1, ncols=5, figsize=(3.75, 3), tight_layout=False)

ax1 = plt.subplot(1, 5, (2, 5))
ax1R = ax1.twiny()
ax1R.patch.set_alpha(0)

# Plot the PSD of all of the modes in black
plt.plot(freq_vect, all_psd.mean(axis=1).T, color=black_colour, alpha=0.3, linewidth=0.5)
# Plot the extracted modes in red and emphasise to make the point clear
plt.plot(freq_vect, surviving_psd.mean(axis=1).T, color=red_colour, alpha=0.3, linewidth=0.9)

# Only put labels on top plot
_, ymx = ax1.get_ylim()
ymn = 0
add_height = (ymx - ymn) * 0.03

for freqnum, (b, g) in enumerate(zip(bands, band_names)):
    alpha = 0.15 + ((freqnum / num_bands) * 0.25)

    low = b[0] + .25
    high = b[1] - .25

    xy = [(low, ymx), (low, ymn), (high, ymn), (high, ymx),
          (high - .5, ymx+add_height), (low + .5, ymx+add_height)]

    r = plt.Polygon(xy, closed=True, alpha=alpha)
    r.set_facecolor('gray')

    ax1R.add_patch(r)

    ax1R.text(np.mean(b), ymx,
              g, horizontalalignment='center', fontsize=fontsize)

ax1.set_ylim(ymn, ymx+add_height)
ax1.ticklabel_format(style='sci', scilimits=(0, 0), axis='y')

ax2 = plt.subplot(1, 5, (1))
ax2.remove()

ax1.set_xlabel('Frequency (Hz)')
ax1.set_ylabel('Power Spectral Density')

ax1R.set_xticks([])
ax1R.set_xticklabels([])

ax1.set_xlim(0, 60)
ax1R.set_xlim(0, 60)

for tag in ['top', 'right']:
    ax1.spines[tag].set_visible(False)
    ax1R.spines[tag].set_visible(False)
    ax2.spines[tag].set_visible(False)

plt.subplots_adjust(bottom=0.20, top=0.95)

plt.savefig(join(out_dir, '42_all_surviving_modes_detail.png'), dpi=600)
