#!/usr/bin/python3 -u

# Run model permutations locally
from sys import argv, exit
import multiprocessing
import subprocess

from os import makedirs
from os.path import dirname, join
from time import time

import numpy as np

from mvarconfig import (PARTICIPANTS, RUNS, DIR_LOGFILES,
                        PROC_NUM, DEFAULT_MODEL_ORDER)

SCRIPT_PATH = join(dirname(argv[0]), 'mvar_21_permute_models.py')


def process_run(subj, run, model_order):
    """
    Just call mvar_21_permute_models.py, capture the output and
    dump into a log.
    """

    print("Starting run for {}/{}".format(subj, run))
    logname = 'modelperm_local_{}_order_{}_{}.o{}'.format(subj, run,
                                                          model_order, int(time()))
    logfilename = join(DIR_LOGFILES, logname)

    error = False

    with open(logfilename, 'w') as logfile:
        ret = subprocess.run([SCRIPT_PATH, str(subj), str(run), str(model_order)],
                             stdout=logfile, stderr=logfile)

        if ret.returncode != 0:
            print("E: Failed to run for {}/{}: check log".format(subj, run))
            error = True

    return error


if len(argv) not in (1, 2):
    print("mvar_20_local_permute_models.py [MODEL_ORDER]")
    exit(1)

if len(argv) == 2:
    model_order = int(argv[1])
else:
    # Default model order
    model_order = DEFAULT_MODEL_ORDER

print("I: Using model order {}".format(model_order))

# Set up log area
makedirs(DIR_LOGFILES, exist_ok=True)

# Prepare a list of jobs to run
to_run = []

for participant in PARTICIPANTS:
    for run in RUNS:
        to_run.append((participant, run, model_order))

# Run the jobs using multiprocessing
p = multiprocessing.Pool(processes=PROC_NUM)
res = p.starmap(process_run, to_run)

res = np.array(res)

if res.any():
    print("E: At least one error when running: Check logs")
    exit(1)

exit(0)
