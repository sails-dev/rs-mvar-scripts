#!/bin/bash

# Run beamforming using an SGE cluster
# Assumes that matlab binary is on the PATH

set -e
set -u

# Load our settings
CURDIR=$(realpath $(dirname $0))
. mvarconfig.ini

# Decide which script we want to run
SCRIPT=mvar_01_beamform_participant

# Ensure that we have a log directory
mkdir -p "${DIR_BASE}/${DIR_LOGFILES}"

# Submit one job per-participant, per-run
for SUBJ in ${PARTICIPANTS}; do
    echo "Submitting ${SCRIPT} ${SUBJ}"

    qsub -j y -N beamform_${SUBJ} -o "${DIR_BASE}/${DIR_LOGFILES}/" << EOF
#!/bin/bash

set -e
set -u

date -u
# Need to run MATLAB script from within correct directory
cd ${CURDIR}
xvfb-run -a matlab -r "${SCRIPT}('${SUBJ}'); exit;"
date -u

EOF

done
