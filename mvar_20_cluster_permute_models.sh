#!/bin/bash

# Run model permutations using an SGE cluster

set -e
set -u

# Load our settings
CURDIR=$(realpath $(dirname $0))
. mvarconfig.ini

# The requested model order can be passed as an optional argument
if [ $# -gt 0 ]; then
    MODEL_ORDER=${1}
else
    MODEL_ORDER=${DEFAULT_MODEL_ORDER}
fi

echo "I: Using model order ${MODEL_ORDER}"

DIR_SAILS=${DIR_BASE}/${DIR_SAILS}
DIR_ANAMNESIS=${DIR_BASE}/${DIR_ANAMNESIS}

# Decide which script we want to run
SCRIPT=${CURDIR}/mvar_21_permute_models.py

# Ensure that we have a log directory
mkdir -p "${DIR_BASE}/${DIR_LOGFILES}"

# Submit one job per-participant, per-run
for SUBJ in ${PARTICIPANTS}; do
    for RUN in 3 4 5; do

        echo "Submitting ${SCRIPT} ${SUBJ} ${RUN}"

        qsub -j y -N modelperm_${SUBJ}_${RUN}_order${MODEL_ORDER} -o "${DIR_BASE}/${DIR_LOGFILES}/" << EOF
#!/bin/bash

export PYTHONPATH=${DIR_SAILS}:${DIR_ANAMNESIS}

set -e
set -u

date -u
${SCRIPT} $SUBJ $RUN $MODEL_ORDER
date -u

EOF

    done

done
