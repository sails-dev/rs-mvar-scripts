#!/usr/bin/python3

from sys import exit, argv
from os import makedirs
from os.path import join, isfile

import h5py

import numpy as np

import sails  # noqa: needed by anamnesis for loading
from anamnesis import obj_from_hdf5group

from mvarconfig import (PARTICIPANTS, RUNS,
                        DIR_NULLDIST, DIR_MODALSTATS,
                        DEFAULT_MODEL_ORDER)

if len(argv) not in (1, 2):
    print("mvar_32_fourier_summary.py [MODEL_ORDER]")
    exit(1)

if len(argv) == 2:
    model_order = int(argv[1])
else:
    # Default model order
    model_order = DEFAULT_MODEL_ORDER

print("I: Using model order {}".format(model_order))

# Check that we have an output directory and don't already have the file
out_dir = join(DIR_MODALSTATS, str(model_order))

makedirs(out_dir, exist_ok=True)

out_filename = join(out_dir, '32_fourier_summary.hdf5')

if isfile(out_filename):
    print("E: File {} already exists; not overwriting".format(out_filename))
    exit(1)

# Configuration

# Fourier bands to analyse and their names
psd = []
freq_vect = None

print("I: Loading fourier PSD data")
for person in PARTICIPANTS:
    for run in RUNS:
        name = '{}_{}.hdf5'.format(person, run)

        filename = join(DIR_NULLDIST, str(model_order), name)

        if not isfile(filename):
            print("E: Cannot find file {}".format(filename))
            exit(1)

        print("I: Loading {}".format(name))
        f = h5py.File(filename, 'r')

        m = obj_from_hdf5group(f['model'])
        M = obj_from_hdf5group(f['M'])
        F = obj_from_hdf5group(f['F'])

        sample_rate = F.sample_rate

        if freq_vect is None:
            freq_vect = F.freq_vect

        psd.append(F.PSD[...])

# Convert to numpy array (nareas, nareas, freqbins, runs)
psd = np.concatenate(psd, axis=3)

p = psd.mean(axis=3)
pd = np.einsum('iij->ij', p)

d = np.dot(pd.T, pd)
C = np.corrcoef(pd.T)

print("I: Saving summary to {}".format(out_filename))

f = h5py.File(out_filename, 'w')
f.create_dataset('psd', data=psd)
f.create_dataset('freq_vect', data=freq_vect)
f.close()
