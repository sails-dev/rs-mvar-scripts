#!/usr/bin/python3

from sys import argv, exit
from os.path import join

from PIL import Image, ImageDraw, ImageFont

from mvarconfig import (DIR_FIGURES, DEFAULT_MODEL_ORDER, FONT)

if len(argv) not in (1, 2):
    print("mvar_64_composite_figure_4.py [MODEL_ORDER]")
    exit(1)

if len(argv) == 2:
    model_order = int(argv[1])
else:
    # Default model order
    model_order = DEFAULT_MODEL_ORDER

print("I: Using model order {}".format(model_order))

fig_dir = join(DIR_FIGURES, str(model_order))

# Composite Figure 4
DPI = 600

fig_pixels = [int(7.5 * DPI), int(6.2 * DPI)]

SPECT_X = 0
SURFACE_X = int(2.25 * DPI)
NETMAT_X = SURFACE_X + int(2.1 * DPI)
CIRCOS_X = NETMAT_X + int(1.5625 * DPI)

LABEL_Y = 50
SPECT_Y = 400
THETA_Y = 50
ALPHA_Y = int(DPI * 1.78)
BETA_Y = int(DPI * 3.5)

LABEL_COLOUR = (85, 85, 85, 255)

# Create a blank figure
f = Image.new('RGBA', fig_pixels, 'white')

# Add the spectrum
spect = Image.open(join(fig_dir, '40_spectrum.png'))
f.paste(spect, (SPECT_X, SPECT_Y))

# Add the surface plot colour guide
surflab = Image.open(join(fig_dir, '49_surface_labels.png'))
f.paste(surflab, (SPECT_X + 250, SPECT_Y + 2600))

# Add the surface plots
theta_s = Image.open(join(fig_dir, '41_fourier_theta.png'))
f.paste(theta_s, (SURFACE_X, THETA_Y + 200))

alpha_s = Image.open(join(fig_dir, '41_fourier_alpha.png'))
f.paste(alpha_s, (SURFACE_X, ALPHA_Y + 200))

beta_s = Image.open(join(fig_dir, '41_fourier_beta.png'))
f.paste(beta_s, (SURFACE_X, BETA_Y + 200))

# Add the netmat plots
theta_n = Image.open(join(fig_dir, '40_netmat_theta.png'))
f.paste(theta_n, (NETMAT_X, THETA_Y))

alpha_n = Image.open(join(fig_dir, '40_netmat_alpha.png'))
f.paste(alpha_n, (NETMAT_X, ALPHA_Y))

beta_n = Image.open(join(fig_dir, '40_netmat_beta.png'))
f.paste(beta_n, (NETMAT_X, BETA_Y))

# Add the circos plots
theta_c = Image.open(join(fig_dir, '40_circos_theta.png'))
f.paste(theta_c, (CIRCOS_X, THETA_Y + 125))

alpha_c = Image.open(join(fig_dir, '40_circos_alpha.png'))
f.paste(alpha_c, (CIRCOS_X, ALPHA_Y + 125))

beta_c = Image.open(join(fig_dir, '40_circos_beta.png'))
f.paste(beta_c, (CIRCOS_X, BETA_Y + 125))

draw = ImageDraw.Draw(f)

font = ImageFont.truetype(FONT, 120)

label_offset = SURFACE_X + int((NETMAT_X - SURFACE_X) / 2) - 40

draw.text((label_offset, THETA_Y + 150), "θ", fill=LABEL_COLOUR, font=font)

draw.text((label_offset, ALPHA_Y + 150), "α", fill=LABEL_COLOUR, font=font)

draw.text((label_offset, BETA_Y + 150), "β", fill=LABEL_COLOUR, font=font)

# Rotated text legend
# Removed for now as I prefer the coloured legend below instead
# Left for reference in case we revert to this as the positioning as a pain
# and I don't want to have to re-do it.

# font = ImageFont.truetype(FONT, 40)

# def draw_rotated_text(target, text, font, location, colour, angle=45):
#     # Drawing rotated text in PIL is a pain, so stick a function
#     # in to approximate some sensible behaviour
#     txtim = Image.new('L', (500, 500))
#     txtim_d = ImageDraw.Draw(txtim)
#     txtim_d.text((0, 0), text, font=font, fill=255)
#     w = txtim.rotate(angle, expand=1)
#     target.paste(ImageOps.colorize(w, (0, 0, 0), colour), location,  w)

# TEXT_OFFSET_Y = 215

# draw_rotated_text(f, "L Frontal", font, (NETMAT_X + 145, THETA_Y - TEXT_OFFSET_Y),
#                   (85, 85, 85, 255), angle=50)

# draw_rotated_text(f, "L Medial", font, (NETMAT_X + 225, THETA_Y - TEXT_OFFSET_Y),
#                   (85, 85, 85, 255), angle=50)

# draw_rotated_text(f, "L Temporal", font, (NETMAT_X + 285, THETA_Y - TEXT_OFFSET_Y),
#                   (85, 85, 85, 255), angle=50)

# draw_rotated_text(f, "L Parietal", font, (NETMAT_X + 350, THETA_Y - TEXT_OFFSET_Y),
#                   (85, 85, 85, 255), angle=50)

# draw_rotated_text(f, "L Parietal", font, (NETMAT_X + 420, THETA_Y - TEXT_OFFSET_Y),
#                   (85, 85, 85, 255), angle=50)

# draw_rotated_text(f, "R Frontal", font, (NETMAT_X + 515, THETA_Y - TEXT_OFFSET_Y),
#                   (85, 85, 85, 255), angle=50)

# draw_rotated_text(f, "R Medial", font, (NETMAT_X + 595, THETA_Y - TEXT_OFFSET_Y),
#                   (85, 85, 85, 255), angle=50)

# draw_rotated_text(f, "R Temporal", font, (NETMAT_X + 650, THETA_Y - TEXT_OFFSET_Y),
#                   (85, 85, 85, 255), angle=50)

# draw_rotated_text(f, "R Parietal", font, (NETMAT_X + 720, THETA_Y - TEXT_OFFSET_Y),
#                   (85, 85, 85, 255), angle=50)

# draw_rotated_text(f, "R Occipital", font, (NETMAT_X + 790, THETA_Y - TEXT_OFFSET_Y),
#                   (85, 85, 85, 255), angle=50)

# Add some coloured highlighted text as a legend
areas = ['Frontal', 'Medial', 'Temporal', 'Parietal', 'Occipital']
left_cols = [(217, 120, 99, 255), (220, 213, 103, 255),
             (155, 152, 183, 255), (127, 180, 128, 255),
             (121, 166, 193, 255)]

right_cols = [(152, 49, 58, 255), (178, 170, 49, 255),
              (99, 62, 139, 255), (49, 109, 82, 255),
              (54, 95, 148, 255)]

draw = ImageDraw.Draw(f)

font = ImageFont.truetype(FONT, 75)

x_base = 1500
y_base = 3300
width = 500
height = 100

for idx, a in enumerate(areas):
    # Left
    draw.rectangle(((x_base + (width*idx), y_base),
                    (x_base + (width*(idx+1)), y_base+height)),
                   fill=left_cols[idx])
    draw.text((x_base + (width*idx) + 10, y_base), "L {}".format(a),
              fill=(0, 0, 0, 255), font=font)

    # Right
    draw.rectangle(((x_base + (width*idx), y_base+height),
                    (x_base + (width*(idx+1)), y_base+(2*height))),
                   fill=right_cols[idx])

    draw.text((x_base + (width*idx) + 10, y_base+height + 10),
              "R {}".format(a), fill=(0, 0, 0, 255), font=font)

# Add the column labels
draw.text((SPECT_X + 50, LABEL_Y), "A", fill=LABEL_COLOUR, font=font)

draw.text((SURFACE_X, LABEL_Y), "B", fill=LABEL_COLOUR, font=font)

draw.text((NETMAT_X, LABEL_Y), "C", fill=LABEL_COLOUR, font=font)

draw.text((CIRCOS_X, LABEL_Y), "D", fill=LABEL_COLOUR, font=font)

# Now create a non-alpha version to save
final = Image.new("RGB", f.size, (255, 255, 255))
final.paste(f, mask=f.split()[3])

final.save(join(fig_dir, '64_figure4.png'), dpi=(DPI, DPI))
