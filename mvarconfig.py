#!/usr/bin/python3

from configparser import ConfigParser
from os import linesep
from os.path import join, dirname

# Find the config file in the same place as this module
CONFIG_PATH = join(dirname(__file__), 'mvarconfig.ini')

# Put a fake section into the file so that ConfigParser can handle it
config_data = open(CONFIG_PATH, 'r').read()
config_data = '[root]' + linesep + config_data

c = ConfigParser()
c.read_string(config_data)

__all__ = []

# Handle DIR_BASE specially
DIR_BASE = c['root']['dir_base']

for key in c['root']:
    if key == 'dir_base':
        continue

    value = c['root'][key]

    if key.startswith('dir') or key.startswith('file'):
        value = join(DIR_BASE, value)

    # Urgh - this is nasty - dynamically generate the variable
    # names that export to avoid listing them in more than one place
    vars()[key.upper()] = value

    __all__.append(key.upper())

# Split PARTICIPANTS into a list
PARTICIPANTS = PARTICIPANTS.strip('"').split(" ")  # noqa

# Split RUNS into a list
RUNS = [int(x) for x in RUNS.strip('"').split(" ")]  # noqa

PROC_NUM = int(PROC_NUM)  # noqa

DEFAULT_MODEL_ORDER = int(DEFAULT_MODEL_ORDER)  # noqa
