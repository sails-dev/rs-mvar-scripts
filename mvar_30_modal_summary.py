#!/usr/bin/python3

from sys import argv, exit
from os import makedirs
from os.path import join, isfile

import h5py

import numpy as np
import scipy.stats as ss

from anamnesis import obj_from_hdf5group
import sails

from mvarconfig import (DIR_NULLDIST, DIR_MODALSTATS,
                        PARTICIPANTS, RUNS,
                        DEFAULT_MODEL_ORDER)


def process_model(f, dt_thresh, part, run):
    print("I: Thresholding modes for {}/{} using threshold {:.3f}".format(part, run, dt_thresh))

    # Extract model and M (modal metrics)
    model = obj_from_hdf5group(f['model'])
    M = obj_from_hdf5group(f['M'])

    num_parcels = M.modes.nsignals

    # The dampening time for both pairs of a pole pair will be identical
    # so we can take the DT of the first
    thresh_modes = [x for x in M.modes.mode_indices if M.modes.dampening_time[x[0]] > dt_thresh]

    # Create output variables for our thresholded modes
    num_thresh_modes = len(thresh_modes)

    out_PSD = np.zeros((num_parcels, num_parcels, num_thresh_modes))
    out_S = np.zeros((num_parcels, num_parcels, num_thresh_modes), np.complex)
    out_peak_frequency = np.zeros((num_thresh_modes))
    out_dampening_time = np.zeros((num_thresh_modes))

    # Populate the output variables with the appropriate data
    for mode, poles in enumerate(thresh_modes):
        # Extract the metrics
        out_peak_frequency[mode] = M.modes.peak_frequency[poles[0]]
        out_dampening_time[mode] = M.modes.dampening_time[poles[0]]
        mode_mets = sails.ModalMvarMetrics.initialise_from_modes(model, M.modes, M.sample_rate,
                                                                 np.array([M.modes.peak_frequency[poles[0]]]),
                                                                 poles, sum_modes=True)

        out_PSD[:, :, mode] = mode_mets.PSD[:, :, 0, 0]
        out_S[:, :, mode] = mode_mets.S[:, :, 0, 0]

    return (out_PSD, out_S, out_peak_frequency, out_dampening_time)


if len(argv) not in (1, 2):
    print("mvar_30_modal_summary.py [MODEL_ORDER]")
    exit(1)

if len(argv) == 2:
    model_order = int(argv[1])
else:
    # Default model order
    model_order = DEFAULT_MODEL_ORDER

print("I: Using model order {}".format(model_order))

# Check that we have an output directory and don't already have the file
out_dir = join(DIR_MODALSTATS, str(model_order))

makedirs(out_dir, exist_ok=True)

outfile = join(out_dir, '30_modal_summary.hdf5')

if isfile(outfile):
    print("E: File {} already exists; not overwriting".format(outfile))
    exit(1)

with h5py.File(outfile, 'w') as outf:
    person_idx = []
    run_idx = []
    dt_thresholds = []

    surviving_dts = []
    surviving_pfs = []

    PSD = []
    S = []
    PF = []
    DT = []
    part_idx = []
    run_nums = []

    for partidx, person in enumerate(PARTICIPANTS):
        for runnum in RUNS:
            name = '{}_{}.hdf5'.format(person, runnum)

            filename = join(DIR_NULLDIST, str(model_order), name)

            if not isfile(filename):
                print("E: Cannot find file {}".format(filename))
                exit(1)

            print("I: Loading thresholds from file {}".format(filename))

            with h5py.File(filename, 'r') as f:
                # Load our max stat perm-based DT distribution
                max_perm_dts = f['max_perm_dts']

                # Calculate the 99% DT threshold for keeping modes
                dts_thresh = ss.scoreatpercentile(max_perm_dts, 99)

                dt_thresholds.append(dts_thresh)

                # Examine the model for modes which survive thresholding
                out_PSD, out_S, out_PF, out_DT = process_model(f, dts_thresh, person, runnum)

                # Store the data for later use
                num_modes = len(out_PF)

                p_idx = np.ones((num_modes)) * partidx
                r_nums = np.ones((num_modes)) * runnum

                PSD.append(out_PSD)
                S.append(out_S)
                PF.append(out_PF)
                DT.append(out_DT)
                part_idx.append(p_idx)
                run_nums.append(r_nums)

    # Concatenate everything together into big arrays
    PSD = np.concatenate(PSD, axis=2)
    S = np.concatenate(S, axis=2)
    PF = np.concatenate(PF)
    DT = np.concatenate(DT)
    part_idx = np.concatenate(part_idx).astype(np.int)
    run_nums = np.concatenate(run_nums)
    dt_thresholds = np.array(dt_thresholds)

    # Save out our summary information for further analysis / plotting
    outf.create_dataset('PSD', data=PSD)
    outf.create_dataset('S', data=S)
    outf.create_dataset('PF', data=PF)
    outf.create_dataset('DT', data=DT)
    outf.create_dataset('part_idx', data=part_idx)
    outf.create_dataset('run_nums', data=run_nums)
    outf.create_dataset('dt_thresholds', data=np.array(dt_thresholds))
