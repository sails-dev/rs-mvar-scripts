#!/usr/bin/python3

from os.path import join

from PIL import Image

from mvarconfig import (DIR_FIGURES)

# Composite Figure 2
DPI = 600

# Figure 7
fig_pixels = [int(7.5 * DPI), int(6.25 * DPI)]

# Create a blank figure
f = Image.new('RGBA', fig_pixels, 'white')

# Part 1
p1 = Image.open(join(DIR_FIGURES, '91_timeseries.png'))
f.paste(p1, (0, 0))

# Part 2
p2 = Image.open(join(DIR_FIGURES, '91_single_session.png'))
f.paste(p2, (0, p1.size[1] - 150))

# Now create a non-alpha version to save
final = Image.new("RGB", f.size, (255, 255, 255))
final.paste(f, mask=f.split()[3])

final.save(join(DIR_FIGURES, '62_figure2.png'), dpi=(DPI, DPI))
