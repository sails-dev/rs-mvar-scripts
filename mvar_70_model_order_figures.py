#!/usr/bin/python3

from os.path import join

from PIL import Image

from mvarconfig import (DIR_FIGURES)

fig_dir = DIR_FIGURES

# Composite Figure SI figures
DPI = 600

ORDER = [2, 4, 6, 8, 10, 12, 14, 16]

FIG_ACROSS = 4

# Spectrum figure
# Original figures are 1.75x1.75 inches
fig_pixels = [int(7.5 * DPI), int(1.75 * 2 * DPI)]

f = Image.new('RGBA', fig_pixels, 'white')

for idx, order in enumerate(ORDER):
    x = int(1.75 * (idx % FIG_ACROSS) * DPI)
    y = int(1.75 * (idx // FIG_ACROSS) * DPI)

    spect = Image.open(join(fig_dir, str(order), '40_spectrum_si.png'))
    f.paste(spect, (x, y))

# Now create a non-alpha version to save
final = Image.new("RGB", f.size, (255, 255, 255))
final.paste(f, mask=f.split()[3])

filename = '70_supp_model_order_spectrum.png'
final.save(join(fig_dir, filename), dpi=(DPI, DPI))

# Mode dist figure
# Original figures are 1.75x1.75 inches
fig_pixels = [int(7.5 * DPI), int(3.5 * 2 * DPI)]

f = Image.new('RGBA', fig_pixels, 'white')

for idx, order in enumerate(ORDER):
    x = int(1.75 * (idx % FIG_ACROSS) * DPI)
    y = int(3.5 * (idx // FIG_ACROSS) * DPI)

    spect = Image.open(join(fig_dir, str(order), '42_mode_dist_si.png'))
    f.paste(spect, (x, y))

# Now create a non-alpha version to save
final = Image.new("RGB", f.size, (255, 255, 255))
final.paste(f, mask=f.split()[3])

filename = '70_supp_model_order_mode_dist.png'
final.save(join(fig_dir, filename), dpi=(DPI, DPI))
