#!/usr/bin/python3

from sys import argv, exit
from os.path import join

from PIL import Image, ImageDraw, ImageFont

from mvarconfig import (DIR_FIGURES, FONT,
                        DEFAULT_MODEL_ORDER)

if len(argv) not in (1, 2):
    print("mvar_67_composite_figure_7.py [MODEL_ORDER]")
    exit(1)

if len(argv) == 2:
    model_order = int(argv[1])
else:
    # Default model order
    model_order = DEFAULT_MODEL_ORDER

print("I: Using model order {}".format(model_order))

fig_dir = join(DIR_FIGURES, str(model_order))

# Composite Figure 7
DPI = 600

LABEL_COLOUR = (85, 85, 85, 255)

# Figure 7
fig_pixels = [int(7.5) * DPI, int(7.5) * DPI]

PC_X_OFFSET = [0, fig_pixels[0] // 2]

HIST_X_OFFSET = (fig_pixels[0] // 4) - 50
HIST_Y_OFFSET = 0

SCATTER_X_OFFSET = HIST_X_OFFSET
SCATTER_Y_OFFSET = int(1.2 * DPI)

SURFACE_X_OFFSET = 100
SURFACE_Y_OFFSET = int(DPI)

NETMAT_X_OFFSET = -50
NETMAT_Y_OFFSET = int((1.2 + 3.2) * DPI)

CIRCOS_X_OFFSET = SCATTER_X_OFFSET
CIRCOS_Y_OFFSET = NETMAT_Y_OFFSET

# Create a blank figure
f = Image.new('RGBA', fig_pixels, 'white')

# Add the scatter plots - only put the bayes estimate on
# where statistically appropriate
pc1_scatter = Image.open(join(fig_dir, '51_pca_alpha_scatter_1.png'))
pc2_scatter = Image.open(join(fig_dir, '51_pca_alpha_scatter_2_bayes.png'))

f.paste(pc1_scatter, (PC_X_OFFSET[0] + SCATTER_X_OFFSET, SCATTER_Y_OFFSET))
f.paste(pc2_scatter, (PC_X_OFFSET[1] + SCATTER_X_OFFSET, SCATTER_Y_OFFSET))

# Load and plot the histograms
# We do these after the scatter plots as there's some white space
# at the top of the scatters and this lets these override that
pc1_hist = Image.open(join(fig_dir, '51_pca_alpha_hist_1.png'))
pc2_hist = Image.open(join(fig_dir, '51_pca_alpha_hist_2.png'))

f.paste(pc1_hist, (PC_X_OFFSET[0] + HIST_X_OFFSET, 0))
f.paste(pc2_hist, (PC_X_OFFSET[1] + HIST_X_OFFSET, 0))

# Add the netmats
pc1_netmat = Image.open(join(fig_dir, '51_pca_alpha_netmat_1.png'))
pc2_netmat = Image.open(join(fig_dir, '51_pca_alpha_netmat_2.png'))

f.paste(pc1_netmat, (PC_X_OFFSET[0] + NETMAT_X_OFFSET, NETMAT_Y_OFFSET))
f.paste(pc2_netmat, (PC_X_OFFSET[1] + NETMAT_X_OFFSET, NETMAT_Y_OFFSET))

# Add the brain overlays
pc1_surf = Image.open(join(fig_dir, '50_hcp_modalpca_alpha_component_tall_1.png'))
pc2_surf = Image.open(join(fig_dir, '50_hcp_modalpca_alpha_component_tall_2.png'))

f.paste(pc1_surf, (PC_X_OFFSET[0] + SURFACE_X_OFFSET, SURFACE_Y_OFFSET))
f.paste(pc2_surf, (PC_X_OFFSET[1] + SURFACE_X_OFFSET, SURFACE_Y_OFFSET))

# Add the circos plots
pc1_netmat = Image.open(join(fig_dir, '51_pca_alpha_circos_1.png'))
pc2_netmat = Image.open(join(fig_dir, '51_pca_alpha_circos_2.png'))

f.paste(pc1_netmat, (PC_X_OFFSET[0] + CIRCOS_X_OFFSET, CIRCOS_Y_OFFSET))
f.paste(pc2_netmat, (PC_X_OFFSET[1] + CIRCOS_X_OFFSET, CIRCOS_Y_OFFSET))

# Add a line to split up the two PCs
draw = ImageDraw.Draw(f)

LINE_X_OFFSET = PC_X_OFFSET[1] + 30
draw.line((LINE_X_OFFSET, 150,
           LINE_X_OFFSET, fig_pixels[1] - 400),
          fill=LABEL_COLOUR, width=5)

# Add some text
font = ImageFont.truetype(FONT, 180)

lfont = ImageFont.truetype(FONT, 120)

draw.text((PC_X_OFFSET[0] + 100, 100), 'PC1', font=font, fill=LABEL_COLOUR)
draw.text((PC_X_OFFSET[1] + 100, 100), 'PC2', font=font, fill=LABEL_COLOUR)

# And some alphabetical labels
for x in PC_X_OFFSET:
    draw.text((x + 100, 400), 'A', font=lfont, fill=LABEL_COLOUR)
    draw.text((x + 1000, 75), 'B', font=lfont, fill=LABEL_COLOUR)
    draw.text((x + 1000, 900), 'C', font=lfont, fill=LABEL_COLOUR)
    draw.text((x + 100, 2550), 'D', font=lfont, fill=LABEL_COLOUR)
    draw.text((x + 1000, 2550), 'E', font=lfont, fill=LABEL_COLOUR)

# And put a region label on the bottom
areas = ['Frontal', 'Medial', 'Temporal', 'Parietal', 'Occipital']
left_cols = [(217, 120, 99, 255), (220, 213, 103, 255),
             (155, 152, 183, 255), (127, 180, 128, 255),
             (121, 166, 193, 255)]

right_cols = [(152, 49, 58, 255), (178, 170, 49, 255),
              (99, 62, 139, 255), (49, 109, 82, 255),
              (54, 95, 148, 255)]

draw = ImageDraw.Draw(f)

font = ImageFont.truetype(FONT, 75)

x_base = 875
y_base = 3900
width = 500
height = 100

for idx, a in enumerate(areas):
    # Left
    draw.rectangle(((x_base + (width*idx), y_base),
                    (x_base + (width*(idx+1)), y_base+height)),
                   fill=left_cols[idx])
    draw.text((x_base + (width*idx) + 10, y_base), "L {}".format(a),
              fill=(0, 0, 0, 255), font=font)

    # Right
    draw.rectangle(((x_base + (width*idx), y_base+height),
                    (x_base + (width*(idx+1)), y_base+(2*height))),
                   fill=right_cols[idx])

    draw.text((x_base + (width*idx) + 10, y_base+height + 10),
              "R {}".format(a), fill=(0, 0, 0, 255), font=font)


# Now create a non-alpha version to save
final = Image.new("RGB", f.size, (255, 255, 255))
final.paste(f, mask=f.split()[3])

final.save(join(fig_dir, '67_figure7.png'), dpi=(DPI, DPI))
