#!/usr/bin/python3

from sys import argv, exit
from os import makedirs
from os.path import join, isfile

import h5py

import numpy as np
import scipy.stats as ss

from anamnesis import obj_from_hdf5group
import sails  # noqa: needed for anamnesis to work

from mvarconfig import (PARTICIPANTS, RUNS,
                        DIR_NULLDIST, DIR_MODALSTATS,
                        DEFAULT_MODEL_ORDER)

if len(argv) not in (1, 2):
    print("mvar_31_per_participant_modes.py [MODEL_ORDER]")
    exit(1)

if len(argv) == 2:
    model_order = int(argv[1])
else:
    # Default model order
    model_order = DEFAULT_MODEL_ORDER

print("I: Using model order {}".format(model_order))

# Check that we have an output directory and don't already have the file
out_dir = join(DIR_MODALSTATS, str(model_order))

makedirs(out_dir, exist_ok=True)

out_filename = join(out_dir, '31_surviving_modes.hdf5')

if isfile(out_filename):
    print("E: File {} already exists; not overwriting".format(out_filename))
    exit(1)

dt_thresholds = []

all_psd = []

surviving_dts = []
surviving_pfs = []
surviving_runs = []
surviving_psd = []

non_surviving_dts = []
non_surviving_pfs = []
non_surviving_runs = []
non_surviving_psd = []

person_list = []
run_list = []

# ? Track participant and run indices to make plotting easier ?
cnt = 0

for person in PARTICIPANTS:
    for run in RUNS:
        name = '{}_{}.hdf5'.format(person, run)
        print("I: Loading {}".format(name))
        # Dampening times
        # Plot histogram of dampening times with real permutation as well

        f = h5py.File(join(DIR_NULLDIST, str(model_order), name), 'r')

        m = obj_from_hdf5group(f['model'])
        M = obj_from_hdf5group(f['M'])
        F = obj_from_hdf5group(f['F'])

        # Get the observed statistics
        real_dts = M.modes.dampening_time
        real_pfs = M.modes.peak_frequency
        real_mags = np.abs(M.modes.evals)

        # Use a max stat approach to get a DT distribution
        max_perm_dts = f['max_perm_dts']

        dts_thresh = ss.scoreatpercentile(max_perm_dts, 99)

        dt_thresholds.append(dts_thresh)

        # Store our above threshold information
        idx = np.where(real_dts > dts_thresh)
        no_idx = np.where(real_dts <= dts_thresh)

        non_thresh_dts = real_dts[no_idx]
        non_thresh_pfs = real_pfs[no_idx]

        non_surviving_dts.append(non_thresh_dts)
        non_surviving_pfs.append(non_thresh_pfs)
        non_surviving_runs.append(np.ones_like(non_thresh_dts) * cnt)

        thresh_dts = real_dts[idx]
        thresh_pfs = real_pfs[idx]

        surviving_dts.append(thresh_dts)
        surviving_pfs.append(thresh_pfs)
        surviving_runs.append(np.ones_like(thresh_dts) * cnt)

        person_list.append(person)
        run_list.append(run)

        # Compute a set of metrics with only the right poles in
        s_mets = sails.ModalMvarMetrics.initialise_from_modes(m, M.modes,
                                                              M.sample_rate, M.freq_vect,
                                                              idx[0], sum_modes=True)

        n_mets = sails.ModalMvarMetrics.initialise_from_modes(m, M.modes,
                                                              M.sample_rate, M.freq_vect,
                                                              no_idx[0], sum_modes=True)

        a_m = np.einsum('iij->ij', M.PSD[:, :, :, 0])
        s_m = np.einsum('iij->ij', s_mets.PSD[:, :, :, 0])
        n_m = np.einsum('iij->ij', n_mets.PSD[:, :, :, 0])

        # Adding an index makes concatenation later easier
        all_psd.append(a_m[:, None])
        surviving_psd.append(s_m[:, None])
        non_surviving_psd.append(n_m[:, None])

        cnt += 1

# Convert from list of arrays to indexed array - easier to save...
a_psd = np.concatenate(all_psd, axis=1)

s_dts = np.concatenate(surviving_dts)
s_pfs = np.concatenate(surviving_pfs)
s_run = np.concatenate(surviving_runs)
s_psd = np.concatenate(surviving_psd, axis=1)

n_dts = np.concatenate(non_surviving_dts)
n_pfs = np.concatenate(non_surviving_pfs)
n_run = np.concatenate(non_surviving_runs)
n_psd = np.concatenate(non_surviving_psd, axis=1)

dts_per_run = np.array([len(x) for x in surviving_dts])

# Save out the summary info
print("I: Saving summary to {}".format(out_filename))

outF = h5py.File(out_filename, 'w')
outF.create_dataset('a_psd', data=a_psd)
outF.create_dataset('s_dts', data=s_dts)
outF.create_dataset('s_pfs', data=s_pfs)
outF.create_dataset('s_runs', data=s_run)
outF.create_dataset('s_psd', data=s_psd)
outF.create_dataset('n_pfs', data=n_pfs)
outF.create_dataset('n_dts', data=n_dts)
outF.create_dataset('n_runs', data=n_run)
outF.create_dataset('n_psd', data=n_run)
outF.create_dataset('dt_thresholds', data=dt_thresholds)
outF.create_dataset('dts_per_run', data=dts_per_run)
outF.create_dataset('freq_vect', data=M.freq_vect)

# Fiddly way of writing a list of strings
dt = h5py.special_dtype(vlen=str)

pl = outF.create_dataset('person_list', shape=(len(person_list), ), maxshape=(None,), dtype=dt)
pl[...] = person_list

rl = outF.create_dataset('run_list', shape=(len(run_list), ), maxshape=(None,), dtype=dt)
rl[...] = run_list
outF.close()
